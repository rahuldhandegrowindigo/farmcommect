package com.growindigo.connectplus;

public class ReportVisitModel {

    public ReportVisitModel()
    {

    }

    private String txt_fnamedetails;
    private String txt_MobileDetails;
    private String txt_CropDetails;

    public String getTxt_VisitDateDetails() {
        return txt_VisitDateDetails;
    }

    public void setTxt_VisitDateDetails(String txt_VisitDateDetails) {
        this.txt_VisitDateDetails = txt_VisitDateDetails;
    }

    public String getTxt_RemarkDetails() {
        return txt_RemarkDetails;
    }

    public void setTxt_RemarkDetails(String txt_RemarkDetails) {
        this.txt_RemarkDetails = txt_RemarkDetails;
    }

    private String txt_VisitDateDetails;
private String txt_RemarkDetails;
    public String getTxt_fnamedetails() {
        return txt_fnamedetails;
    }

    public void setTxt_fnamedetails(String txt_fnamedetails) {
        this.txt_fnamedetails = txt_fnamedetails;
    }

    public String getTxt_MobileDetails() {
        return txt_MobileDetails;
    }

    public void setTxt_MobileDetails(String txt_MobileDetails) {
        this.txt_MobileDetails = txt_MobileDetails;
    }

    public String getTxt_CropDetails() {
        return txt_CropDetails;
    }

    public void setTxt_CropDetails(String txt_CropDetails) {
        this.txt_CropDetails = txt_CropDetails;
    }

    public String getTxt_AreaDetails() {
        return txt_AreaDetails;
    }

    public void setTxt_AreaDetails(String txt_AreaDetails) {
        this.txt_AreaDetails = txt_AreaDetails;
    }

    public String getTxt_SowingDateDetails() {
        return txt_SowingDateDetails;
    }

    public void setTxt_SowingDateDetails(String txt_SowingDateDetails) {
        this.txt_SowingDateDetails = txt_SowingDateDetails;
    }

    public String getTxt_visitnoDetails() {
        return txt_visitnoDetails;
    }

    public void setTxt_visitnoDetails(String txt_visitnoDetails) {
        this.txt_visitnoDetails = txt_visitnoDetails;
    }

    public String getTxt_visitDetails() {
        return txt_visitDetails;
    }

    public void setTxt_visitDetails(String txt_visitDetails) {
        this.txt_visitDetails = txt_visitDetails;
    }

    public String getTxt_PeriodDetails() {
        return txt_PeriodDetails;
    }

    public void setTxt_PeriodDetails(String txt_PeriodDetails) {
        this.txt_PeriodDetails = txt_PeriodDetails;
    }

    public String getTxt_CropStageDetails() {
        return txt_CropStageDetails;
    }

    public void setTxt_CropStageDetails(String txt_CropStageDetails) {
        this.txt_CropStageDetails = txt_CropStageDetails;
    }

    public String getTxt_ObservationDetails() {
        return txt_ObservationDetails;
    }

    public void setTxt_ObservationDetails(String txt_ObservationDetails) {
        this.txt_ObservationDetails = txt_ObservationDetails;
    }

    public String getTxt_ProductDetails() {
        return txt_ProductDetails;
    }

    public void setTxt_ProductDetails(String txt_ProductDetails) {
        this.txt_ProductDetails = txt_ProductDetails;
    }

    public String getTxt_DosageDetails() {
        return txt_DosageDetails;
    }

    public void setTxt_DosageDetails(String txt_DosageDetails) {
        this.txt_DosageDetails = txt_DosageDetails;
    }

    public String getTxt_PuchaseDetails() {
        return txt_PuchaseDetails;
    }

    public void setTxt_PuchaseDetails(String txt_PuchaseDetails) {
        this.txt_PuchaseDetails = txt_PuchaseDetails;
    }

    private String txt_AreaDetails;
    private String txt_SowingDateDetails;
    private String txt_visitnoDetails;
    private String txt_visitDetails;
    private String txt_PeriodDetails;
    private String txt_CropStageDetails;
    private String txt_ObservationDetails;
    private String txt_ProductDetails;
    private String txt_DosageDetails;
    private String txt_PuchaseDetails;
}
