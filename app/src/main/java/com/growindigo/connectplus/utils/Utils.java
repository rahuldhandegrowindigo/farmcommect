package com.growindigo.connectplus.utils;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.location.Address;
import android.location.Geocoder;
import android.media.ExifInterface;
import android.os.Build;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.android.gms.maps.model.LatLng;
import com.growindigo.connectplus.R;
import com.growindigo.connectplus.volley.ApiRequest;
import com.growindigo.connectplus.volley.IApiResponse;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import br.com.joinersa.oooalertdialog.OnClickListener;
import br.com.joinersa.oooalertdialog.OoOAlertDialog;

public class Utils {


    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 124;
    private static final String IMAGE_DIRECTORY_NAME = "Infinity";
    private static final String IMAGE_SUBDIRECTORY_NAME = "Brochure";
    private static Context mContext;


    public static String parseDateTimeChange(String time, String inputPattern, String outputPattern) {
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
        return str;
    }

    public static String loadListJSONFromAsset(Context context) {
        String json = null;
        InputStream is = null;
        try {
            is = context.getAssets().open("getOutlet.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }


    public static String loadListTurnover(Context context) {
        String json = null;
        InputStream is = null;
        try {
            is = context.getAssets().open("getTurnOver.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }


//    public static void showFancyAlertDialog(Context context, DialogTitle dialogTitle, String msg, String PositiveButton, String NegativeButton, final com.mahyco.retail.online.utils.DialogInterface dialogInterface){
//        OoOAlertDialog.Builder builder = new OoOAlertDialog.Builder((Activity) context);
//        builder.setTitle(dialogTitle).setMessage(msg).setCancelable(false).setPositiveButton(PositiveButton, new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialogInterface.Positive((com.mahyco.retail.online.utils.DialogInterface) dialog, id);
//            }
//        }).setNegativeButton(NegativeButton, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int id) {
//                dialogInterface.Negative((com.mahyco.retail.online.utils.DialogInterface) dialog, id);
//            }
//        });
//
//    }

    public static void showAlertDialog(String sTitle, String sMessage, final Context myContext) {
        final OoOAlertDialog.Builder builder = new OoOAlertDialog.Builder((Activity) myContext);

        builder
                .setTitle(sTitle)
                .setBackgroundColor(R.color.white1)
                .setTitleColor(R.color.black)
                .setMessageColor(R.color.black)
                .setPositiveButtonColor(R.color.black)
                .setMessage("    " + sMessage + "    ")
                .setCancelable(false)
                .setPositiveButton(myContext.getString(R.string.ok), new OnClickListener() {
                    @Override
                    public void onClick() {
                        builder.setCancelable(true);
                        ;
                    }
                })
                .build();
    }

    public static void showAlertDialogOk(String sTitle, String sMessage, final Context myContext) {
        final OoOAlertDialog.Builder builder = new OoOAlertDialog.Builder((Activity) myContext);

        builder
                .setTitle(sTitle)
                .setBackgroundColor(R.color.white1)
                .setTitleColor(R.color.black)
                .setMessageColor(R.color.black)
                .setPositiveButtonColor(R.color.black)
                .setMessage("    " + sMessage + "    ")
                .setCancelable(false)
                .setPositiveButton(myContext.getString(R.string.ok), new OnClickListener() {
                    @Override
                    public void onClick() {
                        builder.setCancelable(true);
                        ((Activity) myContext).finish();
                    }
                })
                .build();


    }


    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public static String loadBeatList(Context context) {
        String json = null;
        InputStream is = null;
        try {
            is = context.getAssets().open("getBeatPlanJson.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }


    public static String getOrderHistoryList(Context context) {
        String json = null;
        InputStream is = null;
        try {
            is = context.getAssets().open("getOrderHistoryList.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd MMM, yy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String parseDate(String time, Boolean isStarted) {
        String outputPattern = "";
        String inputPattern = "";
        if (isStarted) {
            outputPattern = "yyyy-MM-dd";
            inputPattern = "dd MMMM yyyy";
        } else {
            outputPattern = "yyyy-MM-dd 23:59:59";
            inputPattern = "dd MMMM yyyy";
        }

        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String parseToDate(String time) {
        String outputPattern = "";
        String inputPattern = "";
        outputPattern = "yyyy-MM-dd";
        inputPattern = "dd MMMM yyyy";

        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String parseToDateinyy(String time) {
        String outputPattern = "";
        String inputPattern = "";
        outputPattern = "yyyy-MM-dd";
        inputPattern = "dd/MM/yyyy";

        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }


    public static String parseDateForSectionItem(String time, Boolean isStarted) {
        String outputPattern = "";
        String inputPattern = "";
        if (isStarted) {
            inputPattern = "yyyy-MM-dd";
            outputPattern = "dd MMM, yy";
        } else {
            inputPattern = "yyyy-MM-dd 23:59:59";
            outputPattern = "dd MMM, yy";
        }

        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static Calendar getLastMonthDate() {
        Calendar now = Calendar.getInstance();
        now.add(Calendar.MONTH, -1);
        return now;
    }

    public static Calendar getCurrentDayDate() {
        Calendar now = Calendar.getInstance();
        now.add(Calendar.DATE, 0);
        return now;
    }

    public static Calendar setCurrentDayDate() {
        Calendar now = Calendar.getInstance();
        now.add(Calendar.DATE, 1);
        return now;
    }


    public static String getGMTCurrentDayDate() {
        SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT"));
        return dateFormatGmt.format(new Date()) + "";
    }

    public static Calendar getLastWeekDate() {
        Calendar now = Calendar.getInstance();
        now.add(Calendar.DAY_OF_MONTH, 0);
        return now;
    }

    public static Calendar getSelectedCalender(String setdate) {
        String pattern = "yyyy-MM-dd";
        try {
            Date date = new SimpleDateFormat(pattern).parse(setdate);
            System.out.println(date); // Wed Mar 09 03:02:10 BOT 2011
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            return calendar;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public static String parseDateChange(String time) {
        String outputPattern = "";
        String inputPattern = "";
        inputPattern = "yyyy-MM-dd HH:mm:ss";
        outputPattern = "dd MMM, yyyy";

        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String parseDateOnScheme(String time) {
        String outputPattern = "";
        String inputPattern = "";
        inputPattern = "yyyy-MM-dd";
        outputPattern = "dd MMM, yy";

        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String parsingDate(String time) {
        String outputPattern = "";
        String inputPattern = "";
        inputPattern = "yyyy-MM-dd";
        outputPattern = "d MMMM yyyy";

        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }


    public static String parsingInDate(String time) {
        String outputPattern = "";
        String inputPattern = "";
        outputPattern = "yyyy-MM-dd";
        inputPattern = "d MMMM yyyy";

        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }


//    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
//    public static boolean checkPermission(final Context context) {
//        int currentAPIVersion = Build.VERSION.SDK_INT;
//        if (currentAPIVersion >= Build.VERSION_CODES.M) {
//            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
//                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
//                    alertBuilder.setCancelable(true);
//                    alertBuilder.setTitle("Permission necessary");
//                    alertBuilder.setMessage("External storage permission is necessary");
//                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
//                        public void onClick(DialogInterface dialog, int which) {
//                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
//                        }
//                    });
//                    AlertDialog alert = alertBuilder.create();
//                    alert.show();
//
//                } else {
//                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
//                                    Manifest.permission.WRITE_EXTERNAL_STORAGE},
//                            MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
//                }
//                return false;
//            } else {
//                return true;
//            }
//        } else {
//            return true;
//        }
//    }


    public static boolean checkPermissionLocation(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.ACCESS_FINE_LOCATION)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission needed");
                    alertBuilder.setMessage(context.getResources().getString(R.string.pleaseallowlocationpermission));
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                    MY_PERMISSIONS_REQUEST_LOCATION);
                        }
                    });
                    alertBuilder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                    MY_PERMISSIONS_REQUEST_LOCATION);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();

                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            MY_PERMISSIONS_REQUEST_LOCATION);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }


    public static LatLng getLocationFromAddress(String strAddress, Activity context) {
        LatLng latLng = null;
        Geocoder coder = new Geocoder(context);
        try {
            ArrayList<Address> adresses = (ArrayList<Address>) coder.getFromLocationName(strAddress, 5);
            for (Address add : adresses) {
//Controls to ensure it is right address such as country etc.
                double longitude = add.getLongitude();
                double latitude = add.getLatitude();
                latLng = new LatLng(latitude, longitude);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return latLng;
    }


    public static File getOutputMediaFile(String f_url) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME + "/" + IMAGE_SUBDIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;

        String extension = "", filename = "";
        if (f_url.contains(".")) {
            extension = f_url.substring(f_url.lastIndexOf("."));
        }

        if (extension.equalsIgnoreCase(".jpg") || extension.equalsIgnoreCase(".png") || extension.equalsIgnoreCase(".jpeg")) {
            filename = System.currentTimeMillis() + ".jpg";
        }

        if (extension.equalsIgnoreCase(".pdf")) {
            filename = System.currentTimeMillis() + ".pdf";
        }

        if (extension.equalsIgnoreCase(".mp4") || extension.equalsIgnoreCase(".mov") || extension.equalsIgnoreCase(".3gp")) {
            filename = System.currentTimeMillis() + ".mp4";
        }


        System.out.println("EXtens" + filename);

        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + filename);


        //Utils.printData("getOutputMediaFile",mediaFile.getPath());

        return mediaFile;
    }


    public static String getCurrentData() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        return df.format(c.getTime());
    }

    public static String getCurrentDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        System.out.println("Curreeent da:" + df.format(c.getTime()));
        return df.format(c.getTime());
    }

    public static String getCurrentDateTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println("Curreeent da:" + df.format(c.getTime()));
        return df.format(c.getTime());
    }

    public static String getcurrentDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        cal = Calendar.getInstance();
        // cal.add(Calendar.HOUR, +1);
        return dateFormat.format(cal.getTime());
    }

    public static String convertDate(String time) {
        String outputPattern = "";
        String inputPattern = "";
        inputPattern = "d MMMM yyyy";
        outputPattern = "yyyy-MM-dd";

        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String GetDateinDDMMYYYY(String getdate) {
        String reformattedStr = "";
        SimpleDateFormat fromUser = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat myFormat = new SimpleDateFormat("dd/MM/yyyy");

        try {
            if (!getdate.equals("")) {
                reformattedStr = myFormat.format(fromUser.parse(getdate));
            }


        } catch (ParseException e) {
            e.printStackTrace();
        }
        return reformattedStr;
    }

    public static String convertMyDate(String time) {
        String outputPattern = "";
        String inputPattern = "";
        outputPattern = "dd MMMM yyyy";
        inputPattern = "yyyy-MM-dd";

        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String convertDateString(String time) {
        String outputPattern = "";
        String inputPattern = "";
        outputPattern = "dd MMMM yyyy";
        inputPattern = "yyyy-MM-dd HH:mm";

        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }


    public static int convertMyMonth(String time) {
        String outputPattern = "";
        String inputPattern = "";
        outputPattern = "MM";
        inputPattern = "yyyy-MM-dd";

        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        String Cdate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

        Date date = null;
        String str = Cdate;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return Integer.parseInt(str);
    }


    public static int convertMyMonthth(String time) {
        String outputPattern = "";
        String inputPattern = "";
        outputPattern = "MM";
        inputPattern = "yyyy-MM-dd";

        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        String Cdate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

        Date date = null;
        String str = Cdate;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return Integer.parseInt(str);
    }


    public static int convertMyDay(String time) {
        String outputPattern = "";
        String inputPattern = "";
        outputPattern = "dd";
        inputPattern = "yyyy-MM-dd";

        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        String Cdate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());


        Date date = null;
        String str = Cdate;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {

            e.printStackTrace();
        }
        return Integer.parseInt(str);
    }


    public static int convertMyYear(String time) {
        String outputPattern = "";
        String inputPattern = "";
        outputPattern = "yyyy";
        inputPattern = "yyyy-MM-dd";

        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return Integer.parseInt(str);
    }


    public static int getCurrentYear() {
        Calendar now = Calendar.getInstance();
        return now.get(now.YEAR);
    }

    /**
     * <P>Method to convert file path into base64</P>
     *
     * @param path
     * @return
     */
    public static String getBase64FromPath(String path) {
        String base64 = "";
        try {/*from   w w w .  ja  va  2s  .  c om*/
            File file = new File(path);
            byte[] buffer = new byte[(int) file.length() + 100];
            int length = new FileInputStream(file).read(buffer);
            base64 = Base64.encodeToString(buffer, 0, length,
                    Base64.DEFAULT);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return base64;
    }

    public static String encodeImage(String path) {
        File imagefile = new File(path);
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(imagefile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Bitmap bm = BitmapFactory.decodeStream(fis);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);
        //Base64.de
        return encImage;
    }

    // Converting File to Base64.encode String type using Method
    public static String getImageDatadetail(String path) {
        String str = "";
        try {
            if (path != null || path.length() > 0) {
                str = path;
                File f = new File(str);
                Bitmap b = BitmapFactory.decodeFile(path);
                // original measurements
                int origWidth = b.getWidth();
                int origHeight = b.getHeight();
                final int destWidth = 200;//or the width you need
                if (origWidth > destWidth) {
                    // picture is wider than we want it, we calculate its target height
                    int destHeight = origHeight / (origWidth / destWidth);
                    // we create an scaled bitmap so it reduces the image, not just trim it
                    Bitmap b2 = compressImage(str);//scaleBitmap(b,400,400);


                    // 70 is the 0-100 quality percentage
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    b2.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    str = Base64.encodeToString(byteArray, Base64.DEFAULT);
                } else {
                    // 70 is the 0-100 quality percentage
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    b.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    str = Base64.encodeToString(byteArray, Base64.DEFAULT);
                }
            }


        } catch (Exception e) {
            Log.d("TAG_NAME", e.getMessage());
        }


        return str;
    }

    public static Bitmap compressImage(String str) {
        Bitmap scaledBitmap = null;
        try {
            File f = new File(str);


            BitmapFactory.Options options = new BitmapFactory.Options();
//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
            options.inJustDecodeBounds = true;
            Bitmap bmp = BitmapFactory.decodeFile(f.getAbsolutePath(), options);
            int actualHeight = options.outHeight;
            int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612
            // float maxHeight = 816.0f;
            //float maxWidth = 612.0f;
            float maxHeight = 516.0f;
            float maxWidth = 412.0f;
            float imgRatio = actualWidth / actualHeight;
            float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image
            if (actualHeight > maxHeight || actualWidth > maxWidth) {
                if (imgRatio < maxRatio) {
                    imgRatio = maxHeight / actualHeight;
                    actualWidth = (int) (imgRatio * actualWidth);
                    actualHeight = (int) maxHeight;
                } else if (imgRatio > maxRatio) {
                    imgRatio = maxWidth / actualWidth;
                    actualHeight = (int) (imgRatio * actualHeight);
                    actualWidth = (int) maxWidth;
                } else {
                    actualHeight = (int) maxHeight;
                    actualWidth = (int) maxWidth;
                }
            }

//      setting inSampleSize value allows to load a scaled down version of the original image
            options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
            options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
            options.inPurgeable = true;
            options.inInputShareable = true;
            options.inTempStorage = new byte[16 * 1024];

            try {
//          load the bitmap from its path
                //bmp = BitmapFactory.decodeFile(filePath, options);
                bmp = BitmapFactory.decodeFile(f.getAbsolutePath(), options);
            } catch (OutOfMemoryError exception) {
                exception.printStackTrace();

            }
            try {
                scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
            } catch (OutOfMemoryError exception) {
                exception.printStackTrace();
            }

            float ratioX = actualWidth / (float) options.outWidth;
            float ratioY = actualHeight / (float) options.outHeight;
            float middleX = actualWidth / 2.0f;
            float middleY = actualHeight / 2.0f;

            Matrix scaleMatrix = new Matrix();
            scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

            Canvas canvas = new Canvas(scaledBitmap);
            canvas.setMatrix(scaleMatrix);
            canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
            ExifInterface exif = null;
            try {
                try {
                    exif = new ExifInterface(f.getAbsolutePath());
                } catch (IOException e) {
                    e.printStackTrace();
                }

                int orientation = exif.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION, 0);
                Log.d("EXIF", "Exif: " + orientation);
                Matrix matrix = new Matrix();
                if (orientation == 6) {
                    matrix.postRotate(90);
                    Log.d("EXIF", "Exif: " + orientation);
                } else if (orientation == 3) {
                    matrix.postRotate(180);
                    Log.d("EXIF", "Exif: " + orientation);
                } else if (orientation == 8) {
                    matrix.postRotate(270);
                    Log.d("EXIF", "Exif: " + orientation);
                }
                scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                        scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                        true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return scaledBitmap;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        String debugTag = "MemoryInformation";
        // Image nin islenmeden onceki genislik ve yuksekligi
        final int height = options.outHeight;
        final int width = options.outWidth;
        Log.d(debugTag, "image height: " + height + "---image width: " + width);
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        Log.d(debugTag, "inSampleSize: " + inSampleSize);
        return inSampleSize;
    }


    public static void fetchCurrentLocation(String userCode, String latitude,
                                            String longitude, String activityName,
                                            String currentDate, Activity mContext, IApiResponse iApiResponse) {
//        Log.d("userCode::", userCode + " latitude::" + latitude + " longitude::" +
//                longitude + " activityName::" + activityName + " currentDate::" +
//                currentDate);
//
//        Map<String, String> paramsReq = new HashMap<String, String>();
//        paramsReq.put("userCode", userCode);
//        if (latitude != null)
//            paramsReq.put("latitude", latitude);
//        else
//            paramsReq.put("latitude", "0.0");
//        if (longitude != null)
//            paramsReq.put("longitude", longitude);
//        else
//            paramsReq.put("longitude", "0.0");
//        if (activityName != null && activityName.length() > 0)
//            paramsReq.put("activity", activityName);
//        else
//            paramsReq.put("activity", "NA");
//        paramsReq.put("activityDate", currentDate);
//        ApiRequest apiRequest = new ApiRequest(mContext, iApiResponse);
//        apiRequest.postJSONRequestSales(Constants.BASE_URL + Constants.activityTrackingUpload, Constants.activityTrackingUpload,
//                paramsReq, Request.Method.POST);
    }
}
