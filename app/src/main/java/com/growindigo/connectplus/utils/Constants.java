package com.growindigo.connectplus.utils;


public class Constants {
    //public static String BASE_URL = "http://growonline.growindigo.co.in/APIs/FarmConnect/api/connectplus/";
    public static String BASE_URL = "https://growonline.mahyco.com/APIs/FarmConnect/api/connectplus/";

    public static String VerifyUser = "VerifyUser";
    public static String DownloadData = "DownloadData";
    public static String UplaodFarmerVisitData = "UplaodFarmerVisitData";
    public static String farmerinfo_Upload = "farmerinfo_Upload";
    public static String PREF_USERNAME = "username";
    public static String PREF_USERCODE = "usercode";
    public static String PREF_USERPWD = "userpwd";
    public static String PREF_USERROLE = "userrole";
    public static String PREF_Territory_Name = "Territory_Name";
    public static String PREF_Territory_code = "territory_Code";
    public static String IS_LOGGED_IN = "IS_LOGGED_IN";
    public static String UplaodFarmerComplaintData = "UplaodFarmerComplaintData";
    public static String UplaodFarmerDemoData = "UplaodFarmerDemoData";
    public static String UplaodFarmerMeetData = "UplaodFarmerMeetData";

    public static String dist_collection_upload = "dist_collection_upload";
    public static String dist_complaint_upload = "dist_complaint_upload";
    public static String dist_Appointment_upload = "dist_Appointment_upload";
    public static String Ret_dist_upload = "Ret_dist_upload";
    public static String Campaign_upload = "Campaign_upload";
    public static String Sales_order_upload = "Sales_order_upload";
    public static String Stock_upload = "Stock_upload";
    public static String dist_assign = "dist_assign";
    public static String activityTrackingUpload = "activityTrackingUpload";
    public static String UploadGenralVisit="GenralVisit";
}
