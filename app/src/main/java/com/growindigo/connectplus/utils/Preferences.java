package com.growindigo.connectplus.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.growindigo.connectplus.login;

import java.lang.reflect.Type;
import java.util.ArrayList;


public class Preferences {

    public static final String PREF_IS_LANG_CHANGED = "isLangChanged";
    public static final String PREF_IS_LANG_SELECTED = "isLangSelected";

    public Preferences(Context context) {
    }


    public static final String APP_PREF = "MahycoRetailPreferences";
    public static SharedPreferences sp;
    public static String KEY_AMOUNT = "keyAmount";
    public static String KEY_MOBILE_NUMBER = "mobileNumber";
    public static String KEY_RETAILER_NAME = "retailerName";
    public static String KEY_FIRM_NAME = "firm_name";
    public static String KEY_DEVICE_ID = "deviceId";
    public static String KEY_CART_ITEMS = "cartItems";
    public static String KEY_OTP = "otp";
    public static String KEY_ISPROFILEUPDATED = "profileUpdate";
    public static String KEY_IS_VERIFIED = "isVerified";
    public static String KEY_APPVERSION_FIREBASE = "appVersionCode";
    public static String KEY_APPVERSION_NAME_FIREBASE = "appVersionName";
    public static String KEY_IS_ALL_DETAILS_AVAIL = "isAllDetailsAvail";

    public static String KEY_RETAILER_ID = "keyRetailerId";
    public static String PAYMENT_MODE = "payMode";
    public static String KEY_FCM_TOKEN = "fcmToken";
    public static String KEY_BADGE_COUNT = "badge";
    public static String KEY_USER_ID = "keyuserid";
    public static String KEY_SALES_TYPE = "keysalestype";
    public static String PAYMENT_ID = "payment_id";
    public static String ORDER_ID = "order_id";
    public static String PAYMENT_STATUS = "payment_status";
    public static String KEY_EMAIL = "email";

    public static String LATITUDE = "latitude";
    public static String LONGITUDE = "longitude";
    public static String CITY_NAME = "cityName";
    public static String KEY_VERSION_CODE = "appVersionCode";
    public static String KEY_TOPIC_SUBSCRIBED = "topicSubscribed";
    public static String KEY_FCM_TOKEN_SYNCED = "tokenSynced";
    public static String KEY_ACC_NO = "accountNo";
    public static String KEY_SHIPPING_ADD_ID = "shippingAddId";
    public static String KEY_WALLET_BALANCE = "walletBalance";

    // For crop details
    public static String KEY_CROP_DETAILS = "cropDetails";

    public static final String PREF_LANG = "lang";
//    public static void saveArrayList(Context context, String key, ArrayList<SKUModelList> value) {
//        try {
//            sp = context.getSharedPreferences(APP_PREF, 0);
//            SharedPreferences.Editor edit = sp.edit();
//            Gson gson = new Gson();
//            Type type = new TypeToken<ArrayList<SKUModelList>>() {
//            }.getType();
//
//            String jsonData = gson.toJson(value, type);
//            edit.putString(key, jsonData);
//
//            edit.commit();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

//    public static ArrayList<SKUModelList> getArrayList(Context context, String key) {
//        Gson gson = new Gson();
//        sp = context.getSharedPreferences(APP_PREF, 0);
//        String json = sp.getString(key, null);
//        Log.d("cart", "getArrayList: " + json);
//        Type type = new TypeToken<ArrayList<SKUModelList>>() {
//        }.getType();
//        // ArrayList<JSONObject> arrayList = gson.fromJson(json, type);
//        return gson.fromJson(json, type);
//    }

    public static void saveBool(Context context, String key, boolean value) {
        sp = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor edit = sp.edit();
        edit.putBoolean(key, value);
        edit.commit();
    }

    public static Boolean getBool(Context context, String key) {
        sp = context.getSharedPreferences(APP_PREF, 0);
        boolean val = sp.getBoolean(key, false);
        return val;
    }

    public static int getInt(Context context, String key) {
        sp = context.getSharedPreferences(APP_PREF, 0);
        int userId = sp.getInt(String.valueOf(key), 0);
        return userId;
    }

    public static void saveInt(Context context, String key, int value) {
        sp = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor edit = sp.edit();
        edit.putInt(key, value);
        edit.commit();
    }

    public static void save(Context context, String key, String value) {
        sp = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor edit = sp.edit();
        edit.putString(key, value);
        edit.commit();
    }

    public static String get(Context context, String key) {
        sp = context.getSharedPreferences(APP_PREF, 0);
        String userId = sp.getString(key, "");
        return userId;
    }

    public static void clearPreference(Context context) {
        sp = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor edit = sp.edit();
        edit.clear();
        edit.commit();
    }

    public static void logOut(Context context) {

        // clearPreference(context);
        Preferences.saveBool(context, Constants.IS_LOGGED_IN, false);
        Intent intent = new Intent(context,
                login.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
        ((Activity) context).finish();
    }
}
