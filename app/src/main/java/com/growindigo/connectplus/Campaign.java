package com.growindigo.connectplus;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.growindigo.connectplus.databaseutils.DBCreation;
import com.growindigo.connectplus.databaseutils.Messageclass;
import com.growindigo.connectplus.model.DistributorModel;
import com.growindigo.connectplus.model.RetailerModel;
import com.growindigo.connectplus.utils.CheckConnection;
import com.growindigo.connectplus.utils.Constants;
import com.growindigo.connectplus.utils.Preferences;
import com.growindigo.connectplus.utils.Utils;
import com.growindigo.connectplus.volley.ApiRequest;
import com.growindigo.connectplus.volley.IApiResponse;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class Campaign extends AppCompatActivity implements IApiResponse {
   SearchableSpinner spnDisributorList,spnRetailerList;
   EditText txtVillage,txtFdate,txtTdate,txtBudget,txtcrop;
    String currentDateandTime="",ReferanceFrom;
    Button btn_next;
    DBCreation db;
    DatePickerDialog picker;
    Calendar myCalendar;
    ArrayList<DistributorModel> mDistributor_list;
    ArrayList<DistributorModel> mDistributor_code;

    ArrayList<RetailerModel> mRetailer_list;
    ArrayList<RetailerModel> mRetailer_code;

    String distcode="";
    String retailercode="";

    String up_Campaign_type,up_dist_name,up_dist_code,up_ret_name,up_ret_code,up_village,up_FromDate,
            up_ToDate,up_budget,up_crop,up_usercode,up_tr_date;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_campaign);
        Initializecontrol();
        Intent i = getIntent();
        ReferanceFrom=i.getStringExtra("ref_From");
        if(ReferanceFrom.equals("jeep")) {
            getSupportActionBar().setTitle("Jeep Campaign");
           txtcrop.setVisibility(View.GONE);
        }
        else if(ReferanceFrom.equals("meet"))
        {
            getSupportActionBar().setTitle("Meeting");
            txtcrop.setVisibility(View.VISIBLE);
        }
        loadspinnerdata();


        spnRetailerList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                retailercode= mRetailer_code.get(position).getRetailerCode();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spnDisributorList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                distcode= mDistributor_code.get(position).getDistributorCode();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {


            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                // TODO Auto-generated method stub

                String myFormat = "dd-MM-yyyy"; //In which you need put here
                SimpleDateFormat sdformat = new SimpleDateFormat(myFormat, Locale.US);
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                txtFdate.setText(sdformat.format(myCalendar.getTime()));
            }

        };

        final DatePickerDialog.OnDateSetListener date1 = new DatePickerDialog.OnDateSetListener() {


            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                // TODO Auto-generated method stub

                String myFormat = "dd-MM-yyyy"; //In which you need put here
                SimpleDateFormat sdformat = new SimpleDateFormat(myFormat, Locale.US);
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                txtTdate.setText(sdformat.format(myCalendar.getTime()));

            }

        };

        txtFdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(Campaign.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        txtTdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new DatePickerDialog(Campaign.this, date1, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });



        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userCode = Preferences.get(Campaign.this, Constants.PREF_USERCODE);
                boolean result=db.insertCampaign(ReferanceFrom,spnDisributorList.getSelectedItem().toString(),distcode,
                        spnRetailerList.getSelectedItem().toString(),retailercode,txtVillage.getText().toString(),
                        txtFdate.getText().toString(),txtTdate.getText().toString(),txtBudget.getText().toString(),
                        txtcrop.getText().toString(),userCode,currentDateandTime);
                if (result) {
                    Utils.showAlertDialogOk("SUCCESS", "Data Save Successfully", Campaign.this);
                } else {
                    Utils.showAlertDialogOk("Error", "Something Went Wrong", Campaign.this);
                }
                ////////////////////////////////////
                if (CheckConnection.isNetworkAvailable(Campaign.this)) {
                    String searchQuery = "select  *  from Campaign where  uploaded ='0'";
                    Cursor cursor = db.getReadableDatabase().rawQuery(searchQuery, null);
                    int count = cursor.getCount();
                    if (count > 0) {
                        if (cursor.moveToFirst()) {
                            do {
                                up_Campaign_type = cursor.getString(cursor.getColumnIndex("Campaign_type"));
                                up_dist_name = cursor.getString(cursor.getColumnIndex("dist_name"));
                                up_dist_code = cursor.getString(cursor.getColumnIndex("dist_code"));
                                up_ret_name = cursor.getString(cursor.getColumnIndex("ret_name"));
                                up_ret_code = cursor.getString(cursor.getColumnIndex("ret_code"));
                                up_village = cursor.getString(cursor.getColumnIndex("village"));
                                up_FromDate = cursor.getString(cursor.getColumnIndex("FromDate"));
                                up_ToDate = cursor.getString(cursor.getColumnIndex("ToDate"));
                                up_budget = cursor.getString(cursor.getColumnIndex("budget"));
                                up_crop = cursor.getString(cursor.getColumnIndex("crop"));
                                up_usercode = cursor.getString(cursor.getColumnIndex("usercode"));
                                up_tr_date = cursor.getString(cursor.getColumnIndex("tr_date"));

                                uploadData(up_Campaign_type,up_dist_name,up_dist_code,up_ret_name,up_ret_code,up_village,up_FromDate,
                                        up_ToDate,up_budget,up_crop,up_usercode,up_tr_date);
                            }
                            while (cursor.moveToNext());
                        }
                    } else {
                        Toast.makeText(Campaign.this, "Data Will Upload When Internet Available", Toast.LENGTH_SHORT).show();
                    }
                }

////////////////////////////////////
            }
        });
    }

    protected void Initializecontrol() {
        myCalendar = Calendar.getInstance();
        spnDisributorList = (SearchableSpinner) findViewById(R.id.spnDisributorList);
        spnRetailerList = (SearchableSpinner) findViewById(R.id.spnRetailerList);
        txtVillage = (EditText) findViewById(R.id.txtVillage);
        txtFdate=(EditText)findViewById(R.id.txtFdate);
        txtTdate=(EditText)findViewById(R.id.txtTdate);
        txtBudget=(EditText)findViewById(R.id.txtBudget);
        txtcrop=(EditText)findViewById(R.id.txtcrop);
        btn_next=(Button)findViewById(R.id.btn_next);
        db=new DBCreation(this);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        currentDateandTime = sdf.format(new Date());
    }

    protected void loadspinnerdata()
    {

        //Distributor List and code
            mDistributor_code=db.getDistributorListData();
            List<String> dist_code=new ArrayList<>();
            //dist_code.add("0");
            for (int j=0;j<mDistributor_code.size();j++)
            {
                dist_code.add(mDistributor_code.get(j).getDistributorCode());
            }

            mDistributor_list = db.getDistributorListData();
            List<String> spnDisributor_list = new ArrayList<String>();
            //spnDisributor_list.add("Select Distributor");
            for (int i = 0; i < mDistributor_list.size(); i++) {
                spnDisributor_list.add(mDistributor_list.get(i).getDistributorName());
            }
            spnDisributorList.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, spnDisributor_list));

            //Retailer list and code
            mRetailer_code=db.getRetailerListData();
            List<String> ret_code=new ArrayList<>();
            //dist_code.add("0");
            for (int j=0;j<mRetailer_code.size();j++)
            {
                ret_code.add(mRetailer_code.get(j).getRetailerCode());
            }

            mRetailer_list = db.getRetailerListData();
            List<String> spnRetialer_list = new ArrayList<String>();
            //spnDisributor_list.add("Select Distributor");
            for (int i = 0; i < mRetailer_list.size(); i++) {
                spnRetialer_list.add(mRetailer_list.get(i).getRetailerName());
            }
            spnRetailerList.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, spnRetialer_list));
        }

    public void uploadData(String Campaign_type,String dist_name,String dist_code,String ret_name,String ret_code,
                           String village,String FromDate,String ToDate,String budget,String crop,
                           String usercode,String tr_date) {


        Map<String, String> paramsReq = new HashMap<String, String>();

        paramsReq.put("Campaign_type", Campaign_type);
        paramsReq.put("dist_name", dist_name);
        paramsReq.put("dist_code", dist_code);
        paramsReq.put("ret_name", ret_name);
        paramsReq.put("ret_code", ret_code);
        paramsReq.put("village", village);
        paramsReq.put("FromDate", FromDate);
        paramsReq.put("ToDate", ToDate);
        paramsReq.put("budget", budget);
        paramsReq.put("crop", crop);
        paramsReq.put("usercode", usercode);
        paramsReq.put("tr_date", tr_date);
        db.updateCampaign(tr_date, "1");
        apiRequest(paramsReq, Constants.Campaign_upload, Request.Method.POST);
    }

    private void apiRequest(Map<String, String> paramsReq, String tag, int method) {
        ApiRequest apiRequest = new ApiRequest(this, this);
        apiRequest.postJSONRequest(Constants.BASE_URL + Constants.Campaign_upload, tag,
                paramsReq, method);
    }
    @Override
    public void onResultReceived(String response, String tag_json_obj) {
      //  Toast.makeText(Campaign.this, "Data Uploaded Successfully", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        //Log.d("Error::", "error:: " + error);
    }
}
