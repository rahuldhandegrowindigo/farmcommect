package com.growindigo.connectplus.volley;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.growindigo.connectplus.R;
import com.growindigo.connectplus.utils.CheckConnection;
import com.growindigo.connectplus.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ApiRequest {

    Context oContext;
    IApiResponse apiResponse;
    ProgressDialog pDialog;
    String TAG = "ApiRequest";

    public ApiRequest(Context oContext, IApiResponse apiResponse) {
        this.oContext = oContext;
        this.apiResponse = apiResponse;
    }

    public static long getMinutesDifference(long timeStart, long timeStop) {
        long diff = timeStop - timeStart;
        long diffMinutes = diff / (60 * 1000);
        return diffMinutes;
    }

    public static byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 95, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public void postRequest1(final String url, final String tag_json_obj, final Map<String, String> paramsReq, int method) {
        System.out.println("URLL::" + url);
        if (CheckConnection.isNetworkAvailable(oContext)) {

        /*    final ProgressDialog pDialog = new ProgressDialog(oContext);
            pDialog.setMessage(oContext.getString(R.string.Loading));
            pDialog.show();*/
            final StringRequest strReq = new StringRequest(method, url, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                   // Log.d("Push Response POST", response.toString());
                    apiResponse.onResultReceived(response, tag_json_obj);
                  /*  if (pDialog != null && pDialog.isShowing())
                        pDialog.hide();*/
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    // imgSendNotification.setVisibility(ImageView.);
                    apiResponse.onErrorResponse(error);
                    VolleyLog.e("Push Response Error", "Error: " + error.getMessage());
                  /*  if (pDialog != null && pDialog.isShowing())
                        pDialog.hide();*/
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    return paramsReq;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/x-www-form-urlencoded");
                    return params;
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                        String json = null;
                        try {
                            String responseBody = new String(volleyError.networkResponse.data);
                            JSONObject jsonObject = new JSONObject(responseBody);

                            //Log.d("parse jsonObject", jsonObject.toString());
                            json = trimMessage(jsonObject, "message");
                            if (json != null)
                                displayMessage(json);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    return volleyError;
                }
            };
            strReq.setRetryPolicy(new DefaultRetryPolicy(240 * 1000, 1, 1.0f));
            AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
        } else {
            // Toast.makeText(oContext, oContext.getString(R.string.No_internet_connection), Toast.LENGTH_SHORT).show();
            Utils.showAlertDialog(oContext.getString(R.string.warning), oContext.getString(R.string.No_internet_connection), oContext);

           /* Snackbar.make(findViewById(android.R.id.content), "Had a snack at Snackbar", Snackbar.LENGTH_LONG)
                    .setAction("Undo", mOnClickListener)
                    .setActionTextColor(Color.RED)
                    .show();*/
        }
    }

    /////
    public void postRequest(final String url, final String tag_json_obj, final Map<String, String> paramsReq, int method, boolean isFirstLoad) {
        if (CheckConnection.isNetworkAvailable(oContext)) {
            final ProgressDialog pDialog = new ProgressDialog(oContext);
            if (isFirstLoad) {
                if (pDialog != null) {
                    pDialog.setMessage(oContext.getString(R.string.Loading));

                    pDialog.show();
                }
            } else {
                pDialog.dismiss();
            }
            final StringRequest strReq = new StringRequest(method, url, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    apiResponse.onResultReceived(response, tag_json_obj);


                    if (pDialog != null && pDialog.isShowing())
                        pDialog.dismiss();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    apiResponse.onErrorResponse(error);
                    VolleyLog.e("Push Response Error", "Error: " + error.getMessage());
                    if (pDialog != null && pDialog.isShowing())
                        pDialog.dismiss();
                    displayMessage(error.getMessage());
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    return paramsReq;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
//                    String credentials = Constants.USER_NAME + ":" + Constants.PASSWORD;
//
//                    String base64EncodedCredentials =
//                            Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                    params.put("Content-Type", "application/x-www-form-urlencoded");
                    //    params.put("Authorization", "Basic " + base64EncodedCredentials);
                    return params;
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                        String json = null;
                        try {
                            String responseBody = new String(volleyError.networkResponse.data);
                            JSONObject jsonObject = new JSONObject(responseBody);
                            //Log.d("parse jsonObject", jsonObject.toString());
                            json = trimMessage(jsonObject, "message");
                            if (json != null)
                                displayMessage(json);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    return volleyError;
                }
            };
            strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 100, 1, 1.0f));
            AppController.getInstance().addToRequestQueue(strReq);
        } else {
            // AlertManager.showToast(oContext, oContext.getString(R.string.Pleasemakesureyourinternetconnectionisenabled));
            Utils.showAlertDialog("Error", oContext.getString(R.string.No_internet_connection), oContext);
        }
    }

    ////
    public void postRequest(String url, final String tag_json_obj, final Map<String, String> paramsReq, int method) {
        //System.out.println("paramsReq::" + paramsReq);
        if (CheckConnection.isNetworkAvailable(oContext)) {

            final ProgressDialog pDialog = new ProgressDialog(oContext);
            pDialog.setMessage(oContext.getString(R.string.Loading));
            pDialog.show();
            //if (method == 0) {
            StringBuilder stringBuilder = new StringBuilder(url);
            Iterator<Map.Entry<String, String>> iterator = paramsReq.entrySet().iterator();
            int i = 1;
            while (iterator.hasNext()) {
                Map.Entry<String, String> entry = iterator.next();
                if (i == 1) {
                    stringBuilder.append("?" + entry.getKey() + "=" + entry.getValue());
                } else {
                    stringBuilder.append("&" + entry.getKey() + "=" + entry.getValue());
                }
                iterator.remove(); // avoids a ConcurrentModificationException
                i++;
            }
            url = stringBuilder.toString();
            url = url.replaceAll(" ", "%20");

            System.out.println("URLL::" + url);

            //}

            final StringRequest strReq = new StringRequest(method, url, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                   // Log.d("Push Response POST", response.toString());

                    if (pDialog != null && pDialog.isShowing())
                        pDialog.cancel();
                    apiResponse.onResultReceived(response, tag_json_obj);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    // imgSendNotification.setVisibility(ImageView.);

                    VolleyLog.e("Push Response Error", "Error: " + error.getMessage());
                    if (pDialog != null && pDialog.isShowing())
                        pDialog.cancel();
                    apiResponse.onErrorResponse(error);
                }
            }) {
                @Override
                protected Map<String, String> getParams() {

                    return paramsReq;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    // params.put("Content-Type", "application/x-www-form-urlencoded");
                    params.put("Content-Type", "application/json");
                    return params;
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                        String json = null;

                        try {
                            String responseBody = new String(volleyError.networkResponse.data);
                            //Log.d(TAG, "parseNetworkError: " + responseBody);
                            JSONObject jsonObject = new JSONObject(responseBody);

                            //Log.d("parse jsonObject", jsonObject.toString());
                            json = trimMessage(jsonObject, "message");
                            if (json != null)
                                displayMessage(json);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    return volleyError;
                }
            };
            strReq.setRetryPolicy(new DefaultRetryPolicy(240 * 1000, 1, 1.0f));
            AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
        } else {

            // Utils.showFancyToast(oContext, oContext.getString(R.string.No_internet_connection), FancyToast.WARNING);
            Utils.showAlertDialog("Error", oContext.getString(R.string.No_internet_connection), oContext);


        }

    }

    private JSONObject getJsonFromMap(Map<String, String> map) throws JSONException {
        JSONObject jsonData = new JSONObject();
        try{
            for (String key : map.keySet()) {
                Object value = map.get(key);
                if (value instanceof Map<?, ?>) {
                    value = getJsonFromMap((Map<String, String>) value);
                }
                jsonData.put(key, value);
            }
        }catch (Exception e){
            //Log.d("Error",e.getMessage());
        }
        //Log.d("Mahyco","JsonDataFromMap: "+jsonData.toString());
        return jsonData;
    }

    public void postJSONRequest(String url, final String tag_json_obj, final Map<String, String> paramsReq, int method) {
        //System.out.println("paramsReq::" + paramsReq);
        //Log.d("paramsReq::", paramsReq.toString());
        if (CheckConnection.isNetworkAvailable(oContext)) {

            final ProgressDialog pDialog = new ProgressDialog(oContext);
            pDialog.setMessage(oContext.getString(R.string.Loading));
            //   pDialog.show();
            if (method == 0) {
                StringBuilder stringBuilder = new StringBuilder(url);
                Iterator<Map.Entry<String, String>> iterator = paramsReq.entrySet().iterator();
                int i = 1;
                while (iterator.hasNext()) {
                    Map.Entry<String, String> entry = iterator.next();
                    if (i == 1) {
                        stringBuilder.append("?" + entry.getKey() + "=" + entry.getValue());
                    } else {
                        stringBuilder.append("&" + entry.getKey() + "=" + entry.getValue());
                    }
                    iterator.remove(); // avoids a ConcurrentModificationException
                    i++;
                }
                url = stringBuilder.toString();
                url = url.replaceAll(" ", "%20");

                System.out.println("URLL::" + url);

            }

            final JsonObjectRequest strReq = new JsonObjectRequest(method, url, new JSONObject(paramsReq), new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    //Log.d("Push Response POST", response.toString());

                    //    if (pDialog != null && pDialog.isShowing())
                    //    pDialog.cancel();
                    apiResponse.onResultReceived(response.toString(), tag_json_obj);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    VolleyLog.e("Push Response Error", "Error: " + error.getMessage());
                    if (pDialog != null && pDialog.isShowing())
                        //  pDialog.cancel();
                        apiResponse.onErrorResponse(error);
//                    error.printStackTrace();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    return paramsReq;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    //params.put("Accept","application/json");
                    params.put("Content-Type", "application/json");
                    return params;
                }

                @Override
                public String getBodyContentType() {
                    return "application/json";
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                        String json = null;

                        try {
                            String responseBody = new String(volleyError.networkResponse.data);
                            //Log.d(TAG, "parseNetworkError: " + responseBody);
                            JSONObject jsonObject = new JSONObject(responseBody);

                            //Log.d("parse jsonObject", jsonObject.toString());
                            json = trimMessage(jsonObject, "message");
                            if (json != null)
                                displayMessage(json);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    return volleyError;
                }
            };
            strReq.setRetryPolicy(new DefaultRetryPolicy(1 * 1000, 1, 1.0f));
            AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
        } else {

            // Utils.showFancyToast(oContext, oContext.getString(R.string.No_internet_connection), FancyToast.WARNING);
            Utils.showAlertDialog("Error", oContext.getString(R.string.No_internet_connection), oContext);
        }

    }


    /*
     * To Submit Sales order :: Added 18 June 2020 5pm*/
    public void postJSONRequestSales(String url, final String tag_json_obj, final Map<String, String> paramsReq, int method) {
        try {

            //System.out.println("paramsReq::" + paramsReq);
            //Log.d("Mahyco paramsReq::", paramsReq.toString());
            if (CheckConnection.isNetworkAvailable(oContext)) {

                final ProgressDialog pDialog = new ProgressDialog(oContext);
                pDialog.setMessage(oContext.getString(R.string.Loading));

                Gson prettyGson = new GsonBuilder().setPrettyPrinting().create();
                String prettyJson = prettyGson.toJson(paramsReq);

                //Log.d("PrettyJSONObject", prettyJson);
                JSONObject crunchifyObject = new JSONObject(prettyJson);
                //Log.d("crunchifyObject", crunchifyObject.toString());

                String fullnfinalJson = crunchifyObject.toString().replace("\\", "");
                String fullnfinalJson1 = fullnfinalJson.replace("\"{", "{");
                String fullnfinalJson2 = fullnfinalJson1.replace("}\"", "}");

                //Log.d("fullnfinalJson", "fullnfinalJson::   "+fullnfinalJson2.toString());

                final JsonObjectRequest strReq = new JsonObjectRequest(method, url, fullnfinalJson2, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                       // Log.d("Push Response POST", response.toString());

                        //    if (pDialog != null && pDialog.isShowing())
                        //    pDialog.cancel();
                        apiResponse.onResultReceived(response.toString(), tag_json_obj);
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        VolleyLog.e("Push Response Error", "Error: " + error.getMessage());
                        if (pDialog != null && pDialog.isShowing())
                            //  pDialog.cancel();
                            apiResponse.onErrorResponse(error);
//                    error.printStackTrace();
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        return paramsReq;
                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        //params.put("Accept","application/json");
                        params.put("Content-Type", "application/json");
                        return params;
                    }

                    @Override
                    public String getBodyContentType() {
                        return "application/json";
                    }

                    @Override
                    protected VolleyError parseNetworkError(VolleyError volleyError) {
                        if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                            String json = null;

                            try {
                                String responseBody = new String(volleyError.networkResponse.data);
                                //Log.d(TAG, "parseNetworkError: " + responseBody);
                                JSONObject jsonObject = new JSONObject(responseBody);

                                //Log.d("parse jsonObject", jsonObject.toString());
                                json = trimMessage(jsonObject, "message");
                                if (json != null)
                                    displayMessage(json);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        return volleyError;
                    }
                };
                strReq.setRetryPolicy(new DefaultRetryPolicy(240 * 1000, 1, 1.0f));
                AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
            } else {

                // Utils.showFancyToast(oContext, oContext.getString(R.string.No_internet_connection), FancyToast.WARNING);
                Utils.showAlertDialog("Error", oContext.getString(R.string.No_internet_connection), oContext);

            }
        }catch (Exception e){
            //Log.d("Data",e.getMessage());
        }
    }



    public void postRequestObject(final String url, final String tag, final JSONObject paramsReq, int method) {
        if (CheckConnection.isNetworkAvailable(oContext)) {
            pDialog = new ProgressDialog(oContext);
            if (pDialog != null) {
                pDialog.setMessage("Loading...");
                pDialog.setCancelable(false);
                pDialog.show();

                JSONObject parameters = null;
                try {
                    parameters = new JSONObject(String.valueOf(paramsReq));
                    VolleyLog.d("", "RequestParams: " + parameters.toString());
                    VolleyLog.d("", "URL: " + url.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                JsonObjectRequest req = new JsonObjectRequest(method, url,
                        parameters, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        if (pDialog != null && pDialog.isShowing())
                            pDialog.cancel();
                        apiResponse.onResultReceived(response.toString(), tag);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d("", "Error: " + error.getMessage());
                        if (pDialog != null && pDialog.isShowing())
                            pDialog.dismiss();

                    }
                }) {
                    @Override
                    protected VolleyError parseNetworkError(VolleyError volleyError) {
                        if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                            String json = null;
                            try {
                                String responseBody = new String(volleyError.networkResponse.data);
                                JSONObject jsonObject = new JSONObject(responseBody);
                                json = trimMessage(jsonObject, "message");
                                if (jsonObject != null) {
                                    if (jsonObject.has("error")) {
                                        displayMessage(json);
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        return volleyError;
                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        //params.put("Content-Type", "application/json");

                        return params;
                    }
                };

                req.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 10000, 1, 1.0f));
                AppController.getInstance().addToRequestQueue(req);
            }
        } else {

            // Utils.showFancyToast(oContext, oContext.getString(R.string.No_internet_connection), FancyToast.WARNING);
            Utils.showAlertDialog("Error", oContext.getString(R.string.No_internet_connection), oContext);


        }

    }

    public String trimMessage(JSONObject obj, String key) {
        String trimmedString = null;

        try {
            // JSONObject obj = new JSONObject(json);
            JSONObject errorObj = obj.getJSONObject("error");
            trimmedString = errorObj.getString(key);
            int statusCode = errorObj.getInt("code");
            if (trimmedString.equalsIgnoreCase("Token has expired") || trimmedString.equalsIgnoreCase("Unauthorized.")) {
                //Preferences.logOut(oContext);
            }

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return trimmedString;
    }

    public void displayMessage(final String toastString) {
//        Log.d("message", toastString);

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {

            @Override
            public void run() {
//                Toast.makeText(oContext, oContext.getString(R.string.No_internet_connection), Toast.LENGTH_SHORT).show();

                Utils.showAlertDialog(oContext.getString(R.string.warning), oContext.getString(R.string.No_internet_connection), oContext);

            }
        });
    }

    public void postRequestAceept(final String url, final String tag_json_obj, final Map<String, String> paramsReq, int method) {
        System.out.println("URLL::" + url);
        System.out.println("paramsReq::" + paramsReq);
        if (CheckConnection.isNetworkAvailable(oContext)) {

            pDialog = new ProgressDialog(oContext);
            pDialog.setMessage(oContext.getString(R.string.Loading));
//            pDialog.show();
            final StringRequest strReq = new StringRequest(method, url, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    //Log.d("Push Response POST", response.toString());
                    apiResponse.onResultReceived(response, tag_json_obj);
                    if (pDialog != null && pDialog.isShowing())
                        pDialog.hide();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    apiResponse.onErrorResponse(error);
                    VolleyLog.e("Push Response Error", "Error: " + error.getMessage());
                    if (pDialog != null && pDialog.isShowing())
                        pDialog.hide();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {

                    return paramsReq;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Accept", "application/json");
                    params.put("Content-Type", "application/x-www-form-urlencoded");
                    return params;
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                        String json = null;

                        try {
                            String responseBody = new String(volleyError.networkResponse.data);
                            JSONObject jsonObject = new JSONObject(responseBody);

                            //Log.d("parse jsonObject", jsonObject.toString());
                            json = trimMessage(jsonObject, "message");
                            if (json != null)
                                displayMessage(json);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    return volleyError;
                }
            };
            strReq.setRetryPolicy(new DefaultRetryPolicy(240 * 1000, 1, 1.0f));
            AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
        } else {

            Utils.showAlertDialog(oContext.getString(R.string.warning), oContext.getString(R.string.No_internet_connection), oContext);

        }
    }

    public void postRequestAceeptPage(final int page, final String url, final String tag_json_obj, final Map<String, String> paramsReq, int method) {
        System.out.println("URLL::" + url);
        System.out.println("paramsReq::" + paramsReq);
        if (CheckConnection.isNetworkAvailable(oContext)) {
            final ProgressDialog pDialog = new ProgressDialog(oContext);
            if (page == 0) {
                pDialog.setMessage(oContext.getString(R.string.Loading));
                pDialog.show();
            }
            final StringRequest strReq = new StringRequest(method, url, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    //Log.d("Push Response POST", response.toString());
                    apiResponse.onResultReceived(response, tag_json_obj);
                    if (pDialog != null && pDialog.isShowing())
                        pDialog.hide();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    // imgSendNotification.setVisibility(ImageView.);
                    apiResponse.onErrorResponse(error);
                    VolleyLog.e("Push Response Error", "Error: " + error.getMessage());
                    if (pDialog != null && pDialog.isShowing())
                        pDialog.hide();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {

                    return paramsReq;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Accept", "application/json");
                    params.put("Content-Type", "application/x-www-form-urlencoded");
                    return params;
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                        String json = null;

                        try {
                            String responseBody = new String(volleyError.networkResponse.data);
                            JSONObject jsonObject = new JSONObject(responseBody);

                            //Log.d("parse jsonObject", jsonObject.toString());
                            json = trimMessage(jsonObject, "message");
                            if (json != null)
                                displayMessage(json);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    return volleyError;
                }
            };
            strReq.setRetryPolicy(new DefaultRetryPolicy(240 * 1000, 1, 1.0f));
            AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
        } else {
            // Toast.makeText(oContext, oContext.getString(R.string.No_internet_connection), Toast.LENGTH_SHORT).show();

            Utils.showAlertDialog(oContext.getString(R.string.warning), oContext.getString(R.string.No_internet_connection), oContext);

        }
    }


    public void postRequestFirebase(final String url, final String tag_json_obj, final Map<String, String> paramsReq, int method) {
        System.out.println("URLL::" + url);
        System.out.println("paramsReq::" + paramsReq);
        if (CheckConnection.isNetworkAvailable(oContext)) {
            final StringRequest strReq = new StringRequest(method, url, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    //Log.d("Push Response POST", response.toString());
                    apiResponse.onResultReceived(response, tag_json_obj);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    // imgSendNotification.setVisibility(ImageView.);
                    apiResponse.onErrorResponse(error);
                    VolleyLog.e("Push Response Error", "Error: " + error.getMessage());
                }
            }) {
                @Override
                protected Map<String, String> getParams() {

                    return paramsReq;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Accept", "application/json");
                    params.put("Content-Type", "application/x-www-form-urlencoded");
                    return params;
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    }
                    return volleyError;
                }
            };
            strReq.setRetryPolicy(new DefaultRetryPolicy(240 * 1000, 1, 1.0f));
            AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
        }
    }

    public void hideLoaderDialog() {
        if (pDialog != null && pDialog.isShowing())
            pDialog.dismiss();
    }
}
