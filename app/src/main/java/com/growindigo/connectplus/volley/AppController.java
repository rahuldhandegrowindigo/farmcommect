package com.growindigo.connectplus.volley;

import android.content.Context;
import android.text.TextUtils;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AppController extends MultiDexApplication {

    public static final String TAG = AppController.class
            .getSimpleName();
    private static AppController mInstance;
    private static Context mContext;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public static Context getContext() {
        return mContext;
    }

    public static <T> List<T> getGsonList(String s, Class<T[]> clazz) {
        T[] arr = new Gson().fromJson(s, clazz);
        return Arrays.asList(arr);
    }

    public static <T> ArrayList<T> getGsonArrayList(String s, Class<T[]> clazz) {
        T[] arr = new Gson().fromJson(s, clazz);
        return new ArrayList<T>(Arrays.asList(arr));
    }

    public static <T> Object getGsonObject(String s, Class<T> clazz) {
        Object object = new Gson().fromJson(s, clazz);
        return object;
    }


    public static String getStringFromArray(ArrayList<?> arrayList) {
        String jsonString = "";

        jsonString = new Gson().toJson(arrayList);
        return jsonString;
    }

    public static String getStringFromArray(List<?> arrayList) {
        String jsonString = "";

        jsonString = new Gson().toJson(arrayList);
        return jsonString;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        mContext = this;
      //  FacebookSdk.sdkInitialize(getApplicationContext());
      //  AppEventsLogger.activateApp(this);


        // Mint.initAndStartSession(getBaseContext(), "149cbfd2");
        //StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        // StrictMode.setVmPolicy(builder.build());

    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}