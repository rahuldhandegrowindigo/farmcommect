package com.growindigo.connectplus;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.growindigo.connectplus.utils.Utils;

import java.util.ArrayList;

public class salesorderAdaptor extends RecyclerView.Adapter<salesorderAdaptor.RecyclerItemViewHolder>{
    @NonNull
    private ArrayList<sales_order_class> myList;
    int mLastPosition = 0;
    private RemoveClickListner mListner;

    public salesorderAdaptor(ArrayList<sales_order_class> myList,RemoveClickListner listner) {
        this.myList = myList;
        mListner=listner;
    }
    @Override
    public salesorderAdaptor.RecyclerItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.salesorderlist, parent, false);
        RecyclerItemViewHolder holder = new RecyclerItemViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull salesorderAdaptor.RecyclerItemViewHolder holder, int position) {
        //Log.d("onBindViewHoler ", myList.size() + "");
        holder.txtProduct.setText(myList.get(position).getTxtProduct());
        holder.txtProductcode.setText(myList.get(position).getTxtProductcode());

        holder.txtPrice.setText(myList.get(position).getTxtPrice());
        holder.txtunit.setText(myList.get(position).getTxtunit());

        mLastPosition =position;
    }

    @Override
    public int getItemCount() {
        return(null != myList?myList.size():0);
    }
    public void notifyData(ArrayList<sales_order_class> myList) {
        //Log.d("notifyData ", myList.size() + "");
        this.myList = myList;
        notifyDataSetChanged();
    }
    public class RecyclerItemViewHolder extends RecyclerView.ViewHolder {

        private TextView txtProduct;
        private TextView txtProductcode;
        private TextView txtunit;
        private TextView txtPrice;
        private LinearLayout mainLayout;
        private ImageView imgDelete;
        public RecyclerItemViewHolder(final View itemView) {
            super(itemView);

            txtProduct=(TextView)itemView.findViewById(R.id.txtProduct);
            txtProductcode=(TextView)itemView.findViewById(R.id.txtProductcode);
            txtunit=(TextView)itemView.findViewById(R.id.txtunit);
            txtPrice=(TextView)itemView.findViewById(R.id.txtPrice);
            mainLayout = (LinearLayout) itemView.findViewById(R.id.mainLayout);
            imgDelete=(ImageView)itemView.findViewById(R.id.imgDelete);

            imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(itemView.getContext());
                    builder.setMessage("Do you want to delete this product?");
                    builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            mListner.OnRemoveClick(getAdapterPosition());
                            dialog.dismiss();
                        }
                    });

                    builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            });
        }
    }
    public interface RemoveClickListner {
        void OnRemoveClick(int index);
    }

}
