package com.growindigo.connectplus;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.TextView;

import com.growindigo.connectplus.databaseutils.DBCreation;
import com.growindigo.connectplus.databaseutils.Messageclass;

import java.util.ArrayList;

public class report extends AppCompatActivity {
    public RecyclerView recyclerList;
    com.growindigo.connectplus.ReportVisitAdaptor ReportVisitAdaptor;
    DBCreation db;
    String MobileNo;
    Messageclass msclass;
    public TextView txt_fnamedetails,txt_MobileDetails,txt_CropDetails,txt_AreaDetails,txt_SowingDateDetails;
    String str_fnamedetails,str_MobileDetails,str_CropDetails,str_AreaDetails,str_SowingDateDetails;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        Intent i = getIntent();
        //farmerName.setText(i.getStringExtra("farmerName"));
        MobileNo =i.getStringExtra("Mobile");

        txt_fnamedetails=(TextView)findViewById(R.id.txt_fnamedetails);
        txt_MobileDetails=(TextView)findViewById(R.id.txt_MobileDetails);
        txt_CropDetails=(TextView)findViewById(R.id.txt_CropDetails);
        txt_AreaDetails=(TextView)findViewById(R.id.txt_AreaDetails);
        txt_SowingDateDetails=(TextView)findViewById(R.id.txt_SowingDateDetails);
        msclass = new Messageclass(this);
        db=new DBCreation(this);
        userinfo();
        recyclerList = (RecyclerView) findViewById(R.id.recycler1);
        db=new DBCreation(this);
        LinearLayoutManager lllm = new LinearLayoutManager(this);
        lllm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerList.setLayoutManager(lllm);

        ArrayList<ReportVisitModel> mlist = db.getVisitData(MobileNo);
        ReportVisitAdaptor = new ReportVisitAdaptor(mlist,this,getSupportFragmentManager());
        recyclerList.setAdapter(ReportVisitAdaptor);
    }

    public void userinfo()
    {
        Cursor data = db.userinfo(MobileNo);

        if (data.getCount() == 0) {
            msclass.showMessage("No Data Available... ");
        } else {
            data.moveToFirst();
            if (data != null) {
                do {
                    str_fnamedetails = data.getString(data.getColumnIndex("FarmerName"));
                    str_MobileDetails = data.getString(data.getColumnIndex("Mobile"));
                    str_CropDetails = data.getString(data.getColumnIndex("crop"));
                    str_AreaDetails = data.getString(data.getColumnIndex("area"));
                    str_SowingDateDetails = data.getString(data.getColumnIndex("sowingDate"));

                } while (data.moveToNext());
                txt_fnamedetails.setText(str_fnamedetails);
                txt_MobileDetails.setText(str_MobileDetails);
                txt_CropDetails.setText(str_CropDetails);
                txt_AreaDetails.setText(str_AreaDetails);
                txt_SowingDateDetails.setText(str_SowingDateDetails);
            }
            data.close();
        }

    }
}
