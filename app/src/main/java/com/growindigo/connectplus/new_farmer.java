package com.growindigo.connectplus;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import java.io.IOException;
import java.util.Map;


import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.growindigo.connectplus.databaseutils.DBCreation;
import com.growindigo.connectplus.databaseutils.Messageclass;
import com.growindigo.connectplus.databaseutils.listadaptor;
import com.growindigo.connectplus.model.DistributorModel;
import com.growindigo.connectplus.model.DistrictModel;
import com.growindigo.connectplus.model.StateModel;
import com.growindigo.connectplus.model.TalukaModel;
import com.growindigo.connectplus.model.listModel;
import com.growindigo.connectplus.utils.CheckConnection;
import com.growindigo.connectplus.utils.Constants;
import com.growindigo.connectplus.utils.Preferences;
import com.growindigo.connectplus.utils.Utils;
import com.growindigo.connectplus.volley.ApiRequest;
import com.growindigo.connectplus.volley.IApiResponse;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.enums.EPickType;
import com.vansuita.pickimage.listeners.IPickResult;

/**
 * A simple {@link Fragment} subclass.
 */
public class new_farmer extends Fragment implements IPickResult, LocationListener, IApiResponse {
    public EditText txtFarmer, txtFarmerVillage, txtFarmerPincode;
    public EditText txtFarmerMobile, txtFarmerArea, txtFarmerAddress;
    private ScrollView addfarmerScrollView;
    private Button btn_addfarmer, btn_FarmerPhoto, btn_farmerSave;
    private GoogleMap mMap;
    LocationManager locationManager;
    DBCreation db;
    private double longitude;
    private double latitude;
    private SearchableSpinner spnIrrigation;
    private SearchableSpinner spnDisributor;
    private Switch switchgeo;
    private ImageView geoicon, capureImage;
    private static final int pic_id = 123;
    Messageclass msclass;
    String cordinates = "";
    String currentDateandTime = "";
    int imageselect;
    File photoFile = null;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    ImageView ivImage;
    EditText txtpalce;
    PickSetup setup;
    private static final String IMAGE_DIRECTORY_NAME = "VISITPHOTO";
    private String Imagepath1, spnStateCode, spnDistrictCode, spnTalukaCode;

    String up_farmerName, up_Mobile, up_state, up_district, up_taluka, up_village, up_pincode,
            up_Territory, up_Territory_code, up_area, up_cordinate, up_geoaddress, up_sourceofIrrigation,
            up_distributor, up_img, up_visitno, up_usercode, up_tr_date, up_uploaded;

    SearchableSpinner spnState, spnDistrict, spnTaluka;
    ArrayList<StateModel> mState_list;
    ArrayList<StateModel> mState_Code;
    ArrayList<DistrictModel> mDistrict_list;
    ArrayList<DistrictModel> mDistrict_Code;
    ArrayList<TalukaModel> mTaluka_list;
    ArrayList<TalukaModel> mTaluka_Code;
    Context context;
    List<String> spnStateCodeList;
    List<String> spnDistrictCodeList;
    List<String> spntalukaCodeList;

    public new_farmer() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_new_farmer, container, false);
        getActivity().setTitle("New Farmer");
        txtFarmer = (EditText) rootView.findViewById(R.id.txtFarmerName);
        txtFarmerAddress = (EditText) rootView.findViewById(R.id.txtFarmerAddress);
        context = this.getActivity();
        setup = new PickSetup()
                .setTitle("Field Connect App")
                .setFlip(false)
                .setMaxSize(500)
                .setPickTypes(EPickType.CAMERA)
                .setCameraButtonText("Click Photo")
                // .setGalleryButtonText("selectfromgallery")
                .setSystemDialog(false);

        spnState = (SearchableSpinner) rootView.findViewById(R.id.spnState);
        spnDistrict = (SearchableSpinner) rootView.findViewById(R.id.spnDistrict);
        spnTaluka = (SearchableSpinner) rootView.findViewById(R.id.spnTaluka);


        txtFarmerVillage = (EditText) rootView.findViewById(R.id.txtFarmerVillage);
        txtFarmerPincode = (EditText) rootView.findViewById(R.id.txtFarmerPincode);
        txtFarmerMobile = (EditText) rootView.findViewById(R.id.txtFarmerMobile);
        txtFarmerArea = (EditText) rootView.findViewById(R.id.txtFarmerArea);
        addfarmerScrollView = (ScrollView) rootView.findViewById(R.id.addfarmerScroll);
        btn_addfarmer = (Button) rootView.findViewById(R.id.btn_addfarmer);
        btn_FarmerPhoto = (Button) rootView.findViewById(R.id.btn_FarmerPhoto);
        btn_farmerSave = (Button) rootView.findViewById(R.id.btn_farmerSave);
        spnIrrigation = (SearchableSpinner) rootView.findViewById(R.id.spnIrrigation);
        spnDisributor = (SearchableSpinner) rootView.findViewById(R.id.spnDisributor);
        switchgeo = (Switch) rootView.findViewById(R.id.switchgeo);
        geoicon = (ImageView) rootView.findViewById(R.id.geoicon);
        ivImage = (ImageView) rootView.findViewById(R.id.ivImage);
        addfarmerScrollView.setVisibility(View.INVISIBLE);
        msclass = new Messageclass(this.getContext());
        db = new DBCreation(this.getContext());
        locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        getLocation();

        ArrayList<DistributorModel> mDistributor_list = db.getDistributorListData();
        mState_list = db.getStateListData();
        mState_Code = db.getStateListData();
        mDistrict_list = db.getDistrictListData();
        mDistrict_Code = db.getDistrictListData();
        //mTaluka_list = db.getTalukaDetailsData();
        //mTaluka_Code = db.getTalukaDetailsData();


        //Log.d("DistSize: ", mDistributor_list.size() + "");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        currentDateandTime = sdf.format(new Date());

        btn_FarmerPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Toast.makeText(context, "Hi", Toast.LENGTH_SHORT).show();

//                if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.CAMERA)
//                        == PackageManager.PERMISSION_DENIED) {
//                    ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.CAMERA}, 101);
//                }
//                PickImageDialog.build(setup).show(getActivity());

                if (ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_DENIED) {
                }
                imageselect = 1;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    captureImage();
                } else {
                    captureImage2();
                }
            }

        });


        txtFarmerMobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (txtFarmerMobile.getText().length() == 10) {
                    ArrayList<listModel> mlist = db.SearchFarmerlistData(txtFarmerMobile.getText().toString());
                    if (mlist.size() <= 0) {

                    } else {
                        Utils.showAlertDialog("Warning", "Farmer Already Available!", getContext());
                    }
                }
            }
        });

        switchgeo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (switchgeo.isChecked()) {
                    geoicon.setColorFilter(getResources().getColor(R.color.colorPrimaryDark));
                } else {
                    geoicon.setColorFilter(getResources().getColor(R.color.black));
                }
            }
        });

        List<String> spnIrrigation_list = new ArrayList<String>();
        spnIrrigation_list.add("Select Source of Irrigation");
        spnIrrigation_list.add("Localized irrigation");
        spnIrrigation_list.add("Drip irrigation");
        spnIrrigation_list.add("Sprinkler irrigation");
        spnIrrigation.setAdapter(new ArrayAdapter<String>(this.getContext(), android.R.layout.simple_spinner_dropdown_item, spnIrrigation_list));

        List<String> spnDisributor_list = new ArrayList<String>();
        spnDisributor_list.add("Select Distributor");
        for (int i = 0; i < mDistributor_list.size(); i++) {
            spnDisributor_list.add(mDistributor_list.get(i).getDistributorName());
        }
        spnDisributor.setAdapter(new ArrayAdapter<String>(this.getContext(), android.R.layout.simple_spinner_dropdown_item, spnDisributor_list));

        /*Bind State District Taluka*/

        spnStateCodeList = new ArrayList<String>();
        spnStateCodeList.add("0");
        for (int i = 0; i < mState_list.size(); i++) {
            spnStateCodeList.add(mState_list.get(i).getStateCode());
        }


        List<String> spnStateList = new ArrayList<String>();
        spnStateList.add("Select State");
        for (int i = 0; i < mState_list.size(); i++) {
            spnStateList.add(mState_list.get(i).getStateName());
        }
        spnState.setAdapter(new ArrayAdapter<String>(this.getContext(), android.R.layout.simple_spinner_dropdown_item, spnStateList));

        /////////////////////////////////////////////////////////////////////////////

        spnDistrictCodeList = new ArrayList<String>();
        spnDistrictCodeList.add("0");
        for (int i = 0; i < mDistrict_list.size(); i++) {
            spnDistrictCodeList.add(mDistrict_list.get(i).getDistrictCode());
        }

        List<String> spnDistrictList = new ArrayList<String>();
        spnDistrictList.add("Select District");
        for (int i = 0; i < mDistrict_list.size(); i++) {
            spnDistrictList.add(mDistrict_list.get(i).getDistrictName());
        }
        spnDistrict.setAdapter(new ArrayAdapter<String>(this.getContext(), android.R.layout.simple_spinner_dropdown_item, spnDistrictList));

        /////////////////////////////////////////////////////////////////////////////

//        spntalukaCodeList = new ArrayList<String>();
//        spntalukaCodeList.add("0");
//        for (int i = 0; i < mTaluka_list.size(); i++) {
//            spntalukaCodeList.add(mTaluka_list.get(i).getTalukaCode());
//        }
//
//
//        List<String> spntalukaList = new ArrayList<String>();
//        spntalukaList.add("Select Taluka");
//        for (int i = 0; i < mTaluka_list.size(); i++) {
//            spntalukaList.add(mTaluka_list.get(i).getTalukaName());
//        }
//        spnTaluka.setAdapter(new ArrayAdapter<String>(this.getContext(), android.R.layout.simple_spinner_dropdown_item, spntalukaList));

        /*Bind State District Taluka End*/

        if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);

        }
        Criteria criteria = new Criteria();
        criteria.setVerticalAccuracy(Criteria.ACCURACY_HIGH);
        criteria.setPowerRequirement(Criteria.POWER_HIGH);
        criteria.setAltitudeRequired(false);
        criteria.setSpeedRequired(false);
        criteria.setCostAllowed(true);
        criteria.setBearingRequired(false);
        getLocation();

//API level 9 and up
        criteria.setHorizontalAccuracy(Criteria.ACCURACY_HIGH);
        criteria.setVerticalAccuracy(Criteria.ACCURACY_HIGH);

        btn_addfarmer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addfarmerScrollView.setVisibility(View.VISIBLE);
                btn_addfarmer.setVisibility(View.GONE);
                getLocation();
            }
        });

        spnState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spnStateCode = spnStateCodeList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spnDistrictCode = spnDistrictCodeList.get(position);
                mTaluka_list = db.getTalukaListData(spnDistrictCode);
                mTaluka_Code = db.getTalukaListData(spnDistrictCode);
                spntalukaCodeList = new ArrayList<String>();
                spntalukaCodeList.add("0");
                for (int i = 0; i < mTaluka_list.size(); i++) {
                    spntalukaCodeList.add(mTaluka_list.get(i).getTalukaCode());
                }

                List<String> spntalukaList = new ArrayList<String>();
                spntalukaList.add("Select Taluka");
                for (int i = 0; i < mTaluka_list.size(); i++) {
                    spntalukaList.add(mTaluka_list.get(i).getTalukaName());
                }
                spnTaluka.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, spntalukaList));

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnTaluka.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                spnTalukaCode = spntalukaCodeList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btn_farmerSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validation() == true) {
                    String userCode = Preferences.get(getContext(), Constants.PREF_USERCODE);
                    boolean result = db.InsertFarmerData(txtFarmer.getText().toString(), spnStateCode,
                            spnDistrictCode, spnTalukaCode, txtFarmerVillage.getText().toString(),
                            txtFarmerPincode.getText().toString(), cordinates, txtFarmerAddress.getText().toString(),
                            txtFarmerMobile.getText().toString(), txtFarmerArea.getText().toString(),
                            spnIrrigation.getSelectedItem().toString(), spnDisributor.getSelectedItem().toString(),
                            Imagepath1, userCode, currentDateandTime, "0");
                    if (result) {
                        //databaseHelper1.updatePlot(Integer.parseInt(txtPlotNo.getText().toString()), Trialcodedata.getText().toString());

                        Utils.showAlertDialogOk("SUCCESS", "Data Save Successfully", getContext());
                        txtFarmerMobile.setText("");
                        txtFarmer.setText("");
                        //FragmentTransaction ft = getFragmentManager().beginTransaction();
                        //ft.detach(new_farmer.this).attach(new_farmer.this).commit();

                    } else {
                        Utils.showAlertDialogOk("Error", "Mobile Number Already Exist", getContext());
                    }

                    if (CheckConnection.isNetworkAvailable(getContext())) {
                        String searchQuery = "select  *  from FarmerMaster where  uploaded ='0'";
                        Cursor cursor = db.getReadableDatabase().rawQuery(searchQuery, null);
                        int count = cursor.getCount();
                        if (count > 0) {
                            if (cursor.moveToFirst()) {
                                do {
                                    up_farmerName = cursor.getString(cursor.getColumnIndex("FarmerName"));
                                    up_Mobile = cursor.getString(cursor.getColumnIndex("Mobile"));
                                    up_state = cursor.getString(cursor.getColumnIndex("State"));
                                    up_district = cursor.getString(cursor.getColumnIndex("Dist"));
                                    up_taluka = cursor.getString(cursor.getColumnIndex("taluka"));
                                    up_village = cursor.getString(cursor.getColumnIndex("village"));
                                    up_pincode = cursor.getString(cursor.getColumnIndex("pincode"));
                                    up_area = cursor.getString(cursor.getColumnIndex("area"));
                                    up_cordinate = cursor.getString(cursor.getColumnIndex("cordinate"));
                                    up_geoaddress = cursor.getString(cursor.getColumnIndex("geoaddress"));
                                    up_sourceofIrrigation = cursor.getString(cursor.getColumnIndex("sourceofIrrigation"));
                                    up_distributor = cursor.getString(cursor.getColumnIndex("distributor"));
                                    up_img = cursor.getString(cursor.getColumnIndex("img"));
                                    up_visitno = cursor.getString(cursor.getColumnIndex("visitno"));
                                    up_usercode = cursor.getString(cursor.getColumnIndex("usercode"));
                                    up_tr_date = cursor.getString(cursor.getColumnIndex("tr_date"));
                                    up_uploaded = cursor.getString(cursor.getColumnIndex("uploaded"));

                                    uploadData(up_farmerName, up_Mobile, up_state, up_district, up_taluka,
                                            up_village, up_pincode, up_area, up_cordinate, up_geoaddress,
                                            up_sourceofIrrigation, up_distributor,
                                            up_img, up_visitno, up_usercode, up_tr_date, up_uploaded);
                                }
                                while (cursor.moveToNext());
                            }
                        } else {
                            Toast.makeText(getContext(), "Data Will Upload When Internet Available", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });
        return rootView;
    }

    private void captureImage2() {

        try {
            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

            if (imageselect == 1) {
                photoFile = createImageFile4();
                if (photoFile != null) {
                    Log.i("Mayank", photoFile.getAbsolutePath());
                    Uri photoURI = Uri.fromFile(photoFile);
                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(cameraIntent, REQUEST_CAMERA);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            msclass.showMessage(ex.getMessage());
        }
    }

    private void captureImage() {

        try {
            if (ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions((Activity) getContext(), new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
            } else {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
                    // Create the File where the photo should go
                    try {
                        if (imageselect == 1) {
                            photoFile = createImageFile();

                            if (photoFile != null) {
                                Uri photoURI = FileProvider.getUriForFile(getContext(),
                                        "com.growindigo.connectplus",
                                        photoFile);
                                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                                startActivityForResult(takePictureIntent, REQUEST_CAMERA);
                            }
                        }

                    } catch (Exception ex) {

                        msclass.showMessage(ex.toString());
                        ex.printStackTrace();
                    }


                } else {
                }
            }
        } catch (Exception ex) {
            msclass.showMessage(ex.getMessage());
            ex.printStackTrace();
            //dialog.dismiss();
        }
    }

    private File createImageFile4() //  throws IOException
    {
        File mediaFile = null;
        try {
            // External sdcard location
            File mediaStorageDir = new File(
                    Environment
                            .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                    IMAGE_DIRECTORY_NAME);
            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    return null;
                }
            }
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                    Locale.getDefault()).format(new Date());
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } catch (Exception ex) {
            ex.printStackTrace();
            msclass.showMessage(ex.getMessage());
        }
        return mediaFile;
    }

    private File createImageFile() {
        // Create an image file name
        File image = null;
        try {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = "JPEG_" + timeStamp + "_";
            File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );

        } catch (Exception ex) {
            ex.printStackTrace();
            msclass.showMessage(ex.toString());
        }
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == SELECT_FILE)
                    onSelectFromGalleryResult(data);
                else if (requestCode == REQUEST_CAMERA)
                    onCaptureImageResult(data);
            }
        } catch (Exception e) {
            e.printStackTrace();
            msclass.showMessage(e.toString());
        }
    }

    private void onCaptureImageResult(Intent data) {


        try {
            if (imageselect == 1) {

                try {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 2;

                    Bitmap myBitmap = BitmapFactory.decodeFile(photoFile.getAbsolutePath(), options);
                    Imagepath1 = photoFile.getAbsolutePath();
                    ivImage.setImageBitmap(myBitmap);
                    ivImage.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    msclass.showMessage(e.toString());
                    e.printStackTrace();
                }
                //end
            }

        } catch (Exception e) {
            msclass.showMessage(e.toString());
            e.printStackTrace();
        }

    }

    private void onSelectFromGalleryResult(Intent data) {


        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getContext().getApplicationContext().getContentResolver(), data.getData());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (imageselect == 1) {
            ivImage.setImageBitmap(bm);
            ivImage.setVisibility(View.VISIBLE);
        }

    }

    public void getLocation() {
        try {
            if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
            }
            Criteria criteria = new Criteria();
            criteria.setVerticalAccuracy(Criteria.ACCURACY_HIGH);
            criteria.setPowerRequirement(Criteria.POWER_HIGH);
            criteria.setAltitudeRequired(false);
            criteria.setSpeedRequired(false);
            criteria.setCostAllowed(true);
            criteria.setBearingRequired(false);
            locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) this);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, (LocationListener) this);
            //locationManager.getLastKnownLocation(locationManager.GPS_PROVIDER);
            setUpMapIfNeeded();
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    public void onLocationChanged(Location location) {
        longitude = location.getLongitude();
        latitude = location.getLatitude();
        cordinates = location.getLatitude() + "-" + location.getLongitude();
        getLocation();
    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(context, "Please Enable GPS", Toast.LENGTH_SHORT).show();
        final Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    private void setUpMapIfNeeded() {

        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        String result = null;
        try {
            List<Address> addressList = geocoder.getFromLocation(
                    latitude, longitude, 1);
            if (addressList != null && addressList.size() > 0) {
                Address address = addressList.get(0);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getAddressLine(i)).append("\n");//adress
                }
//                sb.append(address.getLocality()).append("\n");//village
//
//                sb.append(address.getPostalCode()).append("\n");
//                sb.append(address.getCountryName());
//                sb.append(address.getAdminArea()).append("\n"); //state
//
//                sb.append(address.getSubAdminArea()).append("\n");//district


                //txtFarmerState.setText(address.getAdminArea());
                //txtFarmerDistrict.setText(address.getSubAdminArea());
                //txtFarmerVillage.setText(address.getLocality());

                txtFarmerPincode.setText(address.getPostalCode());
                txtFarmerAddress.setText(address.getAddressLine(0).toString());
                txtFarmerAddress.setEnabled(false);
                result = sb.toString();
            }
        } catch (IOException e) {
            // Log.e(TAG, "Unable connect to Geocoder", e);
        }


    }

    protected boolean validation() {
        boolean flag = true;

        if (txtFarmerMobile.getText().length() == 0 || txtFarmerMobile.getText().length() < 10) {
            msclass.showMessage("Please Enter Valid Mobile No ");
            txtFarmerMobile.setError("Required");
            return false;
        }

        if (txtFarmer.getText().length() == 0) {
            msclass.showMessage("Please Enter Farmername ");
            txtFarmer.setError("Required");
            return false;
        }
        if (txtFarmerAddress.getText().length() == 0) {
            msclass.showMessage("Please Enter Address ");
            txtFarmerAddress.setError("Required");
            return false;
        }
//        if (txtFarmerState.getText().length() == 0) {
//            msclass.showMessage("Please Enter State ");
//            txtFarmerState.setError("Required");
//            return false;
//        }
//        if (txtFarmerDistrict.getText().length() == 0) {
//            msclass.showMessage("Please Enter District ");
//            txtFarmerDistrict.setError("Required");
//            return false;
//        }
//        if (txtFarmerTaluka.getText().length() == 0) {
//            msclass.showMessage("Please Enter Taluka ");
//            txtFarmerTaluka.setError("Required");
//            return false;
//        }
        if (txtFarmerVillage.getText().length() == 0) {
            msclass.showMessage("Please Enter Village ");
            txtFarmerVillage.setError("Required");
            return false;
        }
        if (txtFarmerPincode.getText().length() == 0) {
            msclass.showMessage("Please Enter Pincode ");
            txtFarmerPincode.setError("Required");
            return false;
        }

        if (txtFarmerArea.getText().length() == 0) {
            msclass.showMessage("Please Enter Area ");
            txtFarmerArea.setError("Required");
            return false;
        }
        if (spnIrrigation.getSelectedItem().toString().equals("Select")) {
            msclass.showMessage("Please Select Source of Irrigation ");
            return false;
        }
        if (spnDisributor.getSelectedItem().toString().equals("Select")) {
            msclass.showMessage("Please Select Distributor ");
            return false;
        }

        if (!hasImage(ivImage)) {
            msclass.showMessage("Please Select Image");
            return false;
        }
        return true;
    }

    private boolean hasImage(@NonNull ImageView view) {
        Drawable drawable = view.getDrawable();
        boolean hasImage = (drawable != null);
        if (hasImage && (drawable instanceof BitmapDrawable)) {
            hasImage = ((BitmapDrawable) drawable).getBitmap() != null;
        }
        return hasImage;
    }

    public void uploadData(String up_farmerName, String up_Mobile, String up_state, String up_district, String up_taluka,
                           String up_village, String up_pincode, String up_area, String up_cordinate, String up_geoaddress,
                           String up_sourceofIrrigation, String up_distributor, String up_img, String up_visitno,
                           String up_usercode, String up_tr_date, String up_uploaded) {
        String converdImage = "";
        if (up_img != null && !up_img.isEmpty()) {
            converdImage = Utils.getImageDatadetail(up_img);
        }
        String terri_name = Preferences.get(getContext(), Constants.PREF_Territory_Name);
        String teri_code = Preferences.get(getContext(), Constants.PREF_Territory_code);
        Map<String, String> paramsReq = new HashMap<String, String>();
        paramsReq.put("farmerName", up_farmerName);
        paramsReq.put("Mobile", up_Mobile);
        paramsReq.put("state", up_state);
        paramsReq.put("district", up_district);
        paramsReq.put("taluka", up_taluka);
        paramsReq.put("village", up_village);
        paramsReq.put("pincode", up_pincode);
        paramsReq.put("Territory", teri_code);
        paramsReq.put("Territory_code", terri_name);
        paramsReq.put("area", up_area);
        paramsReq.put("cordinate", up_cordinate);
        paramsReq.put("geoaddress", up_geoaddress);
        paramsReq.put("sourceofIrrigation", up_sourceofIrrigation);
        paramsReq.put("distributor", up_distributor);
        paramsReq.put("img", converdImage);
        paramsReq.put("visitno", up_visitno);
        paramsReq.put("usercode", up_usercode);
        paramsReq.put("tr_date", up_tr_date);
        paramsReq.put("uploaded", up_uploaded);
        //paramsReq.put("Remark", up_Remark);
        db.updateFarmerMaster(up_Mobile, "1");
        apiRequest(paramsReq, Constants.farmerinfo_Upload, Request.Method.POST);
    }

    private void apiRequest(Map<String, String> paramsReq, String tag, int method) {
        ApiRequest apiRequest = new ApiRequest(getContext(), this);
        apiRequest.postJSONRequest(Constants.BASE_URL + Constants.farmerinfo_Upload, tag,
                paramsReq, method);
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        //Log.d("upload:: ", response);
        // Log.d("up_visitnoRes:: ", up_visitno);
        if (response != null) {
            // Toast.makeText(context, "Data Uploaded Successfully", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        //Log.d("Error::", error.toString());
    }

    @Override
    public void onResume() {
        super.onResume();
        String userCode = Preferences.get(this.getContext(), Constants.PREF_USERCODE);
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
        Utils.fetchCurrentLocation(userCode, String.valueOf(latitude), String.valueOf(longitude), this.getClass().getSimpleName(), formattedDate + " " + currentTime, this.getActivity(), this);
    }

    @Override
    public void onPickResult(PickResult r) {
        if (r.getError() == null) {
            ivImage.setImageBitmap(r.getBitmap());
            //Image path
            String pathImage = r.getPath();
            Imagepath1 = Utils.getImageDatadetail(pathImage);
        } else {
            Toast.makeText(getActivity(), r.getError().getMessage(), Toast.LENGTH_LONG).show();
        }
    }
}
