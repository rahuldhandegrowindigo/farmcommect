package com.growindigo.connectplus;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.growindigo.connectplus.databaseutils.DBCreation;
import com.growindigo.connectplus.databaseutils.Messageclass;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static java.security.AccessController.getContext;

public class farmer_visit_1 extends AppCompatActivity implements LocationListener {
    private SearchableSpinner spnVisitPurpose, spnCrop, spnPeriod, spnCropStage, spnObservation, spnProduct;
    private TextView farmerName;
    private String FarmerMobile;
    int imageselect;
    File photoFile = null;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    ImageView ivImage;
    EditText txtpalce;
    private static final String IMAGE_DIRECTORY_NAME = "VISITPHOTO";
    private String Imagepath1;
    private EditText txtDateOfSowing, txtFarmerArea, txtDosage;
    LocationManager locationManager;
    DBCreation db;
    private double longitude;
    private double latitude;
    //Calendar myCalendar;
    DatePickerDialog picker;
    Messageclass msclass;
    String cordinates = "";
    String currentDateandTime = "";
    String geoAddress = "";
    private Button btn_FarmerPhoto, btn_farmerSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farmer_visit_1);
        Initializecontrol();
        loadspinnerdata();
        final Calendar myCalendar = Calendar.getInstance();
        Intent i = getIntent();
        farmerName.setText(i.getStringExtra("farmerName"));
        FarmerMobile = i.getStringExtra("mobile");
        getSupportActionBar().setTitle("First Farmer Visit");

        txtDateOfSowing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog
                picker = new DatePickerDialog(farmer_visit_1.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                //txtDateOfSowing.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                txtDateOfSowing.setText(sdf.format(myCalendar.getTime()));
                            }
                        }, year, month, day);
                picker.show();
            }
        });


        btn_farmerSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validation() == true) {


                    boolean result = db.InsertFirstVisitData(farmerName.getText().toString(), cordinates, geoAddress,
                            FarmerMobile, spnVisitPurpose.getSelectedItem().toString(), spnCrop.getSelectedItem().toString(),
                            txtFarmerArea.getText().toString(), spnPeriod.getSelectedItem().toString(),
                            spnCropStage.getSelectedItem().toString(), txtDateOfSowing.getText().toString(),
                            spnObservation.getSelectedItem().toString(), spnProduct.getSelectedItem().toString(),
                            txtDosage.getText().toString(),
                            1, "test", "123", currentDateandTime, "", "");
                    if (result) {
                        db.updateVisit(1, FarmerMobile);
                        msclass.showMessage("Data Save Sucessfully");

                    } else {
                        msclass.showMessage("Something went wrong!");
                    }
                }
            }
        });

        btn_FarmerPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(farmer_visit_1.this, android.Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_DENIED) {
                }
                imageselect = 1;
                try {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        captureImage();
                    } else {
                        captureImage2();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    msclass.showMessage(ex.getMessage());
                }
            }

        });
    }

    private void captureImage2() {

        try {
            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

            if (imageselect == 1) {
                photoFile = createImageFile4();
                if (photoFile != null) {
                    Log.i("Mayank", photoFile.getAbsolutePath());
                    Uri photoURI = Uri.fromFile(photoFile);
                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(cameraIntent, REQUEST_CAMERA);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            msclass.showMessage(ex.getMessage());
        }
    }

    private void captureImage() {

        try {

            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
            } else {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(this.getPackageManager()) != null) {
                    // Create the File where the photo should go
                    try {
                        if (imageselect == 1) {
                            photoFile = createImageFile();

                            if (photoFile != null) {
                                Uri photoURI = FileProvider.getUriForFile(this,
                                        "com.growindigo.connectplus",
                                        photoFile);
                                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                                startActivityForResult(takePictureIntent, REQUEST_CAMERA);
                            }
                        }

                    } catch (Exception ex) {

                        msclass.showMessage(ex.toString());
                        ex.printStackTrace();
                    }


                } else {
                }
            }
        } catch (Exception ex) {
            msclass.showMessage(ex.getMessage());
            ex.printStackTrace();
            //dialog.dismiss();
        }
    }

    private File createImageFile4() //  throws IOException
    {
        File mediaFile = null;
        try {
            // External sdcard location
            File mediaStorageDir = new File(
                    Environment
                            .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                    IMAGE_DIRECTORY_NAME);
            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    return null;
                }
            }
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                    Locale.getDefault()).format(new Date());
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } catch (Exception ex) {
            ex.printStackTrace();
            msclass.showMessage(ex.getMessage());
        }
        return mediaFile;
    }

    private File createImageFile() {
        // Create an image file name
        File image = null;
        try {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = "JPEG_" + timeStamp + "_";
            File storageDir = this.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );

        } catch (Exception ex) {
            ex.printStackTrace();
            msclass.showMessage(ex.toString());
        }
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == SELECT_FILE)
                    onSelectFromGalleryResult(data);
                else if (requestCode == REQUEST_CAMERA)
                    onCaptureImageResult(data);
            }
        } catch (Exception e) {
            e.printStackTrace();
            msclass.showMessage(e.toString());
        }
    }

    private void onCaptureImageResult(Intent data) {


        try {
            if (imageselect == 1) {

                try {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 2;

                    Bitmap myBitmap = BitmapFactory.decodeFile(photoFile.getAbsolutePath(), options);
                    Imagepath1 = photoFile.getAbsolutePath();
                    ivImage.setImageBitmap(myBitmap);
                    ivImage.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    msclass.showMessage(e.toString());
                    e.printStackTrace();
                }
                //end
            }

        } catch (Exception e) {
            msclass.showMessage(e.toString());
            e.printStackTrace();
        }

    }

    private void onSelectFromGalleryResult(Intent data) {


        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(this.getApplicationContext().getContentResolver(), data.getData());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (imageselect == 1) {
            ivImage.setImageBitmap(bm);
            ivImage.setVisibility(View.VISIBLE);
        }

    }

    protected void Initializecontrol() {
        spnVisitPurpose = (SearchableSpinner) findViewById(R.id.spnVisitPurpose);
        spnCrop = (SearchableSpinner) findViewById(R.id.spnCrop);
        spnPeriod = (SearchableSpinner) findViewById(R.id.spnPeriod);
        spnCropStage = (SearchableSpinner) findViewById(R.id.spnCropStage);
        spnObservation = (SearchableSpinner) findViewById(R.id.spnObservation);
        spnProduct = (SearchableSpinner) findViewById(R.id.spnProduct);
        txtDateOfSowing = (EditText) findViewById(R.id.txtDateOfSowing);
        farmerName = (TextView) findViewById(R.id.farmerName);
        txtFarmerArea = (EditText) findViewById(R.id.txtFarmerArea);
        txtDosage = (EditText) findViewById(R.id.txtDosage);
        msclass = new Messageclass(this);
        db = new DBCreation(this);
        btn_FarmerPhoto = (Button) findViewById(R.id.btn_FarmerPhoto);
        btn_farmerSave = (Button) findViewById(R.id.btn_farmerSave);
        ivImage = (ImageView) findViewById(R.id.ivImage);
        getLocation();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        currentDateandTime = sdf.format(new Date());
    }

    protected void loadspinnerdata() {
        List<String> spnVisitPurpose_list = new ArrayList<String>();
        spnVisitPurpose_list.add("Select Visit Purpose");
        spnVisitPurpose_list.add("Sales");
        spnVisitPurpose_list.add("Complain");
        spnVisitPurpose_list.add("Demonstration");
        spnVisitPurpose_list.add("Farmer meet");
        spnVisitPurpose_list.add("Demo Result");
        spnVisitPurpose_list.add("Field Day");
        spnVisitPurpose_list.add("Meeting");
        spnVisitPurpose_list.add("Campaign");
        spnVisitPurpose_list.add("Review Meeting");
        spnVisitPurpose.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, spnVisitPurpose_list));

        List<String> spnCrop_list = new ArrayList<String>();
        spnCrop_list.add("Select Crop");

        spnCrop_list.add("Soyabean");
        spnCrop_list.add("Maize");
        spnCrop_list.add("Cotton");
        spnCrop_list.add("Chickpea");
        spnCrop_list.add("Pigeon Pea");
        spnCrop_list.add("Green Gram");
        spnCrop_list.add("Red Gram");
        spnCrop_list.add("Lentil");
        spnCrop_list.add("Pea");
        spnCrop_list.add("Ginger");
        spnCrop_list.add("Potato");
        spnCrop_list.add("Rice");
        spnCrop_list.add("Wheat");
        spnCrop_list.add("Pearl Millet");
        spnCrop_list.add("Black Gram");
        spnCrop_list.add("Chilli");
        spnCrop_list.add("Capsicum");
        spnCrop_list.add("Okra");
        spnCrop_list.add("Brinjal");
        spnCrop_list.add("Other");
        spnCrop.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, spnCrop_list));

        List<String> spnPeriod_list = new ArrayList<String>();
        spnPeriod_list.add("Select Period");
        spnPeriod_list.add("Weekly");
        spnPeriod_list.add("Fortnightly");

        spnPeriod.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, spnPeriod_list));

        List<String> spnCropStage_list = new ArrayList<String>();
        spnCropStage_list.add("Select Crop Stage");
        spnCropStage_list.add("Land Preparation");
        spnCropStage_list.add("Sowing");
        spnCropStage_list.add("Nursery");
        spnCropStage_list.add("Transplating");
        spnCropStage_list.add("Vegetative Growth / Tillering");
        spnCropStage_list.add("Flowering");
        spnCropStage_list.add("Fruiting / Pod Development");
        spnCropStage_list.add("Grain Filling");
        spnCropStage_list.add("Picking");
        spnCropStage_list.add("Harvesting");
        spnCropStage_list.add("Tuber formation");
        spnCropStage_list.add("Tuber Development");
        spnCropStage.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, spnCropStage_list));

        List<String> spnObservation_list = new ArrayList<String>();
        spnObservation_list.add("Select Observation");
        spnObservation_list.add("Germination %");
        spnObservation_list.add("Seedling number / Sq. mt");
        spnObservation_list.add("Plant Height");
        spnObservation_list.add("Root Length");
        spnObservation_list.add("No. of tillers");
        spnObservation_list.add("No. of branches");
        spnObservation_list.add("Seeds / Pod");
        spnObservation_list.add("Tiller Length");
        spnObservation_list.add("No. of Fruits / plant");
        spnObservation_list.add("Fruit Size");
        spnObservation_list.add("Yield / sq mt.");
        spnObservation.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, spnObservation_list));

        List<String> spnProduct_list = new ArrayList<String>();
        spnProduct_list.add("Select Product");
        spnProduct_list.add("prerak Soya");
        spnProduct_list.add("prerak Cotton");
        spnProduct_list.add("prerak Maize");
        spnProduct_list.add("prerak Chickpea");
        spnProduct_list.add("prerak Pulses");
        spnProduct_list.add("prerak Wheat");
        spnProduct_list.add("Oorjit Liquid");
        spnProduct_list.add("Oorjit Granules");
        spnProduct.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, spnProduct_list));

    }

    public void getLocation() {
        try {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);

            }
            Criteria criteria = new Criteria();
            criteria.setVerticalAccuracy(Criteria.ACCURACY_HIGH);
            criteria.setPowerRequirement(Criteria.POWER_HIGH);
            criteria.setAltitudeRequired(false);
            criteria.setSpeedRequired(false);
            criteria.setCostAllowed(true);
            criteria.setBearingRequired(false);
            locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) this);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, (LocationListener) this);
            //locationManager.getLastKnownLocation(locationManager.GPS_PROVIDER);
            setUpMapIfNeeded();
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        longitude = location.getLongitude();
        latitude = location.getLatitude();
        cordinates = location.getLatitude() + "-" + location.getLongitude();
        getLocation();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, "Please Enable GPS", Toast.LENGTH_SHORT).show();
        final Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
    }

    private void setUpMapIfNeeded() {

        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        String result = null;
        try {
            List<Address> addressList = geocoder.getFromLocation(
                    latitude, longitude, 1);
            if (addressList != null && addressList.size() > 0) {
                Address address = addressList.get(0);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getAddressLine(i)).append("\n");//adress
                }
                geoAddress = address.getAddressLine(0).toString();
                result = sb.toString();
            }
        } catch (IOException e) {
            // Log.e(TAG, "Unable connect to Geocoder", e);
        }


    }

    protected boolean validation() {
        boolean flag = true;

        if (txtFarmerArea.getText().length() == 0) {

            msclass.showMessage("Please Enter Area ");
            txtFarmerArea.setError("Required");
            return false;
        }
        if (txtDosage.getText().length() == 0) {
            msclass.showMessage("Please Enter Dosage ");
            txtDosage.setError("Required");
            return false;
        }
        if (txtDateOfSowing.getText().length() == 0) {
            msclass.showMessage("Please Select Sowing Date ");
            txtDateOfSowing.setError("Required");
            return false;
        }
        if (spnVisitPurpose.getSelectedItem().toString().equals("Select Visit Purpose")) {
            msclass.showMessage("Please Select Visit Purpose");
            return false;
        }
        if (spnCrop.getSelectedItem().toString().equals("Select Crop")) {
            msclass.showMessage("Please Select Crop");
            return false;
        }
        if (spnPeriod.getSelectedItem().toString().equals("Select Period")) {
            msclass.showMessage("Please Select Period");
            return false;
        }
        if (spnCropStage.getSelectedItem().toString().equals("Select Crop Stage")) {
            msclass.showMessage("Please Select Crop Stage");
            return false;
        }
        if (spnObservation.getSelectedItem().toString().equals("Select Observation")) {
            msclass.showMessage("Please Select Observation");
            return false;
        }
        if (spnProduct.getSelectedItem().toString().equals("Select Product")) {
            msclass.showMessage("Please Select Product");
            return false;
        }

        if (!hasImage(ivImage)) {
            msclass.showMessage("Please Capture Image");
            return false;
        }
        return true;
    }

    private boolean hasImage(@NonNull ImageView view) {
        Drawable drawable = view.getDrawable();
        boolean hasImage = (drawable != null);
        if (hasImage && (drawable instanceof BitmapDrawable)) {
            hasImage = ((BitmapDrawable) drawable).getBitmap() != null;
        }
        return hasImage;
    }

}
