package com.growindigo.connectplus;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.growindigo.connectplus.databaseutils.DBCreation;
import com.growindigo.connectplus.utils.Constants;
import com.growindigo.connectplus.utils.Preferences;
import com.growindigo.connectplus.utils.Utils;
import com.growindigo.connectplus.volley.ApiRequest;
import com.growindigo.connectplus.volley.IApiResponse;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class download_data extends AppCompatActivity implements IApiResponse, LocationListener {

    Button btn_download;
    private DBCreation mDatabase;
    ProgressDialog pDialog;
    private double longitude;
    private double latitude;
    LocationManager locationManager;
    String geoAddress = "";
    String downloadResponse = "";
    LinearLayout llProgressBar;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download_data);
        getSupportActionBar().setTitle("Download Master");
        context = this;
        pDialog = new ProgressDialog(this);
        mDatabase = new DBCreation(this);
        btn_download = (Button) findViewById(R.id.btn_download);
        llProgressBar = (LinearLayout) findViewById(R.id.llProgressBar);
        getLocation();
        Animation shaking = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.shaking);
        btn_download.setAnimation(shaking);
        btn_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadData();
                // new MyTask().execute();
            }
        });
    }

    //
    public class MyTask extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog.setMessage("Downloading Data");
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.setCancelable(false);
            //llProgressBar.setVisibility(View.VISIBLE);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            pDialog.dismiss();
            Utils.showAlertDialogOk(getString(R.string.success), getString(R.string.success_download), download_data.this);

        }

        @Override
        protected JSONObject doInBackground(String... params) {
            try {
                JSONObject jsonObj = new JSONObject(downloadResponse);

                if (downloadResponse.contains("Table")) {
                    JSONArray jsonArray = jsonObj.getJSONArray("Table");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        mDatabase.InsertFarmerData(jsonArray.getJSONObject(i).getString("farmerName"),
                                jsonArray.getJSONObject(i).getString("state"),
                                jsonArray.getJSONObject(i).getString("district"),
                                jsonArray.getJSONObject(i).getString("taluka"),
                                jsonArray.getJSONObject(i).getString("village"),
                                jsonArray.getJSONObject(i).getString("pincode"),
                                "",
                                "",
                                jsonArray.getJSONObject(i).getString("MobileNo"),
                                jsonArray.getJSONObject(i).getString("Territory"),
                                "",
                                "",
                                "",
                                jsonArray.getJSONObject(i).getString("MDO_code"),
                                "",
                                ""
                        );
                    }

                    // Utils.showAlertDialogOk(getString(R.string.success), getString(R.string.success_register), download_data.this);
                }
                if (downloadResponse.contains("Table1")) {
                    JSONArray jsonArray = jsonObj.getJSONArray("Table1");
                    // Extract the Place descriptions from the results
                    mDatabase.insertDistributorData("Grow Online", "8888899999", "8888899999");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        mDatabase.insertDistributorData(jsonArray.getJSONObject(i).getString("dist_name"),
                                jsonArray.getJSONObject(i).getString("dist_code"),
                                jsonArray.getJSONObject(i).getString("dist_mobile")
                        );
                    }
                    // Utils.showAlertDialogOk(getString(R.string.success), getString(R.string.success_register), download_data.this);
                }
                if (downloadResponse.contains("Table2")) {
                    JSONArray jsonArray = jsonObj.getJSONArray("Table2");
                    // Extract the Place descriptions from the results
                    for (int i = 0; i < jsonArray.length(); i++) {
                        mDatabase.insertCropData(jsonArray.getJSONObject(i).getString("crop_name")
                        );
                    }
                    // Utils.showAlertDialogOk(getString(R.string.success), getString(R.string.success_register), download_data.this);
                }
                if (downloadResponse.contains("Table3")) {
                    JSONArray jsonArray = jsonObj.getJSONArray("Table3");
                    // Extract the Place descriptions from the results
                    for (int i = 0; i < jsonArray.length(); i++) {
                        mDatabase.insertProductData(jsonArray.getJSONObject(i).getString("product_name"),
                                jsonArray.getJSONObject(i).getString("product_code"),
                                jsonArray.getJSONObject(i).getString("mrp"),
                                jsonArray.getJSONObject(i).getString("dist_price"),
                                jsonArray.getJSONObject(i).getString("ret_price")
                        );
                    }
                    // Utils.showAlertDialogOk(getString(R.string.success), getString(R.string.success_download), download_data.this);
                }
                if (downloadResponse.contains("Table4")) {
                    JSONArray jsonArray = jsonObj.getJSONArray("Table4");
                    // Extract the Place descriptions from the results
                    for (int i = 0; i < jsonArray.length(); i++) {
                        mDatabase.insertRetailerData(jsonArray.getJSONObject(i).getString("ret_name"),
                                jsonArray.getJSONObject(i).getString("ret_code"),
                                jsonArray.getJSONObject(i).getString("ret_mobile")
                        );
                    }
                    // pDialog.dismiss();
                    // Utils.showAlertDialogOk(getString(R.string.success), getString(R.string.success_download), download_data.this);
                }
                if (downloadResponse.contains("Table5")) {
                    JSONArray jsonArray = jsonObj.getJSONArray("Table5");
                    // Extract the Place descriptions from the results
                    for (int i = 0; i < jsonArray.length(); i++) {
                        mDatabase.insertStateData(jsonArray.getJSONObject(i).getString("stat_code"),
                                jsonArray.getJSONObject(i).getString("stat_desc"),
                                jsonArray.getJSONObject(i).getString("dist_code"),
                                jsonArray.getJSONObject(i).getString("dist_desc"),
                                jsonArray.getJSONObject(i).getString("talq_code"),
                                jsonArray.getJSONObject(i).getString("talq_desc")
                        );
                    }
                    //pDialog.dismiss();
                    //Utils.showAlertDialogOk(getString(R.string.success), getString(R.string.success_download), download_data.this);
                }
                if (downloadResponse.contains("Table6")) {
                    JSONArray jsonArray = jsonObj.getJSONArray("Table6");
                    // Extract the Place descriptions from the results
                    for (int i = 0; i < jsonArray.length(); i++) {
                        mDatabase.DownloadDistAssignWithCode(jsonArray.getJSONObject(i).getString("dist_code"),
                                jsonArray.getJSONObject(i).getString("dist_name"),
                                jsonArray.getJSONObject(i).getString("ret_code"),
                                jsonArray.getJSONObject(i).getString("ret_name")

                        );
                    }
//                    pDialog.dismiss();
//                    Utils.showAlertDialogOk(getString(R.string.success), getString(R.string.success_download), download_data.this);
                } else {
                    pDialog.dismiss();
                    Utils.showAlertDialog(getString(R.string.error), getString(R.string.some_problem_occurred), download_data.this);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return new JSONObject();
        }
    }
    //

    public void downloadData() {
        mDatabase.deleledata("FarmerMaster");
        mDatabase.deleledata("DistributorTable");
        mDatabase.deleledata("CropMaster");
        mDatabase.deleledata("ProductTable");
        mDatabase.deleledata("RetailerTable");
        mDatabase.deleledata("distAssing");
        mDatabase.deleledata("stateMaster");

        Map<String, String> paramsReq = new HashMap<String, String>();
        String userCode = Preferences.get(this, Constants.PREF_USERCODE);
        paramsReq.put("usercode", userCode);
        pDialog.setMessage("Downloading Data");
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setCancelable(false);
        pDialog.show();
        apiRequest(paramsReq, Constants.DownloadData, Request.Method.POST);
    }

    private void apiRequest(Map<String, String> paramsReq, String tag, int method) {
        ApiRequest apiRequest = new ApiRequest(this, this);
        apiRequest.postJSONRequest(Constants.BASE_URL + Constants.DownloadData, tag,
                paramsReq, method);
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        //Log.d("verifyResponse: ", response);
        if (tag_json_obj.equalsIgnoreCase(Constants.DownloadData)) {
            downloadResponse = response;
            new MyTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            //new MyTask().execute(response);
        }
//        try {
//            if (tag_json_obj.equals(Constants.DownloadData)) {
//                JSONObject jsonObj = new JSONObject(response);
//
//
//                pDialog.dismiss();
//                Utils.showAlertDialogOk(getString(R.string.success), getString(R.string.success_download), download_data.this);
//                //pDialog.setMessage(this.getString(R.string.Loading));
//                // pDialog.show();
//
//                if (response.contains("Table")) {
//                    JSONArray jsonArray = jsonObj.getJSONArray("Table");
//
//                    for (int i = 0; i < jsonArray.length(); i++) {
//                        mDatabase.InsertFarmerData(jsonArray.getJSONObject(i).getString("farmerName"),
//                                jsonArray.getJSONObject(i).getString("state"),
//                                jsonArray.getJSONObject(i).getString("district"),
//                                jsonArray.getJSONObject(i).getString("taluka"),
//                                jsonArray.getJSONObject(i).getString("village"),
//                                jsonArray.getJSONObject(i).getString("pincode"),
//                                "",
//                                "",
//                                jsonArray.getJSONObject(i).getString("MobileNo"),
//                                jsonArray.getJSONObject(i).getString("Territory"),
//                                "",
//                                "",
//                                "",
//                                jsonArray.getJSONObject(i).getString("MDO_code"),
//                                "",
//                                ""
//                        );
//                    }
//
//                    // Utils.showAlertDialogOk(getString(R.string.success), getString(R.string.success_register), download_data.this);
//                }
//                if (response.contains("Table1")) {
//                    JSONArray jsonArray = jsonObj.getJSONArray("Table1");
//                    // Extract the Place descriptions from the results
//                    mDatabase.insertDistributorData("Grow Online", "8888899999", "8888899999");
//                    for (int i = 0; i < jsonArray.length(); i++) {
//                        mDatabase.insertDistributorData(jsonArray.getJSONObject(i).getString("dist_name"),
//                                jsonArray.getJSONObject(i).getString("dist_code"),
//                                jsonArray.getJSONObject(i).getString("dist_mobile")
//                        );
//                    }
//                    // Utils.showAlertDialogOk(getString(R.string.success), getString(R.string.success_register), download_data.this);
//                }
//                if (response.contains("Table2")) {
//                    JSONArray jsonArray = jsonObj.getJSONArray("Table2");
//                    // Extract the Place descriptions from the results
//                    for (int i = 0; i < jsonArray.length(); i++) {
//                        mDatabase.insertCropData(jsonArray.getJSONObject(i).getString("crop_name")
//                        );
//                    }
//                    // Utils.showAlertDialogOk(getString(R.string.success), getString(R.string.success_register), download_data.this);
//                }
//                if (response.contains("Table3")) {
//                    JSONArray jsonArray = jsonObj.getJSONArray("Table3");
//                    // Extract the Place descriptions from the results
//                    for (int i = 0; i < jsonArray.length(); i++) {
//                        mDatabase.insertProductData(jsonArray.getJSONObject(i).getString("product_name"),
//                                jsonArray.getJSONObject(i).getString("product_code"),
//                                jsonArray.getJSONObject(i).getString("mrp"),
//                                jsonArray.getJSONObject(i).getString("dist_price"),
//                                jsonArray.getJSONObject(i).getString("ret_price")
//                        );
//                    }
//                    // Utils.showAlertDialogOk(getString(R.string.success), getString(R.string.success_download), download_data.this);
//                }
//                if (response.contains("Table4")) {
//                    JSONArray jsonArray = jsonObj.getJSONArray("Table4");
//                    // Extract the Place descriptions from the results
//                    for (int i = 0; i < jsonArray.length(); i++) {
//                        mDatabase.insertRetailerData(jsonArray.getJSONObject(i).getString("ret_name"),
//                                jsonArray.getJSONObject(i).getString("ret_code"),
//                                jsonArray.getJSONObject(i).getString("ret_mobile")
//                        );
//                    }
//                    // pDialog.dismiss();
//                    // Utils.showAlertDialogOk(getString(R.string.success), getString(R.string.success_download), download_data.this);
//                }
//                if (response.contains("Table5")) {
//                    JSONArray jsonArray = jsonObj.getJSONArray("Table5");
//                    // Extract the Place descriptions from the results
//                    for (int i = 0; i < jsonArray.length(); i++) {
//                        mDatabase.insertStateData(jsonArray.getJSONObject(i).getString("stat_code"),
//                                jsonArray.getJSONObject(i).getString("stat_desc"),
//                                jsonArray.getJSONObject(i).getString("dist_code"),
//                                jsonArray.getJSONObject(i).getString("dist_desc"),
//                                jsonArray.getJSONObject(i).getString("talq_code"),
//                                jsonArray.getJSONObject(i).getString("talq_desc")
//                        );
//                    }
//                    //pDialog.dismiss();
//                    //Utils.showAlertDialogOk(getString(R.string.success), getString(R.string.success_download), download_data.this);
//                }
//                if (response.contains("Table6")) {
//                    JSONArray jsonArray = jsonObj.getJSONArray("Table6");
//                    // Extract the Place descriptions from the results
//                    for (int i = 0; i < jsonArray.length(); i++) {
//                        mDatabase.DownloadDistAssignWithCode(jsonArray.getJSONObject(i).getString("dist_code"),
//                                jsonArray.getJSONObject(i).getString("dist_name"),
//                                jsonArray.getJSONObject(i).getString("ret_code"),
//                                jsonArray.getJSONObject(i).getString("ret_name")
//
//                        );
//                    }
////                    pDialog.dismiss();
////                    Utils.showAlertDialogOk(getString(R.string.success), getString(R.string.success_download), download_data.this);
//                } else {
//                    pDialog.dismiss();
//                    Utils.showAlertDialog(getString(R.string.error), getString(R.string.some_problem_occurred), download_data.this);
//                }
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onLocationChanged(Location location) {
        longitude = location.getLongitude();
        latitude = location.getLatitude();
        getLocation();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, "Please Enable GPS", Toast.LENGTH_SHORT).show();
        final Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
    }

    public void getLocation() {
        try {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
            }
            Criteria criteria = new Criteria();
            criteria.setVerticalAccuracy(Criteria.ACCURACY_HIGH);
            criteria.setPowerRequirement(Criteria.POWER_HIGH);
            criteria.setAltitudeRequired(false);
            criteria.setSpeedRequired(false);
            criteria.setCostAllowed(true);
            criteria.setBearingRequired(false);
            locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) this);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, (LocationListener) this);
            locationManager.getLastKnownLocation(locationManager.GPS_PROVIDER);
            setUpMapIfNeeded();
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    private void setUpMapIfNeeded() {

        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        String result = null;
        try {
            List<Address> addressList = geocoder.getFromLocation(
                    latitude, longitude, 1);
            if (addressList != null && addressList.size() > 0) {
                Address address = addressList.get(0);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getAddressLine(i)).append("\n");//adress
                }
                geoAddress = address.getAddressLine(0).toString();
                result = sb.toString();
            }
        } catch (IOException e) {
            // Log.e(TAG, "Unable connect to Geocoder", e);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        String userCode = Preferences.get(download_data.this, Constants.PREF_USERCODE);
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
        getLocation();
        Utils.fetchCurrentLocation(userCode, String.valueOf(latitude), String.valueOf(longitude), this.getClass().getSimpleName(), formattedDate + " " + currentTime, this, this);
    }
}
