package com.growindigo.connectplus;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.growindigo.connectplus.databaseutils.DBCreation;
import com.growindigo.connectplus.databaseutils.Messageclass;
import com.growindigo.connectplus.utils.Constants;
import com.growindigo.connectplus.utils.Preferences;
import com.growindigo.connectplus.utils.Utils;
import com.growindigo.connectplus.volley.ApiRequest;
import com.growindigo.connectplus.volley.IApiResponse;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class dist_complaint extends AppCompatActivity implements LocationListener, IApiResponse {
    private SearchableSpinner spn_ComplaintMode;
    private EditText txtDetails;
    private TextView DistributorName;
    String DistributorCode = "";
    private Button btn_add, btn_complaintPhoto;
    private double longitude;
    private double latitude;
    Messageclass msclass;
    String cordinates = "";
    String currentDateandTime = "";
    int imageselect;
    File photoFile = null;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    ImageView ivImage;
    EditText txtpalce;
    private static final String IMAGE_DIRECTORY_NAME = "VISITPHOTO";
    private String Imagepath1;
    DBCreation db;
    String geoAddress = "";
    LocationManager locationManager;
    String up_dist_name, up_dist_code, up_complaintType, up_detail, up_photo, up_usercode, up_tr_date, up_cordinate, up_geoaddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dist_complaint);
        getSupportActionBar().setTitle("Distributor Complaint Visit");
        Initializecontrol();
        loadspinnerdata();
        Intent i = getIntent();
        DistributorName.setText(i.getStringExtra("dist_name"));
        DistributorCode = i.getStringExtra("dist_code");
        btn_complaintPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_DENIED) {
                }
                imageselect = 1;
                try {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        captureImage();
                    } else {
                        captureImage2();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    msclass.showMessage(ex.getMessage());
                }
            }
        });

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validation() == true) {
                    Map<String, String> paramsReq = new HashMap<String, String>();
                    String userCode = Preferences.get(dist_complaint.this, Constants.PREF_USERCODE);
                    paramsReq.put("usercode", userCode);
                    boolean result = db.dist_complaint_insert(DistributorName.getText().toString(),
                            DistributorCode, spn_ComplaintMode.getSelectedItem().toString(),
                            txtDetails.getText().toString(),
                            Imagepath1, userCode, currentDateandTime, cordinates, geoAddress);
                    if (result) {
                        Utils.showAlertDialogOk("SUCCESS", "Data Save Successfully", dist_complaint.this);

                    } else {
                        Utils.showAlertDialogOk("Error", "Something Went Wrong", dist_complaint.this);
                    }
                    /* Data Upload */
                    if (Utils.checkPermissionLocation(dist_complaint.this)) {
                        String searchQuery = "select  *  from dist_ComplaintTable where  uploaded ='0'";
                        Cursor cursor = db.getReadableDatabase().rawQuery(searchQuery, null);
                        int count = cursor.getCount();
                        if (count > 0) {
                            if (cursor.moveToFirst()) {
                                do {
                                    up_dist_name = cursor.getString(cursor.getColumnIndex("dist_name"));
                                    up_dist_code = cursor.getString(cursor.getColumnIndex("dist_code"));
                                    up_complaintType = cursor.getString(cursor.getColumnIndex("complaintType"));
                                    up_detail = cursor.getString(cursor.getColumnIndex("detail"));
                                    up_photo = cursor.getString(cursor.getColumnIndex("photo"));
                                    up_usercode = cursor.getString(cursor.getColumnIndex("usercode"));
                                    up_tr_date = cursor.getString(cursor.getColumnIndex("tr_date"));
                                    up_cordinate = cursor.getString(cursor.getColumnIndex("cordinate"));
                                    up_geoaddress = cursor.getString(cursor.getColumnIndex("geoaddress"));
                                    uploadData(up_dist_name, up_dist_code, up_complaintType, up_detail, up_photo, up_usercode, up_tr_date, up_cordinate, up_geoaddress);
                                } while (cursor.moveToNext());
                            } else {
                                Toast.makeText(dist_complaint.this, "Data Will Upload When Internet Available", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }

            }
        });
    }

    protected boolean validation() {
        boolean flag = true;

        if (txtDetails.getText().length() == 0) {

            msclass.showMessage("Please Enter Details ");
            txtDetails.setError("Required");
            return false;
        }

        if (spn_ComplaintMode.getSelectedItem().toString().equals("Select Complaint Mode")) {
            msclass.showMessage("Please Select Complaint Mode");
            return false;
        }

        if (!hasImage(ivImage)) {
            msclass.showMessage("Please Select Image");
            return false;
        }

        return true;
    }

    private boolean hasImage(@NonNull ImageView view) {
        Drawable drawable = view.getDrawable();
        boolean hasImage = (drawable != null);
        if (hasImage && (drawable instanceof BitmapDrawable)) {
            hasImage = ((BitmapDrawable) drawable).getBitmap() != null;
        }
        return hasImage;
    }




    protected void Initializecontrol() {
        spn_ComplaintMode = (SearchableSpinner) findViewById(R.id.spn_ComplaintMode);
        txtDetails = (EditText) findViewById(R.id.txtDetails);
        DistributorName = (TextView) findViewById(R.id.DistributorName);
        btn_add = (Button) findViewById(R.id.btn_add);
        btn_complaintPhoto = (Button) findViewById(R.id.btn_complaintPhoto);
        ivImage = (ImageView) findViewById(R.id.ivImage);
        msclass = new Messageclass(this);
        db = new DBCreation(this);
        getLocation();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        currentDateandTime = sdf.format(new Date());
    }

    protected void loadspinnerdata() {
        List<String> spn_ComplaintMode_list = new ArrayList<String>();
        spn_ComplaintMode_list.add("Select Complaint Mode");
        spn_ComplaintMode_list.add("Account relatated");
        spn_ComplaintMode_list.add("Scheme");
        spn_ComplaintMode_list.add("Pending order");
        spn_ComplaintMode_list.add("Leakage");
        spn_ComplaintMode_list.add("Product performance");
        spn_ComplaintMode.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, spn_ComplaintMode_list));

    }

    private void captureImage2() {

        try {
            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

            if (imageselect == 1) {
                photoFile = createImageFile4();
                if (photoFile != null) {
                    Log.i("Mayank", photoFile.getAbsolutePath());
                    Uri photoURI = Uri.fromFile(photoFile);
                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(cameraIntent, REQUEST_CAMERA);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            msclass.showMessage(ex.getMessage());
        }
    }

    private void captureImage() {

        try {

            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions((Activity) this, new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
            } else {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(this.getPackageManager()) != null) {
                    // Create the File where the photo should go
                    try {
                        if (imageselect == 1) {
                            photoFile = createImageFile();

                            if (photoFile != null) {
                                Uri photoURI = FileProvider.getUriForFile(this,
                                        "com.growindigo.connectplus",
                                        photoFile);
                                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                                startActivityForResult(takePictureIntent, REQUEST_CAMERA);
                            }
                        }

                    } catch (Exception ex) {

                        msclass.showMessage(ex.toString());
                        ex.printStackTrace();
                    }


                } else {
                }
            }
        } catch (Exception ex) {
            msclass.showMessage(ex.getMessage());
            ex.printStackTrace();
            //dialog.dismiss();
        }
    }

    private File createImageFile4() //  throws IOException
    {
        File mediaFile = null;
        try {
            // External sdcard location
            File mediaStorageDir = new File(
                    Environment
                            .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                    IMAGE_DIRECTORY_NAME);
            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    return null;
                }
            }
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                    Locale.getDefault()).format(new Date());
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } catch (Exception ex) {
            ex.printStackTrace();
            msclass.showMessage(ex.getMessage());
        }
        return mediaFile;
    }

    private File createImageFile() {
        // Create an image file name
        File image = null;
        try {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = "JPEG_" + timeStamp + "_";
            File storageDir = this.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );

        } catch (Exception ex) {
            ex.printStackTrace();
            msclass.showMessage(ex.toString());
        }
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == SELECT_FILE)
                    onSelectFromGalleryResult(data);
                else if (requestCode == REQUEST_CAMERA)
                    onCaptureImageResult(data);
            }
        } catch (Exception e) {
            e.printStackTrace();
            msclass.showMessage(e.toString());
        }
    }

    private void onCaptureImageResult(Intent data) {


        try {
            if (imageselect == 1) {

                try {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 2;

                    Bitmap myBitmap = BitmapFactory.decodeFile(photoFile.getAbsolutePath(), options);
                    Imagepath1 = photoFile.getAbsolutePath();
                    ivImage.setImageBitmap(myBitmap);
                    ivImage.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    msclass.showMessage(e.toString());
                    e.printStackTrace();
                }
                //end
            }

        } catch (Exception e) {
            msclass.showMessage(e.toString());
            e.printStackTrace();
        }

    }

    private void onSelectFromGalleryResult(Intent data) {


        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(this.getApplicationContext().getContentResolver(), data.getData());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (imageselect == 1) {
            ivImage.setImageBitmap(bm);
            ivImage.setVisibility(View.VISIBLE);
        }

    }

    public void getLocation() {
        try {
            locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
            Criteria criteria = new Criteria();
            criteria.setVerticalAccuracy(Criteria.ACCURACY_HIGH);
            criteria.setPowerRequirement(Criteria.POWER_HIGH);
            criteria.setAltitudeRequired(false);
            criteria.setSpeedRequired(false);
            criteria.setCostAllowed(true);
            criteria.setBearingRequired(false);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) this);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, (LocationListener) this);
            //locationManager.getLastKnownLocation(locationManager.GPS_PROVIDER);
            setUpMapIfNeeded();
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    public void onLocationChanged(Location location) {
        longitude = location.getLongitude();
        latitude = location.getLatitude();
        cordinates = location.getLatitude() + "-" + location.getLongitude();
        getLocation();
    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, "Please Enable GPS", Toast.LENGTH_SHORT).show();
        final Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    private void setUpMapIfNeeded() {

        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        String result = null;
        try {
            List<Address> addressList = geocoder.getFromLocation(
                    latitude, longitude, 1);
            if (addressList != null && addressList.size() > 0) {
                Address address = addressList.get(0);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getAddressLine(i)).append("\n");//adress
                }
                geoAddress = address.getAddressLine(0).toString();
                result = sb.toString();
            }
        } catch (IOException e) {
            // Log.e(TAG, "Unable connect to Geocoder", e);
        }


    }

    public void uploadData(String up_dist_name, String up_dist_code, String up_complaintType, String up_detail,
                           String up_photo, String up_usercode, String up_tr_date,
                           String up_cordinate, String up_geoaddress) {

        //String converdImage = Utils.encodeImage(up_photo);
        String converdImage = "";
        if (up_photo != null && !up_photo.isEmpty()) {
            converdImage = Utils.getImageDatadetail(up_photo);
        }
        Map<String, String> paramsReq = new HashMap<String, String>();
        paramsReq.put("dist_name", up_dist_name);
        paramsReq.put("dist_code", up_dist_code);
        paramsReq.put("complaintType", up_complaintType);
        paramsReq.put("detail", up_detail);
        paramsReq.put("photo", converdImage);
        paramsReq.put("cordinate", up_cordinate);
        paramsReq.put("geoaddress", up_geoaddress);
        paramsReq.put("usercode", up_usercode);
        paramsReq.put("tr_date", up_tr_date);
        db.updatedistComplaint(up_dist_code, "1");
        apiRequest(paramsReq, Constants.dist_complaint_upload, Request.Method.POST);
    }

    private void apiRequest(Map<String, String> paramsReq, String tag, int method) {
        ApiRequest apiRequest = new ApiRequest(this, this);
        apiRequest.postJSONRequest(Constants.BASE_URL + Constants.dist_complaint_upload, tag,
                paramsReq, method);
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        //Log.d("upload:: ", response);

      //  Toast.makeText(dist_complaint.this, "Data Uploaded Successfully", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        //Log.d("Error::", "error:: " + error);
    }

    @Override
    public void onResume() {
        super.onResume();
        String userCode = Preferences.get(dist_complaint.this, Constants.PREF_USERCODE);
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
        Utils.fetchCurrentLocation(userCode, String.valueOf(latitude), String.valueOf(longitude), this.getClass().getSimpleName(), formattedDate + " " + currentTime, this, this);
    }
}
