package com.growindigo.connectplus;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Vibrator;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.growindigo.connectplus.databaseutils.DBCreation;
import com.growindigo.connectplus.databaseutils.Messageclass;
import com.growindigo.connectplus.model.ProductModel;
import com.growindigo.connectplus.utils.CheckConnection;
import com.growindigo.connectplus.utils.Constants;
import com.growindigo.connectplus.utils.Preferences;
import com.growindigo.connectplus.utils.Utils;
import com.growindigo.connectplus.volley.ApiRequest;
import com.growindigo.connectplus.volley.IApiResponse;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class dist_salesvisit extends AppCompatActivity implements salesorderAdaptor.RemoveClickListner, IApiResponse, LocationListener {

    TextView DistributorName, lbldistName;
    EditText txtunit, txtRate, txtvalue;
    SearchableSpinner spn_material_name;
    Button btn_add, btn_Submit;
    EditText txtRemark, txtTotalPrice;
    String ReferanceFrom = "";
    String FromScreen = "";
    String DistributorCode;
    ArrayList<ProductModel> mProduct_list;
    String prd_code = "";

    LocationManager locationManager;
    DBCreation db;
    private double longitude;
    private double latitude;
    String cordinates = "";
    String geoAddress = "";
    ProgressDialog pDialog;
    String Product, Productcode, unit, Price, TotalProductPrice;
    private RecyclerView mRecyclerView;
    private salesorderAdaptor mRecyclerAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    ArrayList<sales_order_class> myList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dist_salesvisit);
        Initializecontrol();
        Intent i = getIntent();
        ReferanceFrom = i.getStringExtra("ReferanceFrom");
        FromScreen = i.getStringExtra("ref_From");
        DistributorName.setText(i.getStringExtra("dist_name"));
        DistributorCode = i.getStringExtra("dist_code");

        if (ReferanceFrom.equals("DistributorTab")) {
            getSupportActionBar().setTitle("Distributor Sales Visit");
            lbldistName.setText("Distributor Name");
        } else if (ReferanceFrom.equals("RetailerTab")) {
            getSupportActionBar().setTitle("Retailer Sales Visit");
            lbldistName.setText("Retailer Name");
        }

        if (ReferanceFrom.equals("DistributorTab") && FromScreen.equals("StockReport")) {
            getSupportActionBar().setTitle("Distributor Stock Report");
            lbldistName.setText("Distributor Name");
        } else if (ReferanceFrom.equals("RetailerTab") && FromScreen.equals("StockReport")) {
            getSupportActionBar().setTitle("Retailer Stock Report");
            lbldistName.setText("Retailer Name");
        }
        loadSpinnerData();

        spn_material_name.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                prd_code = mProduct_list.get(position).getProduct_code();

                Cursor data = db.ProductPrice(prd_code);//ret_price

                if (data != null && data.getCount() > 0) {
                    if (data.moveToFirst()) {
                        do {
                            if (ReferanceFrom.equals("DistributorTab")) {
                                txtRate.setText(data.getString(data.getColumnIndex("dist_price")));
                            } else if (ReferanceFrom.equals("RetailerTab")) {
                                txtRate.setText(data.getString(data.getColumnIndex("ret_price")));
                            }
                        } while (data.moveToNext());
                    }
                }
                data.close();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        txtunit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (txtunit.getText().length() == 0) {
                    txtvalue.setText("0");
                } else {
                    String finalPrice = String.valueOf(((Float.parseFloat(txtRate.getText().toString())) * (Float.parseFloat(txtunit.getText().toString()))));
                    txtvalue.setText(finalPrice);
                }
            }
        });

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validation() == true) {
                    Product = spn_material_name.getSelectedItem().toString();
                    Productcode = prd_code;
                    Price = txtRate.getText().toString();
                    unit = txtunit.getText().toString();
                    TotalProductPrice = txtvalue.getText().toString();
                    sales_order_class mLog = new sales_order_class();
                    mLog.setTxtProduct(Product);
                    mLog.setTxtProductcode(Productcode);
                    mLog.setTxtPrice(Price);
                    mLog.setTxtunit(unit);
                    mLog.setTotalPrice(TotalProductPrice);
                    myList.add(mLog);
                    mRecyclerAdapter.notifyData(myList);
                    txtunit.setText("");
                    txtvalue.setText("0");
                    btn_Submit.setVisibility(View.VISIBLE);
                    txtRemark.setVisibility(View.VISIBLE);
                    txtTotalPrice.setVisibility(View.VISIBLE);
                    Toast.makeText(dist_salesvisit.this, "Product Added in List", Toast.LENGTH_SHORT).show();
                    //Utils.showAlertDialog("Success", "Product Added in List", dist_salesvisit.this);
                    Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    vibe.vibrate(100);
                    String total = String.valueOf(grandTotal(myList));
                    txtTotalPrice.setText(total);
                } else {

                }
            }

        });

        btn_Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                JsonObject jsonObject = null;

                // ArrayList<SeedItems> modelLists = Preferences.getArrayList(context, Preferences.KEY_CART_ITEMS);

                jsonObject = createJson(myList);
                Log.v("SalesList: ", "FinalJSon" + jsonObject.toString());
                String userCode = Preferences.get(dist_salesvisit.this, Constants.PREF_USERCODE);
                uploadData(DistributorName.getText().toString(), DistributorCode, userCode,
                        cordinates, geoAddress, txtTotalPrice.getText().toString(), jsonObject.toString(), txtRemark.getText().toString());
            }
        });

    }

    protected boolean validation() {
        boolean flag = true;
        if (txtunit.getText().length() == 0) {
            Utils.showAlertDialog("Warning", "Enter Unit!", this);
            txtunit.setError("Required");
            return false;
        }
        return true;
    }

    private int grandTotal(List<sales_order_class> items) {
        float totalPrice = 0;
        for (int i = 0; i < items.size(); i++) {
            totalPrice += Float.parseFloat(items.get(i).getTotalPrice());
        }
        return (int) totalPrice;
    }

    public JsonObject createJson(ArrayList<sales_order_class> modelLists) {
        JsonArray mJsonArray = new JsonArray();
        JsonObject jsonResponse = new JsonObject();

        JsonObject finalJson = null;
        for (int i = 0; i < modelLists.size(); i++) {
            JsonObject model = new JsonObject();
            model.addProperty("product", modelLists.get(i).getTxtProduct());
            model.addProperty("productCode", modelLists.get(i).getTxtProductcode());
            model.addProperty("unit", modelLists.get(i).getTxtunit());
            model.addProperty("price", modelLists.get(i).getTxtPrice());
            mJsonArray.add(model);
        }
        jsonResponse.add("Table1", mJsonArray);
        finalJson = jsonResponse;
        return finalJson;
    }

    @Override
    public void OnRemoveClick(int index) {
        myList.remove(index);
        mRecyclerAdapter.notifyData(myList);
        String total = String.valueOf(grandTotal(myList));
        txtTotalPrice.setText(total);
        int count = myList.size();
        if (count == 0) {
            btn_Submit.setVisibility(View.GONE);
            txtRemark.setVisibility(View.GONE);
            txtTotalPrice.setVisibility(View.GONE);
        } else {
            btn_Submit.setVisibility(View.VISIBLE);
            txtRemark.setVisibility(View.VISIBLE);
            txtTotalPrice.setVisibility(View.VISIBLE);
        }
    }

    protected void Initializecontrol() {
        DistributorName = (TextView) findViewById(R.id.DistributorName);
        lbldistName = (TextView) findViewById(R.id.lbldistName);
        txtunit = (EditText) findViewById(R.id.txtunit);
        txtRate = (EditText) findViewById(R.id.txtRate);
        txtvalue = (EditText) findViewById(R.id.txtvalue);
        spn_material_name = (SearchableSpinner) findViewById(R.id.spn_material_name);
        btn_add = (Button) findViewById(R.id.btn_add);
        btn_Submit = (Button) findViewById(R.id.btn_Submit);
        db = new DBCreation(this);
        txtvalue.setText("0");
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerAdapter = new salesorderAdaptor(myList, this);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mRecyclerAdapter);
        checkInternet();
        pDialog = new ProgressDialog(this);
        btn_Submit.setVisibility(View.GONE);
        txtRemark = (EditText) findViewById(R.id.txtRemark);
        txtTotalPrice = (EditText) findViewById(R.id.txtTotalPrice);
        txtRemark.setVisibility(View.GONE);
        txtTotalPrice.setVisibility(View.GONE);
    }

    protected void loadSpinnerData() {
        mProduct_list = db.getProductListData();
        List<String> spnProduct_list = new ArrayList<String>();
        //spnProduct_list.add("Select Product");
        for (int i = 0; i < mProduct_list.size(); i++) {
            spnProduct_list.add(mProduct_list.get(i).getProduct_name());
        }
        spn_material_name.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, spnProduct_list));
    }

    protected void checkInternet() {
        if (CheckConnection.isNetworkAvailable(this)) {

        } else {
            Utils.showAlertDialogOk("Warning", "Internet Connection Not Available!", this);
        }
    }

    public void uploadData(String name, String code, String usercode, String cordinate, String geoaddress, String totalPrice, String encodedData, String remark) {

        //encodedData
        Map<String, String> paramsReq = new HashMap<String, String>();
        paramsReq.put("name", name);
        paramsReq.put("code", code);
        paramsReq.put("usercode", usercode);
        if (cordinate != null && cordinate.length() > 0)
            paramsReq.put("cordinate", cordinate);
        else
            paramsReq.put("cordinate", "0.0");
        if (geoaddress != null && geoaddress.length() > 0)
            paramsReq.put("geoaddress", geoaddress);
        else
            paramsReq.put("geoaddress", "NA");
        paramsReq.put("totalPrice", totalPrice);
        paramsReq.put("encodedData", encodedData);
        if (remark != null && remark.length() > 0)
            paramsReq.put("remark", remark);
        else
            paramsReq.put("remark", "NA");
        if (FromScreen.equals("StockReport")) {
            //Log.d("Mahyco", "Stock_upload");
            apiRequest(paramsReq, Constants.Stock_upload, Request.Method.POST);
        } else {
            //Log.d("Mahyco", "Sales order upload");
            apiRequest(paramsReq, Constants.Sales_order_upload, Request.Method.POST);
        }
    }

    private void apiRequest(Map<String, String> paramsReq, String tag, int method) {
        ApiRequest apiRequest = new ApiRequest(this, this);

        if (tag.equalsIgnoreCase(Constants.Sales_order_upload) || tag.equalsIgnoreCase(Constants.Stock_upload)) {
            if (FromScreen.equals("StockReport")) {
                pDialog.setMessage("Updating Stock Details");
                pDialog.setCanceledOnTouchOutside(false);
                pDialog.setCancelable(false);
                pDialog.show();
                apiRequest.postJSONRequestSales(Constants.BASE_URL + Constants.Stock_upload, tag,
                        paramsReq, method);
            } else {
                pDialog.setMessage("Generating Sales Order");
                pDialog.setCanceledOnTouchOutside(false);
                pDialog.setCancelable(false);
                pDialog.show();
                apiRequest.postJSONRequestSales(Constants.BASE_URL + Constants.Sales_order_upload, tag,
                        paramsReq, method);
            }
        } else if (tag.equalsIgnoreCase(Constants.activityTrackingUpload)) {
            apiRequest.postJSONRequestSales(Constants.BASE_URL + Constants.activityTrackingUpload, tag,
                    paramsReq, method);
        }
    }

    public void getLocation() {
        try {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
            }
            Criteria criteria = new Criteria();
            criteria.setVerticalAccuracy(Criteria.ACCURACY_HIGH);
            criteria.setPowerRequirement(Criteria.POWER_HIGH);
            criteria.setAltitudeRequired(false);
            criteria.setSpeedRequired(false);
            criteria.setCostAllowed(true);
            criteria.setBearingRequired(false);
            locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) this);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, (LocationListener) this);
            //locationManager.getLastKnownLocation(locationManager.GPS_PROVIDER);
            setUpMapIfNeeded();
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        longitude = location.getLongitude();
        latitude = location.getLatitude();
        cordinates = location.getLatitude() + "-" + location.getLongitude();
        getLocation();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, "Please Enable GPS", Toast.LENGTH_SHORT).show();
        final Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
    }

    private void setUpMapIfNeeded() {

        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        String result = null;
        try {
            List<Address> addressList = geocoder.getFromLocation(
                    latitude, longitude, 1);
            if (addressList != null && addressList.size() > 0) {
                Address address = addressList.get(0);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getAddressLine(i)).append("\n");//adress
                }
                geoAddress = address.getAddressLine(0).toString();
                result = sb.toString();
            }
        } catch (IOException e) {
            // Log.e(TAG, "Unable connect to Geocoder", e);
        }
    }

    @Override
    public void onResultReceived(String response, String tag) {

        pDialog.dismiss();
        if (tag.equalsIgnoreCase(Constants.Sales_order_upload) || tag.equalsIgnoreCase(Constants.Stock_upload)) {
            Utils.showAlertDialogOk("Success", "Sales Order Created !", this);
        } else if (tag.equalsIgnoreCase(Constants.activityTrackingUpload)) {
            //Log.d("activityTracking::", response);
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        //Log.d("Error::", error.toString());
        error.printStackTrace();
    }

    @Override
    public void onResume() {
        super.onResume();
        String userCode = Preferences.get(dist_salesvisit.this, Constants.PREF_USERCODE);
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());

        Utils.fetchCurrentLocation(userCode, String.valueOf(latitude), String.valueOf(longitude), this.getClass().getSimpleName(), formattedDate + " " + currentTime, this, this);
    }
}
