package com.growindigo.connectplus;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;

public class farmer extends AppCompatActivity {
    TabLayout tabLayout;
    ViewPager viewPager;
    String ReferanceFrom="";
    String Available="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farmer);
        getSupportActionBar().setTitle("Farmer Visit");

        Intent i = getIntent();
        Available=i.getStringExtra("ref_From");

        tabLayout=(TabLayout)findViewById(R.id.tabLayout);
        viewPager=(ViewPager)findViewById(R.id.viewPager);

        tabLayout.addTab(tabLayout.newTab().setText("New Farmer"));
        tabLayout.addTab(tabLayout.newTab().setText("Existing Farmer"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final MyAdapter adapter = new MyAdapter(this,getSupportFragmentManager(), tabLayout.getTabCount());
        if (Available.equals("Yes")) {
            viewPager.setCurrentItem(1);
            viewPager.setAdapter(adapter);
        }
        if (Available.equals("No"))
        {
            viewPager.setCurrentItem(0);
            viewPager.setAdapter(adapter);
        }
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
}
