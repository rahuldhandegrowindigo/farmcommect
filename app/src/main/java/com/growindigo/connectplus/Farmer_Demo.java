package com.growindigo.connectplus;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.growindigo.connectplus.databaseutils.DBCreation;
import com.growindigo.connectplus.databaseutils.Messageclass;
import com.growindigo.connectplus.model.CropModel;
import com.growindigo.connectplus.model.DistributorModel;
import com.growindigo.connectplus.model.ProductModel;
import com.growindigo.connectplus.utils.CheckConnection;
import com.growindigo.connectplus.utils.Constants;
import com.growindigo.connectplus.utils.Preferences;
import com.growindigo.connectplus.utils.Utils;
import com.growindigo.connectplus.volley.ApiRequest;
import com.growindigo.connectplus.volley.IApiResponse;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class Farmer_Demo extends AppCompatActivity implements LocationListener, IApiResponse {
    private SearchableSpinner spnSatisfaction, spnCropStage, spnCrop, spnProduct;
    private TextView farmerName, viewInfo;
    private String FarmerMobile;
    private String visitcount, DemoCheck;
    private EditText txtFarmerArea, txtDateOfSowing, txtdeoPurpose, txtDosage, txtRemark;
    private Switch switchPurchase;
    private String Str_spnVisitPurpose, Str_spnCrop, Str_spnPeriod, Str_spnCropStage, Str_spnObservation, Str_spnProduct,
            Str_txtDateOfSowing, Str_txtFarmerArea, Str_txtDosage, Str_purchaseProduct, Str_Remark;

    private LinearLayout layout2ndVisit;
    LocationManager locationManager;
    DBCreation db;
    private double longitude;
    private double latitude;
    Calendar myCalendar;
    Messageclass msclass;
    String cordinates = "";
    String currentDateandTime = "";
    String geoAddress = "";
    private Button btn_FarmerPhoto, btn_farmerSave;
    int imageselect;
    File photoFile = null;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    ImageView ivImage;
    EditText txtpalce;
    private static final String IMAGE_DIRECTORY_NAME = "VISITPHOTO";
    private String Imagepath1;
    DatePickerDialog picker;
    ArrayList<CropModel> mCrop_list;
    ArrayList<ProductModel> mProduct_list;
    ArrayList<DistributorModel> mDistributor_list;
    private String up_farmer_name, up_farmer_mobile, up_cordinates, up_geoaddress, up_crop, up_area,
            up_product, up_crop_stage, up_sowing_date, up_dosage, up_image, up_satisfaction,
            up_usercode, up_tr_date, up_uploaded, up_Remark;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farmer__demo);
        getSupportActionBar().setTitle("Demonstration");
        Initializecontrol();
        mCrop_list = db.getCropListData();
       // Log.d("CropSize: ", mCrop_list.size() + "");
        mProduct_list = db.getProductListData();
        //Log.d("CropSizeP: ", mProduct_list.size() + "");
        loadspinnerdata();
        if (DemoCheck.equals("Democheck")) {
            layout2ndVisit.setVisibility(View.VISIBLE);
        } else {
            layout2ndVisit.setVisibility(View.GONE);
        }
        final Calendar myCalendar = Calendar.getInstance();
        txtDateOfSowing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog
                picker = new DatePickerDialog(Farmer_Demo.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                //txtDateOfSowing.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                Calendar c = Calendar.getInstance();
                                c.set(year, monthOfYear, dayOfMonth);

                                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                                String formattedDate = sdf.format(c.getTime());
                                txtDateOfSowing.setText(formattedDate);
                                Toast.makeText(Farmer_Demo.this, formattedDate, Toast.LENGTH_LONG).show();
                            }
                        }, year, month, day);
                picker.show();
            }
        });
        btn_FarmerPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(Farmer_Demo.this, android.Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_DENIED) {
                }
                imageselect = 1;
                try {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        captureImage();
                    } else {
                        captureImage2();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    msclass.showMessage(ex.getMessage());
                }
            }

        });

        btn_farmerSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validation() == true) {
                    //visitcount=(Integer.parseInt(visitcount)+1);
                    Map<String, String> paramsReq = new HashMap<String, String>();
                    String userCode = Preferences.get(Farmer_Demo.this, Constants.PREF_USERCODE);
                    paramsReq.put("usercode", userCode);
                    int visit = (Integer.parseInt(visitcount) + 1);
                    boolean result = db.InsertDemoVisitData(farmerName.getText().toString(), cordinates, geoAddress,
                            FarmerMobile, spnCrop.getSelectedItem().toString(), txtFarmerArea.getText().toString(),
                            spnCropStage.getSelectedItem().toString(), txtDateOfSowing.getText().toString(),
                            spnProduct.getSelectedItem().toString(), txtDosage.getText().toString(),
                            Imagepath1, userCode, currentDateandTime, txtRemark.getText().toString(), spnSatisfaction.getSelectedItem().toString());
                    if (result) {
                        Utils.showAlertDialogOk("SUCCESS", "Data Save Successfully", Farmer_Demo.this);

                    } else {
                        msclass.showMessage("Something went wrong!");
                    }
                    if (CheckConnection.isNetworkAvailable(Farmer_Demo.this)) {
                        String searchQuery = "select  *  from FaremerDemo where  uploaded ='0'";
                        Cursor cursor = db.getReadableDatabase().rawQuery(searchQuery, null);
                        int count = cursor.getCount();
                        if (count > 0) {
                            if (cursor.moveToFirst()) {
                                do {
                                    up_farmer_name = cursor.getString(cursor.getColumnIndex("FarmerName"));
                                    up_farmer_mobile = cursor.getString(cursor.getColumnIndex("Mobile"));
                                    up_cordinates = cursor.getString(cursor.getColumnIndex("cordinate"));
                                    up_geoaddress = cursor.getString(cursor.getColumnIndex("geoaddress"));
                                    up_crop = cursor.getString(cursor.getColumnIndex("crop"));
                                    up_area = cursor.getString(cursor.getColumnIndex("area"));
                                    up_product = cursor.getString(cursor.getColumnIndex("prododuct"));
                                    up_crop_stage = cursor.getString(cursor.getColumnIndex("cropStage"));
                                    up_sowing_date = cursor.getString(cursor.getColumnIndex("sowingDate"));
                                    up_dosage = cursor.getString(cursor.getColumnIndex("dosage"));
                                    up_image = cursor.getString(cursor.getColumnIndex("img"));
                                    up_satisfaction = cursor.getString(cursor.getColumnIndex("Satisfaction"));

                                    up_usercode = cursor.getString(cursor.getColumnIndex("usercode"));
                                    up_tr_date = cursor.getString(cursor.getColumnIndex("tr_date"));
                                    up_uploaded = cursor.getString(cursor.getColumnIndex("uploaded"));
                                    up_Remark = cursor.getString(cursor.getColumnIndex("Remark"));
                                    uploadData(up_farmer_name, up_farmer_mobile, up_cordinates, up_geoaddress, up_crop, up_area,
                                            up_product, up_crop_stage, up_sowing_date, up_dosage, up_image, up_satisfaction,
                                            up_usercode, up_tr_date, up_uploaded, up_Remark);
                                }
                                while (cursor.moveToNext());
                            } else {
                                Toast.makeText(Farmer_Demo.this, "Data Will Upload When Internet Available", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            }
        });
    }

    final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {


        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            // TODO Auto-generated method stub
            String myFormat = "dd-MM-yyyy"; //In which you need put here
            SimpleDateFormat sdformat = new SimpleDateFormat(myFormat, Locale.US);
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            txtDateOfSowing.setText(sdformat.format(myCalendar.getTime()));
        }

    };

    protected void Initializecontrol() {
        spnCrop = (SearchableSpinner) findViewById(R.id.spnCrop);
        spnSatisfaction = (SearchableSpinner) findViewById(R.id.spnSatisfaction);
        spnCropStage = (SearchableSpinner) findViewById(R.id.spnCropStage);
        spnProduct = (SearchableSpinner) findViewById(R.id.spnProduct);
        txtDateOfSowing = (EditText) findViewById(R.id.txtDateOfSowing);
        farmerName = (TextView) findViewById(R.id.farmerName);
        txtFarmerArea = (EditText) findViewById(R.id.txtFarmerArea);
        txtDosage = (EditText) findViewById(R.id.txtDosage);
        switchPurchase = (Switch) findViewById(R.id.switchPurchase);
        txtRemark = (EditText) findViewById(R.id.txtRemark);
        layout2ndVisit = (LinearLayout) findViewById(R.id.layout2ndVisit);
        msclass = new Messageclass(this);
        db = new DBCreation(this);
        btn_FarmerPhoto = (Button) findViewById(R.id.btn_FarmerPhoto);
        btn_farmerSave = (Button) findViewById(R.id.btn_farmerSave);
        ivImage = (ImageView) findViewById(R.id.ivImage);
        getLocation();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        currentDateandTime = sdf.format(new Date());
        viewInfo = (TextView) findViewById(R.id.viewInfo);
        Intent i = getIntent();
        farmerName.setText(i.getStringExtra("farmerName"));
        FarmerMobile = i.getStringExtra("mobile");
        visitcount = i.getStringExtra("visitcount");
        DemoCheck = i.getStringExtra("DemoFollow");
        userinfo();
    }

    protected void loadspinnerdata() {
        List<String> spnCrop_list = new ArrayList<String>();
        spnCrop_list.add("Select Crop");
        for (int i = 0; i < mCrop_list.size(); i++) {
            spnCrop_list.add(mCrop_list.get(i).getCrop_name());
        }
        spnCrop.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, spnCrop_list));


        List<String> spnProduct_list = new ArrayList<String>();
        spnProduct_list.add("Select Product");
        for (int i = 0; i < mProduct_list.size(); i++) {
            spnProduct_list.add(mProduct_list.get(i).getProduct_name());
        }
        spnProduct.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, spnProduct_list));


        List<String> spnCropStage_list = new ArrayList<String>();
        spnCropStage_list.add("Select Crop Stage");
        spnCropStage_list.add("Land Preparation");
        spnCropStage_list.add("Sowing");
        spnCropStage_list.add("Seedling / Transplanting");
        spnCropStage_list.add("Branching");
        spnCropStage_list.add("Flowering");
        spnCropStage.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, spnCropStage_list));

        List<String> spnSatisfaction_list = new ArrayList<String>();
        spnSatisfaction_list.add("Select Satisfaction Level");
        spnSatisfaction_list.add("Excellent");
        spnSatisfaction_list.add("Very Good");
        spnSatisfaction_list.add("Good");
        spnSatisfaction_list.add("Okay - Okay");
        spnSatisfaction_list.add("Not satisfied");
        spnSatisfaction.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, spnSatisfaction_list));

    }

    public void userinfo() {
        Cursor data = db.userinfo(FarmerMobile);

        if (data.getCount() == 0) {
            //msclass.showMessage("No Previous Record Available... ");
        } else {
            data.moveToFirst();
            if (data != null) {
                do {
                    //Str_spnVisitPurpose = data.getString(data.getColumnIndex("FarmerName"));
                    Str_spnCrop = data.getString(data.getColumnIndex("crop"));
                    // Str_spnPeriod = data.getString(data.getColumnIndex("crop"));
                    Str_spnCropStage = data.getString(data.getColumnIndex("cropStage"));
                    Str_spnObservation = data.getString(data.getColumnIndex("observation"));
                    Str_spnProduct = data.getString(data.getColumnIndex("prododuct"));
                    Str_txtDateOfSowing = data.getString(data.getColumnIndex("sowingDate"));
                    Str_txtFarmerArea = data.getString(data.getColumnIndex("area"));
                    Str_txtDosage = data.getString(data.getColumnIndex("dosage"));
                    Str_purchaseProduct = data.getString(data.getColumnIndex("purchaseProduct"));
                    Str_Remark = data.getString(data.getColumnIndex("Remark"));
                } while (data.moveToNext());
//                spnVisitPurpose.setSe(Str_spnVisitPurpose);
//                spnCrop.setText(Str_spnCrop);
//                spnPeriod.setText(Str_spnPeriod);
//                spnCropStage.setText(Str_spnCropStage);
//                spnObservation.setText(Str_spnObservation);
//                spnProduct.setText(Str_spnProduct);
                txtDateOfSowing.setText(Str_txtDateOfSowing);
                txtFarmerArea.setText(Str_txtFarmerArea);
                txtDosage.setText(Str_txtDosage);
                txtRemark.setText(Str_Remark);
            }
            data.close();
        }

    }

    public void getLocation() {
        try {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);

            }
            Criteria criteria = new Criteria();
            criteria.setVerticalAccuracy(Criteria.ACCURACY_HIGH);
            criteria.setPowerRequirement(Criteria.POWER_HIGH);
            criteria.setAltitudeRequired(false);
            criteria.setSpeedRequired(false);
            criteria.setCostAllowed(true);
            criteria.setBearingRequired(false);
            locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) this);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, (LocationListener) this);
            //locationManager.getLastKnownLocation(locationManager.GPS_PROVIDER);
            setUpMapIfNeeded();
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        longitude = location.getLongitude();
        latitude = location.getLatitude();
        cordinates = location.getLatitude() + "-" + location.getLongitude();
        getLocation();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, "Please Enable GPS", Toast.LENGTH_SHORT).show();
        final Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
    }

    private void setUpMapIfNeeded() {

        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        String result = null;
        try {
            List<Address> addressList = geocoder.getFromLocation(
                    latitude, longitude, 1);
            if (addressList != null && addressList.size() > 0) {
                Address address = addressList.get(0);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getAddressLine(i)).append("\n");//adress
                }
                geoAddress = address.getAddressLine(0).toString();
                result = sb.toString();
            }
        } catch (IOException e) {
            // Log.e(TAG, "Unable connect to Geocoder", e);
        }


    }

    private void captureImage2() {

        try {
            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

            if (imageselect == 1) {
                photoFile = createImageFile4();
                if (photoFile != null) {
                    Log.i("Mayank", photoFile.getAbsolutePath());
                    Uri photoURI = Uri.fromFile(photoFile);
                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(cameraIntent, REQUEST_CAMERA);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            msclass.showMessage(ex.getMessage());
        }
    }

    private void captureImage() {

        try {

            if (ContextCompat.checkSelfPermission(this
                    , android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
            } else {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(this.getPackageManager()) != null) {
                    // Create the File where the photo should go
                    try {
                        if (imageselect == 1) {
                            photoFile = createImageFile();

                            if (photoFile != null) {
                                Uri photoURI = FileProvider.getUriForFile(this,
                                        "com.growindigo.connectplus",
                                        photoFile);
                                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                                startActivityForResult(takePictureIntent, REQUEST_CAMERA);
                            }
                        }

                    } catch (Exception ex) {

                        msclass.showMessage(ex.toString());
                        ex.printStackTrace();
                    }


                } else {
                }
            }
        } catch (Exception ex) {
            msclass.showMessage(ex.getMessage());
            ex.printStackTrace();
            //dialog.dismiss();
        }
    }

    private File createImageFile4() //  throws IOException
    {
        File mediaFile = null;
        try {
            // External sdcard location
            File mediaStorageDir = new File(
                    Environment
                            .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                    IMAGE_DIRECTORY_NAME);
            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    return null;
                }
            }
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                    Locale.getDefault()).format(new Date());
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } catch (Exception ex) {
            ex.printStackTrace();
            msclass.showMessage(ex.getMessage());
        }
        return mediaFile;
    }

    private File createImageFile() {
        // Create an image file name
        File image = null;
        try {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = "JPEG_" + timeStamp + "_";
            File storageDir = this.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );

        } catch (Exception ex) {
            ex.printStackTrace();
            msclass.showMessage(ex.toString());
        }
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == SELECT_FILE)
                    onSelectFromGalleryResult(data);
                else if (requestCode == REQUEST_CAMERA)
                    onCaptureImageResult(data);
            }
        } catch (Exception e) {
            e.printStackTrace();
            msclass.showMessage(e.toString());
        }
    }

    private void onCaptureImageResult(Intent data) {


        try {
            if (imageselect == 1) {

                try {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 2;

                    Bitmap myBitmap = BitmapFactory.decodeFile(photoFile.getAbsolutePath(), options);
                    //Imagepath1 = Utils.getBase64FromPath(photoFile.getAbsolutePath());
                    Imagepath1 = photoFile.getAbsolutePath();
                    ivImage.setImageBitmap(myBitmap);
                    ivImage.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    msclass.showMessage(e.toString());
                    e.printStackTrace();
                }
                //end
            }

        } catch (Exception e) {
            msclass.showMessage(e.toString());
            e.printStackTrace();
        }

    }

    private void onSelectFromGalleryResult(Intent data) {


        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(this.getApplicationContext().getContentResolver(), data.getData());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (imageselect == 1) {
            ivImage.setImageBitmap(bm);
            ivImage.setVisibility(View.VISIBLE);
        }

    }

    protected boolean validation() {
        boolean flag = true;

        if (txtFarmerArea.getText().length() == 0) {

            msclass.showMessage("Please Enter Area ");
            txtFarmerArea.setError("Required");
            return false;
        }
        if (txtDosage.getText().length() == 0) {
            msclass.showMessage("Please Enter Dosage ");
            txtDosage.setError("Required");
            return false;
        }
        if (txtDateOfSowing.getText().length() == 0) {
            msclass.showMessage("Please Select Sowing Date ");
            txtDateOfSowing.setError("Required");
            return false;
        }

        if (spnCrop.getSelectedItem().toString().equals("Select Crop")) {
            msclass.showMessage("Please Select Crop");
            return false;
        }

        if (spnCropStage.getSelectedItem().toString().equals("Select Crop Stage")) {
            msclass.showMessage("Please Select Crop Stage");
            return false;
        }
        if (spnProduct.getSelectedItem().toString().equals("Select Product")) {
            msclass.showMessage("Please Select Product");
            return false;
        }

        if (!hasImage(ivImage)) {
            msclass.showMessage("Please Capture Image");
            return false;
        }
        return true;
    }

    private boolean hasImage(@NonNull ImageView view) {
        Drawable drawable = view.getDrawable();
        boolean hasImage = (drawable != null);
        if (hasImage && (drawable instanceof BitmapDrawable)) {
            hasImage = ((BitmapDrawable) drawable).getBitmap() != null;
        }
        return hasImage;
    }

    public void uploadData(String up_farmer_name, String up_farmer_mobile, String up_cordinates, String up_geoaddress,
                           String up_crop, String up_area, String up_product, String up_crop_stage,
                           String up_sowing_date, String up_dosage, String up_image, String up_satisfaction,
                           String up_usercode, String up_tr_date, String up_uploaded, String up_Remark) {

        String converdImage = "";
        if (up_image != null && up_image.length() > 0)
            converdImage = Utils.getImageDatadetail(up_image);

        Map<String, String> paramsReq = new HashMap<String, String>();
        paramsReq.put("farmer_name", up_farmer_name);
        paramsReq.put("farmer_mobile", up_farmer_mobile);
        if (up_cordinates != null && up_cordinates.length() > 0)
            paramsReq.put("cordinates", up_cordinates);
        else
            paramsReq.put("cordinates", "0");
        if (up_cordinates != null && up_cordinates.length() > 0)
        paramsReq.put("geoaddress", up_geoaddress);
        else
            paramsReq.put("geoaddress", "0");
        paramsReq.put("crop", up_crop);
        paramsReq.put("area", up_area);
        paramsReq.put("product", up_product);
        paramsReq.put("crop_stage", up_crop_stage);
        paramsReq.put("sowing_date", up_sowing_date);
        paramsReq.put("dosage", up_dosage);
        paramsReq.put("image", converdImage);
        paramsReq.put("satisfaction", up_satisfaction);
        paramsReq.put("usercode", up_usercode);
        paramsReq.put("tr_date", up_tr_date);
        paramsReq.put("uploaded", up_uploaded);
        paramsReq.put("remark", up_Remark);
        db.updateFarmerDemo(up_farmer_mobile, "1");
        apiRequest(paramsReq, Constants.UplaodFarmerDemoData, Request.Method.POST);
    }

    private void apiRequest(Map<String, String> paramsReq, String tag, int method) {
        ApiRequest apiRequest = new ApiRequest(this, this);
        apiRequest.postJSONRequest(Constants.BASE_URL + Constants.UplaodFarmerDemoData, tag,
                paramsReq, method);
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        //Log.d("upload:: ", response);
      //  Toast.makeText(Farmer_Demo.this, "Data Uploaded Successfully", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        //Log.d("Error::", error.getMessage());
    }
    @Override
    public void onResume() {
        super.onResume();
        String userCode = Preferences.get(Farmer_Demo.this, Constants.PREF_USERCODE);
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
        Utils.fetchCurrentLocation(userCode, String.valueOf(latitude), String.valueOf(longitude), this.getClass().getSimpleName(), formattedDate + " " + currentTime, this, this);
    }
}
