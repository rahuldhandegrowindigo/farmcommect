package com.growindigo.connectplus;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.androidbuts.multispinnerfilter.KeyPairBoolData;
import com.androidbuts.multispinnerfilter.MultiSpinnerSearch;
import com.androidbuts.multispinnerfilter.SpinnerListener;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.growindigo.connectplus.databaseutils.DBCreation;
import com.growindigo.connectplus.databaseutils.Messageclass;
import com.growindigo.connectplus.model.DistributorModel;
import com.growindigo.connectplus.model.RetailerModel;
import com.growindigo.connectplus.utils.CheckConnection;
import com.growindigo.connectplus.utils.Constants;
import com.growindigo.connectplus.utils.Preferences;
import com.growindigo.connectplus.utils.Utils;
import com.growindigo.connectplus.volley.ApiRequest;
import com.growindigo.connectplus.volley.IApiResponse;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class distributor extends AppCompatActivity implements IApiResponse, LocationListener {
    private SearchableSpinner spnDisributorList, spnDisributorVisitPurpose, spnActivityPlan;
    MultiSpinnerSearch spnDisributorListWithCode;
    private Button btn_next, btn_addDist, btn_assign;
    private TextView txtActivityPlan, lbltypename, lbltypenamewithcode, txtdistassing;
    Messageclass ms;
    DBCreation db;
    ArrayList<DistributorModel> mDistributor_list;
    ArrayList<DistributorModel> mDistributor_listWithCode;
    ArrayList<DistributorModel> mDistributor_code;
    ArrayList<String> mDistributor_codeAssign;
    ArrayList<String> mDistributor_nameAssign;
    // ArrayList<KeyPairBoolData> mDistributor_nameAssign;
    LocationManager locationManager;
    ArrayList<RetailerModel> mRetailer_list;
    ArrayList<RetailerModel> mRetailer_code;
    String distcode = "";
    String ReferanceFrom = "";
    String Mobile = "";
    String up_distName, up_distCode, up_retCode, up_MobileNo, up_retName, up_territorycode, up_userCode;
    GridView gridView1;
    private ArrayList<String> list;
    private ArrayAdapter<String> adapter;
    String name = "";
    private double longitude;
    private double latitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distributor);

        spnDisributorList = (SearchableSpinner) findViewById(R.id.spnDisributorList);
        spnDisributorListWithCode = (MultiSpinnerSearch) findViewById(R.id.spnDisributorListWithCode);
        spnDisributorVisitPurpose = (SearchableSpinner) findViewById(R.id.spnDisributorVisitPurpose);
        spnActivityPlan = (SearchableSpinner) findViewById(R.id.spnActivityPlan);
        btn_next = (Button) findViewById(R.id.btn_next);
        btn_assign = (Button) findViewById(R.id.btn_assign);
        btn_addDist = (Button) findViewById(R.id.btn_addDist);
        txtActivityPlan = (TextView) findViewById(R.id.txtActivityPlan);
        lbltypename = (TextView) findViewById(R.id.lbltypename);
        lbltypenamewithcode = (TextView) findViewById(R.id.lbltypenamewithcode);
        mDistributor_codeAssign = new ArrayList<String>();
        mDistributor_nameAssign = new ArrayList<String>();
        spnDisributorListWithCode.setEmptyTitle("Not Data Found!");
        spnDisributorListWithCode.setSearchHint("Find Data");
        ms = new Messageclass(this);
        db = new DBCreation(this);
        getLocation();
        /* Gridview Code*/
        gridView1 = (GridView) findViewById(R.id.gridView1);
        //  txtdistassing=(TextView)findViewById(R.id.txtdistassing);
        //ArrayList
        list = new ArrayList<String>();
        adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, list);
        /* Gridview Code End*/
        Intent i = getIntent();
        ReferanceFrom = i.getStringExtra("ref_From");
        Mobile = i.getStringExtra("mobile");
        if (ReferanceFrom.equals("DistributorTab")) {
            getSupportActionBar().setTitle("Distributor Visit");
            btn_addDist.setText("Want to add new Distributor?");
            lbltypename.setText("Select Distributor");
        } else if (ReferanceFrom.equals("RetailerTab")) {
            getSupportActionBar().setTitle("Retailer Visit");
            btn_addDist.setText("Want to add new Retailer?");
            lbltypename.setText("Select Retailer");

        }
        loadspinnerdata();

        Animation slide_down = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_down);
        btn_addDist.setAnimation(slide_down);
        spnDisributorVisitPurpose.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = spnDisributorVisitPurpose.getSelectedItem().toString();
                if (selectedItem.equals("Activity Planning")) {
                    spnActivityPlan.setVisibility(View.VISIBLE);
                    txtActivityPlan.setVisibility(View.VISIBLE);
                } else {
                    spnActivityPlan.setVisibility(View.INVISIBLE);
                    txtActivityPlan.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                spnActivityPlan.setVisibility(View.INVISIBLE);
                txtActivityPlan.setVisibility(View.INVISIBLE);
            }
        });

        spnDisributorList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (ReferanceFrom.equals("DistributorTab")) {
                    distcode = mDistributor_code.get(position).getDistributorCode();
                } else if (ReferanceFrom.equals("RetailerTab")) {
                    distcode = mRetailer_code.get(position).getRetailerCode();
                    loadGridviewData();
                }

                //Toast.makeText(getApplicationContext(),distcode, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btn_assign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveDistAssign();
            }
        });


        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String selectedDistributor = "";
                if (spnDisributorList != null && spnDisributorList.getCount() > 0) {
                    if (spnDisributorList.getSelectedItem().toString() != null) {
                        if (spnDisributorList.getSelectedItem().toString().equals("Select Retailer")) {
                            ms.showMessage("Kindly Select Retailer");
                        } else {
                            String selectedItem = spnDisributorVisitPurpose.getSelectedItem().toString();
                            if (selectedItem.equals("Sales")) {
                                // startActivity(new Intent(distributor.this, dist_salesvisit.class));
                                // Toast.makeText(getApplicationContext(),"Under Development", Toast.LENGTH_SHORT).show();
                                Intent i = new Intent(distributor.this, dist_salesvisit.class);
                                i.putExtra("dist_name", spnDisributorList.getSelectedItem().toString());
                                i.putExtra("dist_code", distcode);
                                i.putExtra("ReferanceFrom", ReferanceFrom);
                                i.putExtra("ref_From", "test");
                                startActivity(i);
                            } else if (selectedItem.equals("Complaint")) {
                                //startActivity(new Intent(distributor.this, dist_complaint.class));

                                Intent i = new Intent(distributor.this, dist_complaint.class);
                                i.putExtra("dist_name", spnDisributorList.getSelectedItem().toString());
                                i.putExtra("dist_code", distcode);
                                startActivity(i);
                            } else if (selectedItem.equals("Collection")) {
                                //startActivity(new Intent(distributor.this, dist_collection.class));
                                Intent i = new Intent(distributor.this, dist_collection.class);
                                i.putExtra("dist_name", spnDisributorList.getSelectedItem().toString());
                                i.putExtra("dist_code", distcode);
                                startActivity(i);

                            } else if (selectedItem.equals("General Visit")) {
                                //startActivity(new Intent(distributor.this, dist_collection.class));
                                Intent i = new Intent(distributor.this, general_visit.class);
                                i.putExtra("dist_name", spnDisributorList.getSelectedItem().toString());
                                i.putExtra("dist_code", distcode);
                                i.putExtra("ReferanceFrom", ReferanceFrom);
                                startActivity(i);

                            } else if (selectedItem.equals("Stock Report")) {
                                Intent i = new Intent(distributor.this, dist_salesvisit.class);
                                i.putExtra("ref_From", "StockReport");
                                i.putExtra("ReferanceFrom", ReferanceFrom);
                                i.putExtra("dist_name", spnDisributorList.getSelectedItem().toString());
                                i.putExtra("dist_code", distcode);
                                startActivity(i);
                            } else if (selectedItem.equals("New Distributor Appointment")) {
                                //startActivity(new Intent(distributor.this, dist_collection.class));
                                Intent i = new Intent(distributor.this, dist_appointment.class);
                                i.putExtra("ReferanceFrom", ReferanceFrom);
                                // i.putExtra("dist_code",distcode);
                                startActivity(i);

                            } else if (selectedItem.equals("Activity Planning")) {
                                if (spnActivityPlan.getSelectedItem().equals("Jeep Campaign")) {
                                    Intent i = new Intent(distributor.this, Campaign.class);
                                    i.putExtra("ref_From", "jeep");
                                    startActivity(i);
                                } else if (spnActivityPlan.getSelectedItem().equals("Meeting (Farmer/Retailer)")) {
                                    Intent i = new Intent(distributor.this, Campaign.class);
                                    i.putExtra("ref_From", "Meeting");
                                    startActivity(i);
                                } else {
                                    ms.showMessage("Select Activity Planning");
                                }
                            } else if (selectedItem.equals("Select Visit Purpose")) {
                                ms.showMessage("Select Visit Purpose");
                            }
                        }
                    }
                } else {
                    if (ReferanceFrom.equals("DistributorTab")) {
                        ms.showMessage("Kindly add new distributor");
                    } else if (ReferanceFrom.equals("RetailerTab")) {
                        ms.showMessage("Kindly add new retailer");
                    }

                }
            }
        });

        btn_addDist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(distributor.this, dist_appointment.class);
                i.putExtra("ReferanceFrom", ReferanceFrom);
                startActivity(i);
            }
        });
    }

    protected void loadGridviewData() {
        Cursor cursor = db.DistAssing(distcode);
        int count = cursor.getCount();
        if (count > 0) {
            gridView1.setVisibility(View.VISIBLE);
            spnDisributorListWithCode.setVisibility(View.GONE);
            lbltypenamewithcode.setVisibility(View.GONE);
            btn_assign.setVisibility(View.GONE);
            //  txtdistassing.setVisibility(View.VISIBLE);
            list.clear();
            if (cursor.moveToFirst()) {
                do {
                    name = cursor.getString(cursor.getColumnIndex("dist_name"));
                    //add in to array list
                    list.add(name);
                    gridView1.setAdapter(adapter);
                }
                while (cursor.moveToNext());
            }
        } else {
            gridView1.setVisibility(View.GONE);
            //  txtdistassing.setVisibility(View.GONE);
            spnDisributorListWithCode.setVisibility(View.VISIBLE);
            lbltypenamewithcode.setVisibility(View.VISIBLE);
            btn_assign.setVisibility(View.VISIBLE);
        }
    }

    protected void saveDistAssign() {

        Gson gson = new Gson();
        if (spnDisributorList != null && spnDisributorList.getCount() > 0) {
            if (spnDisributorList.getSelectedItem().toString() != null) {
                if (spnDisributorList.getSelectedItem().toString().equals("Select Retailer")) {
                    ms.showMessage("Kindly Select Retailer");
                } else {
                    if (mDistributor_codeAssign != null && mDistributor_codeAssign.size() > 0) {
                        if (mDistributor_nameAssign != null && mDistributor_nameAssign.size() > 0) {
                            String codeInputString = gson.toJson(mDistributor_codeAssign);
                            String nameInputString = gson.toJson(mDistributor_nameAssign);

                            System.out.println("codeInputString= " + codeInputString);
                            System.out.println("nameInputString= " + nameInputString);
                            boolean result = false;
                            for (int i = 0; i < mDistributor_codeAssign.size(); i++) {
                                result = db.insertDistAssignWithCode(mDistributor_codeAssign.get(i), mDistributor_nameAssign.get(i), distcode, spnDisributorList.getSelectedItem().toString());
                                if (result == true) {
                                    Utils.showAlertDialogOk("SUCCESS", "Data Save Successfully", this);
                                } else {
                                    Utils.showAlertDialogOk("Error", "Something Went Wrong", this);
                                }

                                if (CheckConnection.isNetworkAvailable(this)) {
                                    String searchQuery = "select * from distAssing where uploaded='0'";
                                    Cursor cursor = db.getReadableDatabase().rawQuery(searchQuery, null);
                                    int count = cursor.getCount();
                                    if (count > 0) {
                                        if (cursor.moveToFirst()) {
                                            do {
                                                up_distCode = cursor.getString(cursor.getColumnIndex("dist_code"));
                                                up_distName = cursor.getString(cursor.getColumnIndex("dist_name"));
                                                up_retCode = cursor.getString(cursor.getColumnIndex("ret_code"));
                                                up_retName = cursor.getString(cursor.getColumnIndex("ret_name"));
                                                up_territorycode = Preferences.get(this, Constants.PREF_Territory_code);
                                                up_userCode = Preferences.get(this, Constants.PREF_USERCODE);

                                                uploadData(up_distCode, up_distName, up_retCode, up_retName, up_territorycode, up_userCode, "RetailerTab");
                                            }
                                            while (cursor.moveToNext());
                                        }
                                    } else {
                                        Toast.makeText(this, "Data Will Upload When Internet Available", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        } else {
                            ms.showMessage("Kindly select distributor");
                        }
                    } else {
                        ms.showMessage("Kindly select distributor");
                    }
                }
            }
        }
    }


    protected void loadspinnerdata() {
        List<String> spnDisributorVisitPurpose_list = new ArrayList<String>();
        spnDisributorVisitPurpose_list.add("Select Visit Purpose");
        spnDisributorVisitPurpose_list.add("Sales");
        spnDisributorVisitPurpose_list.add("Stock Report");
        spnDisributorVisitPurpose_list.add("Complaint");
        spnDisributorVisitPurpose_list.add("Collection");
        spnDisributorVisitPurpose_list.add("Activity Planning");
        spnDisributorVisitPurpose_list.add("General Visit");
        //spnDisributorVisitPurpose_list.add("New Distributor Appointment");
        spnDisributorVisitPurpose.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, spnDisributorVisitPurpose_list));

        List<String> spnActivityPlan_list = new ArrayList<String>();
        spnActivityPlan_list.add("Activity Planning");
        spnActivityPlan_list.add("Jeep Campaign");
        spnActivityPlan_list.add("Meeting (Farmer/Retailer)");
        spnActivityPlan.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, spnActivityPlan_list));

        //// Bind Start

        if (Mobile.equals("NA")) {
            if (ReferanceFrom.equals("DistributorTab")) {
                mDistributor_code = db.getDistributorListData();
                List<String> dist_code = new ArrayList<>();
                //dist_code.add("0");
                for (int j = 0; j < mDistributor_code.size(); j++) {
                    dist_code.add(mDistributor_code.get(j).getDistributorCode());
                }

                mDistributor_list = db.getDistributorListData();
                List<String> spnDisributor_list = new ArrayList<String>();
                //spnDisributor_list.add("Select Distributor");
                for (int i = 0; i < mDistributor_list.size(); i++) {
                    spnDisributor_list.add(mDistributor_list.get(i).getDistributorName());
                }
                spnDisributorList.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, spnDisributor_list));
            } else if (ReferanceFrom.equals("RetailerTab")) {
                mRetailer_code = db.getRetailerListData();
                List<String> dist_code = new ArrayList<>();
                //dist_code.add("0");
                for (int j = 0; j < mRetailer_code.size(); j++) {
                    dist_code.add(mRetailer_code.get(j).getRetailerCode());
                }

                mRetailer_list = db.getRetailerListData();
                List<String> spnDisributor_list = new ArrayList<String>();
                for (int i = 0; i < mRetailer_list.size(); i++) {
                    if (i == 0) {
                        spnDisributor_list.add("Select Retailer");
                    } else {
                        spnDisributor_list.add(mRetailer_list.get(i).getRetailerName());
                    }
                }
                spnDisributorList.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, spnDisributor_list));
            }
        } else {
            if (ReferanceFrom.equals("DistributorTab")) {
                mDistributor_code = db.getDistributorListDataSearch(Mobile);
                List<String> dist_code = new ArrayList<>();
                //dist_code.add("0");
                for (int j = 0; j < mDistributor_code.size(); j++) {
                    dist_code.add(mDistributor_code.get(j).getDistributorCode());
                }

                mDistributor_list = db.getDistributorListDataSearch(Mobile);
                List<String> spnDisributor_list = new ArrayList<String>();
                //spnDisributor_list.add("Select Distributor");
                for (int i = 0; i < mDistributor_list.size(); i++) {
                    spnDisributor_list.add(mDistributor_list.get(i).getDistributorName());
                }
                spnDisributorList.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, spnDisributor_list));
            } else if (ReferanceFrom.equals("RetailerTab")) {
                mRetailer_code = db.getRetailerListDataSearch(Mobile);
                List<String> dist_code = new ArrayList<>();
                //dist_code.add("0");
                for (int j = 0; j < mRetailer_code.size(); j++) {
                    dist_code.add(mRetailer_code.get(j).getRetailerCode());
                }

                mRetailer_list = db.getRetailerListDataSearch(Mobile);
                List<String> spnDisributor_list = new ArrayList<String>();
                //spnDisributor_list.add("Select Distributor");
                for (int i = 0; i < mRetailer_list.size(); i++) {
                    spnDisributor_list.add(mRetailer_list.get(i).getRetailerName());
                }
                spnDisributorList.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, spnDisributor_list));
            }
            // Bind End
        }

        mDistributor_listWithCode = db.getDistributorListData();
        List<KeyPairBoolData> spnDisributor_listWithCode = new ArrayList<KeyPairBoolData>();
        //spnDisributor_list.add("Select Distributor");
        for (int i = 0; i < mDistributor_listWithCode.size(); i++) {
            KeyPairBoolData h = new KeyPairBoolData();
            h.setDistributorName(mDistributor_listWithCode.get(i).getDistributorName());
            h.setDistributorCode(mDistributor_listWithCode.get(i).getDistributorCode());
            h.setSelected(false);
            spnDisributor_listWithCode.add(h);
        }
        spnDisributorListWithCode.setAdapter(new ArrayAdapter<KeyPairBoolData>(this, android.R.layout.simple_spinner_dropdown_item, spnDisributor_listWithCode));
        //  final List<KeyPairBoolData> selectedItems = spnDisributorListWithCode.getSelectedItems();

        spnDisributorListWithCode.setItems(spnDisributor_listWithCode, -1, new SpinnerListener() {

            @Override
            public void onItemsSelected(List<KeyPairBoolData> items) {
                for (int i = 0; i < items.size(); i++) {
                    if (items.get(i).isSelected()) {
                        Log.i("MultiSelection: ", i + " : " + items.get(i).getDistributorName() + " : " + items.get(i).isSelected());
                        // mDistributor_nameAssign.add(items.get(i));
                        mDistributor_codeAssign.add(items.get(i).getDistributorCode());
                        mDistributor_nameAssign.add(items.get(i).getDistributorName());
                        //  if (selectedItems != null && selectedItems.size() > 0)
                        //      Log.i("MultiSelection1: ", selectedItems.get(0).getDistributorName());

                    }
                }
            }
        });
        //spnDisributorListWithCode.setT

        spnDisributorListWithCode.setColorSeparation(true);
    }

    public void uploadData(String distCode, String distName, String retCode, String retName, String territoryCode, String userCode, String up_isFrom) {

        Map<String, String> paramsReq = new HashMap<String, String>();

        paramsReq.put("dist_code", distCode);
        paramsReq.put("dist_name", distName);
        paramsReq.put("ret_code", retCode);
        paramsReq.put("ret_name", retName);
        paramsReq.put("territorycode", territoryCode);
        paramsReq.put("usercode", userCode);
        if (up_isFrom.equals("RetailerTab")) {
            db.updateNewDistAssign("1");
        }
        apiRequest(paramsReq, Constants.dist_assign, Request.Method.POST);
    }

    private void apiRequest(Map<String, String> paramsReq, String tag, int method) {
        ApiRequest apiRequest = new ApiRequest(this, this);
        apiRequest.postJSONRequestSales(Constants.BASE_URL + Constants.dist_assign, tag,
                paramsReq, method);
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        //  Toast.makeText(this, "Data Uploaded Successfully", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(this, error.toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResume() {
        super.onResume();
        String userCode = Preferences.get(distributor.this, Constants.PREF_USERCODE);
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
        Utils.fetchCurrentLocation(userCode, String.valueOf(latitude), String.valueOf(longitude), this.getClass().getSimpleName(), formattedDate + " " + currentTime, this, this);
    }

    @Override
    public void onLocationChanged(Location location) {
        longitude = location.getLongitude();
        latitude = location.getLatitude();
        getLocation();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        Toast.makeText(this, "Please Enable GPS", Toast.LENGTH_SHORT).show();
        final Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public void getLocation() {
        try {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
            }
            Criteria criteria = new Criteria();
            criteria.setVerticalAccuracy(Criteria.ACCURACY_HIGH);
            criteria.setPowerRequirement(Criteria.POWER_HIGH);
            criteria.setAltitudeRequired(false);
            criteria.setSpeedRequired(false);
            criteria.setCostAllowed(true);
            criteria.setBearingRequired(false);
            locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) this);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, (LocationListener) this);
            //locationManager.getLastKnownLocation(locationManager.GPS_PROVIDER);
            //setUpMapIfNeeded();
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }
}
