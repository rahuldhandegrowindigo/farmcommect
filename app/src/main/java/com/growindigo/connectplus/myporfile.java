package com.growindigo.connectplus;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.growindigo.connectplus.utils.Constants;
import com.growindigo.connectplus.utils.Preferences;
import com.growindigo.connectplus.utils.Utils;
import com.growindigo.connectplus.volley.IApiResponse;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class myporfile extends AppCompatActivity implements LocationListener, IApiResponse {
    private TextView territory, username, usercode;
    private Button btnOK;
    private double longitude;
    private double latitude;
    LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myporfile);
        getSupportActionBar().setTitle("My Profile");

        territory = (TextView) findViewById(R.id.territory);
        username = (TextView) findViewById(R.id.username);
        usercode = (TextView) findViewById(R.id.usercode);
        btnOK = (Button) findViewById(R.id.btnOK);
        getLocation();
        Map<String, String> paramsReq = new HashMap<String, String>();
        String Pref_userCode = Preferences.get(myporfile.this, Constants.PREF_USERCODE);
        String Pref_username = Preferences.get(myporfile.this, Constants.PREF_USERNAME);
        String Pref_Territory = Preferences.get(myporfile.this, Constants.PREF_Territory_Name);
        paramsReq.put("usercode", Pref_userCode);
        paramsReq.put("username", Pref_username);
        paramsReq.put("Territory_Name", Pref_Territory);
        territory.setText(Pref_Territory);
        usercode.setText(Pref_userCode);
        username.setText(Pref_username);

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(myporfile.this, MainActivity.class));
            }
        });
    }

    public void getLocation() {
        try {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
            }
            Criteria criteria = new Criteria();
            criteria.setVerticalAccuracy(Criteria.ACCURACY_HIGH);
            criteria.setPowerRequirement(Criteria.POWER_HIGH);
            criteria.setAltitudeRequired(false);
            criteria.setSpeedRequired(false);
            criteria.setCostAllowed(true);
            criteria.setBearingRequired(false);
            locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) this);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, (LocationListener) this);
            //locationManager.getLastKnownLocation(locationManager.GPS_PROVIDER);
            //setUpMapIfNeeded();
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        longitude = location.getLongitude();
        latitude = location.getLatitude();
        getLocation();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, "Please Enable GPS", Toast.LENGTH_SHORT).show();
        final Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {

    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResume() {
        super.onResume();
        String userCode = Preferences.get(myporfile.this, Constants.PREF_USERCODE);
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
        Utils.fetchCurrentLocation(userCode, String.valueOf(latitude), String.valueOf(longitude), this.getClass().getSimpleName(), formattedDate + " " + currentTime, this, this);
    }
}
