package com.growindigo.connectplus;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.growindigo.connectplus.databaseutils.DBCreation;
import com.growindigo.connectplus.databaseutils.Messageclass;
import com.growindigo.connectplus.model.CropModel;
import com.growindigo.connectplus.model.ProductModel;
import com.growindigo.connectplus.utils.Constants;
import com.growindigo.connectplus.utils.Preferences;
import com.growindigo.connectplus.utils.Utils;
import com.growindigo.connectplus.volley.ApiRequest;
import com.growindigo.connectplus.volley.IApiResponse;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class farmer_Complaint extends AppCompatActivity implements LocationListener, IApiResponse {
    private TextView FarmerName;
    private String farmerMobile;
    SearchableSpinner spn_Crop, spn_product;
    private EditText txtDose, txtRemark;
    private Button btn_complaintPhoto, btn_add;
    private String FarmerMobile;
    private String visitcount;
    private double longitude;
    private double latitude;
    Messageclass msclass;
    String cordinates = "";
    String currentDateandTime = "";
    int imageselect;
    File photoFile = null;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    ImageView ivImage;
    EditText txtpalce;
    private static final String IMAGE_DIRECTORY_NAME = "VISITPHOTO";
    private String Imagepath1;
    DBCreation db;
    String geoAddress = "";
    LocationManager locationManager;
    private String up_farmer_name,
            up_farmer_mobile,
            up_crop,
            up_product,
            up_dosage,
            up_remark,
            up_photo,
            up_usercode,
            up_tr_date,
            up_cordinate,
            up_geoaddress,
            up_uploaded;
    ArrayList<CropModel> mCrop_list;
    ArrayList<ProductModel> mProduct_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farmer__complaint);
        getSupportActionBar().setTitle("Farmer Complaint Visit");
        Initializecontrol();
        mCrop_list = db.getCropListData();
        //Log.d("CropSize: ", mCrop_list.size() + "");
        mProduct_list = db.getProductListData();
        //Log.d("CropSizeP: ", mProduct_list.size() + "");
        loadspinnerdata();

        btn_complaintPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_DENIED) {
                }
                imageselect = 1;
                try {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        captureImage();
                    } else {
                        captureImage2();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    msclass.showMessage(ex.getMessage());
                }
            }
        });

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validation() == true) {
                    Map<String, String> paramsReq = new HashMap<String, String>();
                    String userCode = Preferences.get(farmer_Complaint.this, Constants.PREF_USERCODE);
                    paramsReq.put("usercode", userCode);
                    boolean result = db.farmer_complaint_insert(FarmerName.getText().toString(), FarmerMobile, spn_Crop.getSelectedItem().toString(),
                            spn_product.getSelectedItem().toString(), txtDose.getText().toString(), txtRemark.getText().toString(),
                            Imagepath1, userCode, currentDateandTime, cordinates, geoAddress);
                    if (result) {
                        Utils.showAlertDialogOk("SUCCESS", "Data Save Successfully", farmer_Complaint.this);

                    } else {
                        Utils.showAlertDialogOk("Error", "Something Went Wrong", farmer_Complaint.this);
                    }

                    if (Utils.checkPermissionLocation(farmer_Complaint.this)) {
                        String searchQuery = "select  *  from Farmer_ComplaintTable where  uploaded ='0'";
                        Cursor cursor = db.getReadableDatabase().rawQuery(searchQuery, null);
                        int count = cursor.getCount();
                        if (count > 0) {
                            if (cursor.moveToFirst()) {
                                do {
                                    up_farmer_name = cursor.getString(cursor.getColumnIndex("farmer_name"));
                                    up_farmer_mobile = cursor.getString(cursor.getColumnIndex("farmer_mobile"));
                                    up_crop = cursor.getString(cursor.getColumnIndex("crop"));
                                    up_product = cursor.getString(cursor.getColumnIndex("product"));
                                    up_dosage = cursor.getString(cursor.getColumnIndex("dosage"));
                                    up_remark = cursor.getString(cursor.getColumnIndex("remark"));
                                    up_photo = cursor.getString(cursor.getColumnIndex("photo"));
                                    up_usercode = cursor.getString(cursor.getColumnIndex("usercode"));
                                    up_tr_date = cursor.getString(cursor.getColumnIndex("tr_date"));
                                    up_cordinate = cursor.getString(cursor.getColumnIndex("cordinate"));
                                    up_geoaddress = cursor.getString(cursor.getColumnIndex("geoaddress"));
                                    up_uploaded = cursor.getString(cursor.getColumnIndex("uploaded"));
                                    uploadData(up_farmer_name, up_farmer_mobile, up_crop, up_product,
                                            up_dosage, up_remark, up_photo, up_usercode, up_tr_date, up_cordinate,
                                            up_geoaddress, up_uploaded);
                                } while (cursor.moveToNext());
                            } else {
                                Toast.makeText(farmer_Complaint.this, "Data Will Upload When Internet Available", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }

            }
        });

    }

    protected boolean validation() {
        boolean flag = true;

        if (txtDose.getText().length() == 0) {

            msclass.showMessage("Please Enter txtDose ");
            txtDose.setError("Required");
            return false;
        }
        if (txtRemark.getText().length() == 0) {

            msclass.showMessage("Please Enter txtRemark ");
            txtRemark.setError("Required");
            return false;
        }
        if (spn_Crop.getSelectedItem().toString().equals("Select Crop")) {
            msclass.showMessage("Please Select Crop");
            return false;
        }
        if (spn_product.getSelectedItem().toString().equals("Select Product")) {
            msclass.showMessage("Please Select Product");
            return false;
        }
        if (!hasImage(ivImage)) {
            msclass.showMessage("Please Capture Image");
            return false;
        }
        return true;
    }

    private boolean hasImage(@NonNull ImageView view) {
        Drawable drawable = view.getDrawable();
        boolean hasImage = (drawable != null);
        if (hasImage && (drawable instanceof BitmapDrawable)) {
            hasImage = ((BitmapDrawable) drawable).getBitmap() != null;
        }
        return hasImage;
    }


    protected void loadspinnerdata() {
        List<String> spnCrop_list = new ArrayList<String>();
        spnCrop_list.add("Select Crop");
        for (int i = 0; i < mCrop_list.size(); i++) {
            spnCrop_list.add(mCrop_list.get(i).getCrop_name());
        }
        spn_Crop.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, spnCrop_list));

        List<String> spnProduct_list = new ArrayList<String>();
        spnProduct_list.add("Select Product");
        for (int i = 0; i < mProduct_list.size(); i++) {
            spnProduct_list.add(mProduct_list.get(i).getProduct_name());
        }
        spn_product.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, spnProduct_list));

    }

    protected void Initializecontrol() {
        spn_Crop = (SearchableSpinner) findViewById(R.id.spn_Crop);
        spn_product = (SearchableSpinner) findViewById(R.id.spn_product);
        txtDose = (EditText) findViewById(R.id.txtDose);
        txtRemark = (EditText) findViewById(R.id.txtRemark);
        FarmerName = (TextView) findViewById(R.id.FarmerName);
        btn_add = (Button) findViewById(R.id.btn_add);
        btn_complaintPhoto = (Button) findViewById(R.id.btn_complaintPhoto);
        ivImage = (ImageView) findViewById(R.id.ivImage);
        msclass = new Messageclass(this);
        db = new DBCreation(this);
        getLocation();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        currentDateandTime = sdf.format(new Date());
        Intent i = getIntent();
        FarmerName.setText(i.getStringExtra("farmerName"));
        FarmerMobile = i.getStringExtra("mobile");
        visitcount = i.getStringExtra("visitcount");
    }

    private void captureImage2() {

        try {
            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

            if (imageselect == 1) {
                photoFile = createImageFile4();
                if (photoFile != null) {
                    Log.i("Mayank", photoFile.getAbsolutePath());
                    Uri photoURI = Uri.fromFile(photoFile);
                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(cameraIntent, REQUEST_CAMERA);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            msclass.showMessage(ex.getMessage());
        }
    }

    private void captureImage() {

        try {

            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions((Activity) this, new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
            } else {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(this.getPackageManager()) != null) {
                    // Create the File where the photo should go
                    try {
                        if (imageselect == 1) {
                            photoFile = createImageFile();

                            if (photoFile != null) {
                                Uri photoURI = FileProvider.getUriForFile(this,
                                        "com.growindigo.connectplus",
                                        photoFile);
                                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                                startActivityForResult(takePictureIntent, REQUEST_CAMERA);
                            }
                        }

                    } catch (Exception ex) {

                        msclass.showMessage(ex.toString());
                        ex.printStackTrace();
                    }


                } else {
                }
            }
        } catch (Exception ex) {
            msclass.showMessage(ex.getMessage());
            ex.printStackTrace();
            //dialog.dismiss();
        }
    }

    private File createImageFile4() //  throws IOException
    {
        File mediaFile = null;
        try {
            // External sdcard location
            File mediaStorageDir = new File(
                    Environment
                            .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                    IMAGE_DIRECTORY_NAME);
            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    return null;
                }
            }
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                    Locale.getDefault()).format(new Date());
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } catch (Exception ex) {
            ex.printStackTrace();
            msclass.showMessage(ex.getMessage());
        }
        return mediaFile;
    }

    private File createImageFile() {
        // Create an image file name
        File image = null;
        try {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = "JPEG_" + timeStamp + "_";
            File storageDir = this.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );

        } catch (Exception ex) {
            ex.printStackTrace();
            msclass.showMessage(ex.toString());
        }
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == SELECT_FILE)
                    onSelectFromGalleryResult(data);
                else if (requestCode == REQUEST_CAMERA)
                    onCaptureImageResult(data);
            }
        } catch (Exception e) {
            e.printStackTrace();
            msclass.showMessage(e.toString());
        }
    }

    private void onCaptureImageResult(Intent data) {


        try {
            if (imageselect == 1) {

                try {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 2;

                    Bitmap myBitmap = BitmapFactory.decodeFile(photoFile.getAbsolutePath(), options);
                    Imagepath1 = photoFile.getAbsolutePath();
                    ivImage.setImageBitmap(myBitmap);
                    ivImage.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    msclass.showMessage(e.toString());
                    e.printStackTrace();
                }
                //end
            }

        } catch (Exception e) {
            msclass.showMessage(e.toString());
            e.printStackTrace();
        }

    }

    private void onSelectFromGalleryResult(Intent data) {


        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(this.getApplicationContext().getContentResolver(), data.getData());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (imageselect == 1) {
            ivImage.setImageBitmap(bm);
            ivImage.setVisibility(View.VISIBLE);
        }

    }

    public void getLocation() {
        try {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
            }
            Criteria criteria = new Criteria();
            criteria.setVerticalAccuracy(Criteria.ACCURACY_HIGH);
            criteria.setPowerRequirement(Criteria.POWER_HIGH);
            criteria.setAltitudeRequired(false);
            criteria.setSpeedRequired(false);
            criteria.setCostAllowed(true);
            criteria.setBearingRequired(false);
            locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) this);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, (LocationListener) this);
            //locationManager.getLastKnownLocation(locationManager.GPS_PROVIDER);
            setUpMapIfNeeded();
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    public void onLocationChanged(Location location) {
        longitude = location.getLongitude();
        latitude = location.getLatitude();
        cordinates = location.getLatitude() + "-" + location.getLongitude();
        getLocation();
    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, "Please Enable GPS", Toast.LENGTH_SHORT).show();
        final Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    private void setUpMapIfNeeded() {

        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        String result = null;
        try {
            List<Address> addressList = geocoder.getFromLocation(
                    latitude, longitude, 1);
            if (addressList != null && addressList.size() > 0) {
                Address address = addressList.get(0);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getAddressLine(i)).append("\n");//adress
                }
                geoAddress = address.getAddressLine(0).toString();
                result = sb.toString();
            }
        } catch (IOException e) {
            // Log.e(TAG, "Unable connect to Geocoder", e);
        }


    }

    public void uploadData(String up_farmer_name,
                           String up_farmer_mobile,
                           String up_crop,
                           String up_product, String up_dosage, String up_remark,
                           String up_photo, String up_usercode, String up_tr_date,
                           String up_cordinate, String up_geoaddress, String up_uploaded) {

        //String converdImage = Utils.encodeImage(up_photo);
        String converdImage = "";
        if (up_photo != null && !up_photo.isEmpty()) {
            converdImage = Utils.getImageDatadetail(up_photo);
        }
        Map<String, String> paramsReq = new HashMap<String, String>();
        paramsReq.put("farmer_name", up_farmer_name);
        paramsReq.put("farmer_mobile", up_farmer_mobile);
        paramsReq.put("crop", up_crop);
        paramsReq.put("product", up_product);
        paramsReq.put("dosage", up_dosage);
        paramsReq.put("remark", up_remark);
        paramsReq.put("image", converdImage);
        paramsReq.put("cordianate", up_cordinate);
        paramsReq.put("geoaddress", up_geoaddress);
        paramsReq.put("usercode", up_usercode);
        paramsReq.put("tr_date", up_tr_date);
        paramsReq.put("uploaded", up_uploaded);
        db.updateFarmerCompalint(up_farmer_mobile, "1");
        apiRequest(paramsReq, Constants.UplaodFarmerComplaintData, Request.Method.POST);
    }

    private void apiRequest(Map<String, String> paramsReq, String tag, int method) {
        ApiRequest apiRequest = new ApiRequest(this, this);
        apiRequest.postJSONRequest(Constants.BASE_URL + Constants.UplaodFarmerComplaintData, tag,
                paramsReq, method);
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        //Log.d("upload:: ", response);

       // Toast.makeText(farmer_Complaint.this, "Data Uploaded Successfully", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        //Log.d("Error::", "error:: " + error);
    }
    @Override
    public void onResume() {
        super.onResume();
        String userCode = Preferences.get(farmer_Complaint.this, Constants.PREF_USERCODE);
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
        Utils.fetchCurrentLocation(userCode, String.valueOf(latitude), String.valueOf(longitude), this.getClass().getSimpleName(), formattedDate + " " + currentTime, this, this);
    }
}
