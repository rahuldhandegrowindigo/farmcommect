package com.growindigo.connectplus.databaseutils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.growindigo.connectplus.ReportVisitModel;
import com.growindigo.connectplus.model.CropModel;
import com.growindigo.connectplus.model.DistributorModel;
import com.growindigo.connectplus.model.DistrictModel;
import com.growindigo.connectplus.model.ProductModel;
import com.growindigo.connectplus.model.RetailerModel;
import com.growindigo.connectplus.model.StateModel;
import com.growindigo.connectplus.model.TalukaModel;
import com.growindigo.connectplus.model.listModel;

import java.util.ArrayList;

public class DBCreation extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "connectplus.db";


    public DBCreation(Context context) {
        super(context, DATABASE_NAME, null, 29);
        // SQLiteDatabase.openOrCreateDatabase("/sdcard/"+DATABASE_NAME,null);
        //SQLiteDatabase db = getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CreateUserMasterTable = "CREATE TABLE  UserMaster (user_code TEXT,User_pwd TEXT,user_name TEXT,state Text,role Text,Territory_Name text,territory_Code text)";
        db.execSQL(CreateUserMasterTable);

        String CreatDistributorTable = "CREATE table DistributorTable(distributor Text,distributorCode text,dist_mobile text)";
        db.execSQL(CreatDistributorTable);

        String CreatRetailerTable = "CREATE table RetailerTable(retailer Text,retailerCode text,ret_mobile text)";
        db.execSQL(CreatRetailerTable);

        String newDist = "CREATE table newDist(dist_name Text,dist_mobile text,dist_email text,uploaded text)";
        db.execSQL(newDist);

        String newRet = "CREATE table newRet(ret_name Text,ret_mobile text,ret_email text,uploaded text)";
        db.execSQL(newRet);

        String CreatCropTable = "CREATE table CropMaster(cropName Text)";
        db.execSQL(CreatCropTable);

        String CreatProductTable = "CREATE table ProductTable(product Text, product_code TEXT, mrp TEXT, dist_price TEXT,ret_price TEXT)";
        db.execSQL(CreatProductTable);

        String CreatFarmerTable = "CREATE table FarmerMaster(Mobile text,FarmerName Text,State Text,Dist text,taluka text,village text,pincode text,cordinate text,geoaddress text,area text,sourceofIrrigation text,distributor text,img text,visitno int,usercode text,tr_date text,uploaded text)";
        db.execSQL(CreatFarmerTable);

        String CreatFirstVisitTable = "CREATE table FarmerVisit(FarmerName Text,cordinate text,geoaddress text,Mobile text,visitPurpose text,crop text,area text,period text,cropStage text,sowingDate text,observation text,prododuct text,dosage text,visitno text,img text ,usercode text,tr_date text,uploaded text,purchaseProduct text,Remark text)";
        db.execSQL(CreatFirstVisitTable);

        String CreatColleactionTable = "CREATE table dist_ColleactionTable(dist_name Text,dist_code text,paymentType text,payment_mode text,detail text,usercode text,tr_date text,cordinate text,geoaddress text,uploaded text,Amount text)";
        db.execSQL(CreatColleactionTable);

        String CreatSalesTable = "CREATE table dist_SalesTable(dist_name Text,dist_code text,mat_name text,mat_code text,unit text,rate text,total text,usercode text,tr_date text,cordinate text,geoaddress text,uploaded text)";
        db.execSQL(CreatSalesTable);

        String CreatComplaintTable = "CREATE table dist_ComplaintTable(dist_name Text,dist_code text,complaintType text,detail text,photo text,usercode text,tr_date text,cordinate text,geoaddress text,uploaded text)";
        db.execSQL(CreatComplaintTable);

        String CreatFarmerComplaintTable = "CREATE table Farmer_ComplaintTable(farmer_name Text,farmer_mobile text,crop text,product text,dosage text,remark text,photo text,usercode text,tr_date text,cordinate text,geoaddress text,uploaded text)";
        db.execSQL(CreatFarmerComplaintTable);

        String CreatFarmerMeetTable = "CREATE table Farmer_MeetTable(villageName Text,farmercount text,usercode text,tr_date text,cordinate text,geoaddress text,uploaded text,remark text)";
        db.execSQL(CreatFarmerMeetTable);

        String CreatFaremerDemo = "CREATE table FaremerDemo(FarmerName Text,cordinate text,geoaddress text,Mobile text,crop text,area text,cropStage text,sowingDate text,prododuct text,dosage text,img text ,usercode text,tr_date text,uploaded text,Remark text,Satisfaction text)";
        db.execSQL(CreatFaremerDemo);

        String CreatDist_appoint = "CREATE table Dist_appoint(user_type text,firmName text,PropriterName text,Village text,Taluka text,District text,State text,Pincode text,Mobile text,NearestClient text,remark text,Intrested  text,mahycoClient  text,photo text,usercode text,tr_date text,cordinate text,geolocation text,uploaded text)";
        db.execSQL(CreatDist_appoint);

        String CreateTable_Campaign = "Create table Campaign (Campaign_type text,dist_name text,dist_code text,ret_name text,ret_code text,village text,FromDate text,ToDate text,budget text,crop text,usercode text,tr_date text,uploaded text)";

        db.execSQL(CreateTable_Campaign);

        String CreateDistAssignTable = "Create table distAssing(dist_code text,dist_name text,ret_code text,ret_name text,uploaded text)";
        db.execSQL(CreateDistAssignTable);

        String createStateTable="Create table stateMaster(stat_code text,stat_desc text,dist_code text,dist_desc text,talq_code text,talq_desc text)";
        db.execSQL(createStateTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS UserMaster");
        db.execSQL("DROP TABLE IF EXISTS DistributorTable");
        db.execSQL("DROP TABLE IF EXISTS CropMaster");
        db.execSQL("DROP TABLE IF EXISTS ProductTable");
        db.execSQL("DROP TABLE IF EXISTS FarmerMaster");
        db.execSQL("DROP TABLE IF EXISTS FarmerVisit");

        db.execSQL("DROP TABLE IF EXISTS dist_ColleactionTable");
        db.execSQL("DROP TABLE IF EXISTS dist_SalesTable");
        db.execSQL("DROP TABLE IF EXISTS dist_ComplaintTable");
        db.execSQL("DROP TABLE IF EXISTS Farmer_ComplaintTable");
        db.execSQL("DROP TABLE IF EXISTS Farmer_MeetTable");
        db.execSQL("DROP TABLE IF EXISTS FaremerDemo");
        db.execSQL("DROP TABLE IF EXISTS Dist_appoint");

        db.execSQL("DROP TABLE IF EXISTS RetailerTable");
        db.execSQL("DROP TABLE IF EXISTS newDist");
        db.execSQL("DROP TABLE IF EXISTS distAssing");
        db.execSQL("DROP TABLE IF EXISTS newRet");

        db.execSQL("DROP TABLE IF EXISTS Campaign");
        db.execSQL("DROP TABLE IF EXISTS stateMaster");
        onCreate(db);
    }


    public boolean insertCampaign(String Campaign_type, String dist_name, String dist_code, String ret_name, String ret_code,
                                  String village, String FromDate, String ToDate, String budget, String crop,
                                  String usercode, String tr_date) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("Campaign_type", Campaign_type);
        contentValues.put("dist_name", dist_name);
        contentValues.put("dist_code", dist_code);
        contentValues.put("ret_name", ret_name);
        contentValues.put("ret_code", ret_code);
        contentValues.put("village", village);
        contentValues.put("FromDate", FromDate);
        contentValues.put("ToDate", ToDate);
        contentValues.put("budget", budget);
        contentValues.put("crop", crop);
        contentValues.put("usercode", usercode);
        contentValues.put("tr_date", tr_date);
        contentValues.put("uploaded", "0");
        long result = db.insert("Campaign", null, contentValues);
        if (result == -1)
            return false;
        else
            return true;
    }

    public boolean insertNewDist(String dist_name, String dist_mobile, String dist_email) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("dist_name", dist_name);
        contentValues.put("dist_mobile", dist_mobile);
        contentValues.put("dist_email", dist_email);
        contentValues.put("uploaded", "0");
        long result = db.insert("newDist", null, contentValues);
        if (result == -1)
            return false;
        else
            return true;
    }

    public boolean insertDistAssignWithCode(String jsonCode, String jsonName, String ret_code, String ret_name) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("dist_code", jsonCode);
        contentValues.put("dist_name", jsonName);
        contentValues.put("ret_code", ret_code);
        contentValues.put("ret_name", ret_name);
        contentValues.put("uploaded", "0");
        long result = db.insert("distAssing", null, contentValues);
        if (result == -1)
            return false;
        else
            return true;
    }
    public boolean DownloadDistAssignWithCode(String jsonCode, String jsonName, String ret_code, String ret_name) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("dist_code", jsonCode);
        contentValues.put("dist_name", jsonName);
        contentValues.put("ret_code", ret_code);
        contentValues.put("ret_name", ret_name);
        contentValues.put("uploaded", "1");
        long result = db.insert("distAssing", null, contentValues);
        if (result == -1)
            return false;
        else
            return true;
    }
    public boolean insertNewRet(String ret_name, String ret_mobile, String ret_email) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("ret_name", ret_name);
        contentValues.put("ret_mobile", ret_mobile);
        contentValues.put("ret_email", ret_email);
        contentValues.put("uploaded", "0");
        long result = db.insert("newRet", null, contentValues);
        if (result == -1)
            return false;
        else
            return true;
    }

    // Registration Data
    public boolean insertUserRegistrationData(String username, String usercode, String userpwd, String userrole, String Territory_Name, String territory_Code) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("user_name", username);
        contentValues.put("user_code", usercode);
        contentValues.put("User_pwd", userpwd);
        contentValues.put("role", userrole);
        contentValues.put("Territory_Name", Territory_Name);
        contentValues.put("territory_Code", territory_Code);

        long result = db.insert("UserMaster", null, contentValues);
        if (result == -1)
            return false;
        else
            return true;
    }

//Check user existance

    public boolean checkUser(String userName, String password) {

        // array of columns to fetch
        String[] columns = {"user_code", "User_pwd"};
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        String selection = "user_code" + " = ?" + " AND " + "User_pwd" + " = ?";

        // selection arguments
        String[] selectionArgs = {userName.toUpperCase().trim(), password.trim()};

        // query user table with conditions
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
         */
        Cursor cursor = db.query("UserMaster", //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        int cursorCount = cursor.getCount();

        cursor.close();
        db.close();
        if (cursorCount > 0) {
            return true;
        }

        return false;
    }


    public boolean InsertDist_appointment(String user_type, String firmName, String PropriterName, String Village, String Taluka,
                                          String District, String State, String Pincode, String Mobile,
                                          String NearestClient, String remark, String Intrested, String mahycoClient,
                                          String photo, String usercode, String tr_date, String cordinate,
                                          String geolocation, String uploaded) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("user_type", user_type);
        contentValues.put("firmName", firmName);
        contentValues.put("PropriterName", PropriterName);
        contentValues.put("Village", Village);
        contentValues.put("Taluka", Taluka);
        contentValues.put("District", District);
        contentValues.put("State", State);
        contentValues.put("Pincode", Pincode);
        contentValues.put("Mobile", Mobile);
        contentValues.put("NearestClient", NearestClient);
        contentValues.put("remark", remark);
        contentValues.put("Intrested", Intrested);
        contentValues.put("mahycoClient", mahycoClient);
        contentValues.put("photo", photo);
        contentValues.put("usercode", usercode);
        contentValues.put("tr_date", tr_date);
        contentValues.put("cordinate", cordinate);
        contentValues.put("geolocation", geolocation);
        contentValues.put("uploaded", uploaded);
        long result = db.insert("Dist_appoint", null, contentValues);//secoundVisitTable
        if (result == -1)
            return false;
        else
            return true;
    }


    public boolean InsertFarmerData(String FarmerName, String State, String Dist, String taluka, String village, String pincode, String cordinate, String geoaddress, String Mobile, String area, String sourceofIrrigation, String distributor, String img, String usercode, String tr_date, String upload) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("FarmerName", FarmerName);
        contentValues.put("State", State);
        contentValues.put("Dist", Dist);
        contentValues.put("taluka", taluka);
        contentValues.put("village", village);
        contentValues.put("pincode", pincode);
        contentValues.put("cordinate", cordinate);
        contentValues.put("geoaddress", geoaddress);
        contentValues.put("Mobile", Mobile);
        contentValues.put("area", area);
        contentValues.put("sourceofIrrigation", sourceofIrrigation);
        contentValues.put("distributor", distributor);
        contentValues.put("img", img);
        contentValues.put("visitno", 0);
        contentValues.put("usercode", usercode);
        contentValues.put("tr_date", tr_date);
        contentValues.put("uploaded", upload);
        long result = db.insert("FarmerMaster", null, contentValues);
        if (result == -1)
            return false;
        else
            return true;
    }

    //Distributor Data
    public boolean insertDistributorData(String distributor, String distributorCode, String dist_mobile) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("distributor", distributor);
        contentValues.put("distributorCode", distributorCode);
        contentValues.put("dist_mobile", dist_mobile);
        long result = db.insert("DistributorTable", null, contentValues);
        if (result == -1)
            return false;
        else
            return true;
    }
    //Retailer Data

    public void deleledata(String TABLE_NAME) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("delete from  " + TABLE_NAME + "");
    }

    public boolean insertRetailerData(String retailer, String retailerCode, String ret_mobile) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("retailer", retailer);
        contentValues.put("retailerCode", retailerCode);
        contentValues.put("ret_mobile", ret_mobile);
        long result = db.insert("RetailerTable", null, contentValues);
        if (result == -1)
            return false;
        else
            return true;
    }

    public boolean insertStateData(String stat_code,String stat_desc,String dist_code,String dist_desc,String talq_code,String talq_desc) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("stat_code", stat_code);
        contentValues.put("stat_desc", stat_desc);
        contentValues.put("dist_code", dist_code);
        contentValues.put("dist_desc", dist_desc);
        contentValues.put("talq_code", talq_code);
        contentValues.put("talq_desc", talq_desc);
        long result = db.insert("stateMaster", null, contentValues);
        if (result == -1)
            return false;
        else
            return true;
    }
    //Crop Data
    public boolean insertCropData(String cropName) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("cropName", cropName);
        long result = db.insert("CropMaster", null, contentValues);
        if (result == -1)
            return false;
        else
            return true;
    }

    //Product Data
    public boolean insertProductData(String product_name, String product_code, String mrp, String dist_price, String ret_price) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("product", product_name);
        contentValues.put("product_code", product_code);
        contentValues.put("mrp", mrp);
        contentValues.put("dist_price", dist_price);
        contentValues.put("ret_price", ret_price);
        long result = db.insert("ProductTable", null, contentValues);
        if (result == -1)
            return false;
        else
            return true;
    }

    public Boolean InsertFirstVisitData(String FarmerName, String cordinate, String geoaddress, String Mobile, String visitPurpose,
                                        String crop, String area, String period, String cropStage, String sowingDate,
                                        String observation, String prododuct, String dosage, int visitno, String img,
                                        String usercode, String tr_date, String purchaseProduct, String Remark) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("FarmerName", FarmerName);
        contentValues.put("cordinate", cordinate);
        contentValues.put("geoaddress", geoaddress);
        contentValues.put("Mobile", Mobile);
        contentValues.put("visitPurpose", visitPurpose);
        contentValues.put("crop", crop);
        contentValues.put("area", area);
        contentValues.put("period", period);
        contentValues.put("cropStage", cropStage);
        contentValues.put("sowingDate", sowingDate);
        contentValues.put("observation", observation);
        contentValues.put("prododuct", prododuct);
        contentValues.put("dosage", dosage);
        contentValues.put("visitno", visitno);
        contentValues.put("img", img);
        contentValues.put("usercode", usercode);
        contentValues.put("tr_date", tr_date);
        contentValues.put("uploaded", "0");
        contentValues.put("purchaseProduct", purchaseProduct);
        contentValues.put("Remark", Remark);
        long result = db.insert("FarmerVisit", null, contentValues);//secoundVisitTable
        if (result == -1)
            return false;
        else
            return true;
    }


    public Boolean InsertDemoVisitData(String FarmerName, String cordinate, String geoaddress, String Mobile,
                                       String crop, String area, String cropStage, String sowingDate,
                                       String prododuct, String dosage, String img,
                                       String usercode, String tr_date, String Remark, String Satisfaction) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("FarmerName", FarmerName);
        contentValues.put("cordinate", cordinate);
        contentValues.put("geoaddress", geoaddress);
        contentValues.put("Mobile", Mobile);
        contentValues.put("crop", crop);
        contentValues.put("area", area);
        contentValues.put("cropStage", cropStage);
        contentValues.put("sowingDate", sowingDate);
        contentValues.put("prododuct", prododuct);
        contentValues.put("dosage", dosage);
        contentValues.put("img", img);
        contentValues.put("usercode", usercode);
        contentValues.put("tr_date", tr_date);
        contentValues.put("uploaded", "0");
        contentValues.put("Remark", Remark);
        contentValues.put("Satisfaction", Satisfaction);
        long result = db.insert("FaremerDemo", null, contentValues);//secoundVisitTable
        if (result == -1)
            return false;
        else
            return true;
    }

    public Boolean dist_complaint_insert(String dist_name, String dist_code, String complaintType, String detail,
                                         String photo, String usercode, String tr_date,
                                         String cordinate, String geoaddress) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("dist_name", dist_name);
        contentValues.put("dist_code", dist_code);
        contentValues.put("complaintType", complaintType);
        contentValues.put("detail", detail);
        contentValues.put("photo", photo);
        contentValues.put("usercode", usercode);
        contentValues.put("tr_date", tr_date);
        contentValues.put("cordinate", cordinate);
        contentValues.put("geoaddress", geoaddress);
        contentValues.put("uploaded", "0");
        long result = db.insert("dist_ComplaintTable", null, contentValues);
        if (result == -1)
            return false;
        else
            return true;
    }

    public Boolean dist_collection_insert(String dist_name, String dist_code, String paymentType, String payment_mode,
                                          String detail, String usercode, String tr_date, String cordinate,
                                          String geoaddress, String Amount) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("dist_name", dist_name);
        contentValues.put("dist_code", dist_code);
        contentValues.put("paymentType", paymentType);
        contentValues.put("detail", detail);
        contentValues.put("payment_mode", payment_mode);
        contentValues.put("usercode", usercode);
        contentValues.put("tr_date", tr_date);
        contentValues.put("cordinate", cordinate);
        contentValues.put("geoaddress", geoaddress);
        contentValues.put("uploaded", "0");
        contentValues.put("Amount", Amount);
        long result = db.insert("dist_ColleactionTable", null, contentValues);
        if (result == -1)
            return false;
        else
            return true;
    }

    public Boolean farmer_complaint_insert(String farmer_name, String farmer_mobile, String crop, String product,
                                           String dosage, String remark, String photo, String usercode,
                                           String tr_date, String cordinate, String geoaddress) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("farmer_name", farmer_name);
        contentValues.put("farmer_mobile", farmer_mobile);
        contentValues.put("crop", crop);
        contentValues.put("product", product);
        contentValues.put("dosage", dosage);
        contentValues.put("remark", remark);
        contentValues.put("photo", photo);
        contentValues.put("usercode", usercode);
        contentValues.put("tr_date", tr_date);
        contentValues.put("cordinate", cordinate);
        contentValues.put("geoaddress", geoaddress);
        contentValues.put("uploaded", "0");
        long result = db.insert("Farmer_ComplaintTable", null, contentValues);
        if (result == -1)
            return false;
        else
            return true;
    }

    public Boolean farmer_meet_insert(String villageName, String farmercount, String usercode, String tr_date,
                                      String cordinate, String geoaddress, String uploaded, String remark) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("villageName", villageName);
        contentValues.put("farmercount", farmercount);
        contentValues.put("usercode", usercode);
        contentValues.put("tr_date", tr_date);
        contentValues.put("cordinate", cordinate);
        contentValues.put("geoaddress", geoaddress);
        contentValues.put("uploaded", uploaded);
        contentValues.put("remark", remark);
        long result = db.insert("Farmer_MeetTable", null, contentValues);
        if (result == -1)
            return false;
        else
            return true;
    }

    //    public Boolean InsertsecoundVisitData(String FarmerName ,String cordinate ,String geoaddress ,String Mobile ,String visitPurpose ,
//                                        String crop ,String area ,String period ,String cropStage ,String sowingDate ,
//                                        String observation ,String prododuct ,String dosage ,String visitno ,String img  ,
//                                        String usercode ,String tr_date,String purchaseProduct  )
//    {
//        SQLiteDatabase db = getWritableDatabase();
//        ContentValues contentValues = new ContentValues();
//        contentValues.put("FarmerName", FarmerName);
//        contentValues.put("cordinate", cordinate);
//        contentValues.put("geoaddress", geoaddress);
//        contentValues.put("Mobile", Mobile);
//        contentValues.put("visitPurpose", visitPurpose);
//        contentValues.put("crop", crop);
//        contentValues.put("area", area);
//        contentValues.put("period", period);
//        contentValues.put("cropStage", cropStage);
//        contentValues.put("sowingDate", sowingDate);
//        contentValues.put("observation", observation);
//        contentValues.put("prododuct", prododuct);
//        contentValues.put("dosage", dosage);
//        contentValues.put("visitno", visitno);
//        contentValues.put("img", img);
//        contentValues.put("usercode", usercode);
//        contentValues.put("tr_date", tr_date);
//        contentValues.put("upload", "0");
//        contentValues.put("purchaseProduct",purchaseProduct);
//        long result = db.insert("secoundVisitTable", null, contentValues);//secoundVisitTable
//        if (result == -1)
//            return false;
//        else
//            return true;
//    }
    public void updateVisit(int visitNo, String Mobile) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("Update FarmerMaster set visitno=" + visitNo + "  where Mobile=" + Mobile + " ");
    }



    public void updateDistributorCode(String code, String Mobile) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("Update DistributorTable set distributorCode=" + code + "  where dist_mobile=" + Mobile + " ");
    }
    public void updateRetailerCode(String code, String Mobile) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("Update RetailerTable set retailerCode=" + code + "  where ret_mobile=" + Mobile + " ");
    }
    public void updateFarmerVisit(String visitNo, String Mobile, String uploaded) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("Update FarmerVisit set uploaded=" + uploaded + "  where Mobile=" + Mobile + " AND " + "visitno=" + visitNo + " ");
    }

    public void updateCampaign(String tr_date, String uploaded) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("Update Campaign set uploaded=" + uploaded + "  where tr_date==" + "'" + tr_date + "'");
    }

    public void updateFarmerMaster(String Mobile, String uploaded) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("Update FarmerMaster set uploaded=" + uploaded + "  where Mobile=" + Mobile + " ");
    }

    public void updateDist_appoint(String Mobile, String uploaded) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("Update Dist_appoint set uploaded=" + uploaded + "  where Mobile=" + Mobile + " ");
    }

    public void updateFarmerCompalint(String Mobile, String uploaded) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("Update Farmer_ComplaintTable set uploaded=" + uploaded + "  where farmer_mobile=" + Mobile + " ");
    }

    public void updateFarmerMeet(String uploaded) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("Update Farmer_MeetTable set uploaded=" + uploaded + "");
    }

    public void updateNewDist(String Mobile, String uploaded) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("Update newDist set uploaded=" + uploaded + " where dist_mobile=" + Mobile + " ");
    }

    public void updateNewDistAssign(String uploaded) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("Update distAssing set uploaded=" + uploaded);
    }

    public void updateNewRet(String Mobile, String uploaded) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("Update newRet set uploaded=" + uploaded + " where ret_mobile=" + Mobile + " ");
    }

    public void updateFarmerDemo(String Mobile, String uploaded) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("Update FaremerDemo set uploaded=" + uploaded + "  where Mobile=" + Mobile + " ");
    }

    public void updatedistCollection(String Mobile, String uploaded) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("Update dist_ColleactionTable set uploaded=" + uploaded + "  where dist_code=" + "'" + Mobile + "'");
    }

    public void updatedistComplaint(String Mobile, String uploaded) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("Update dist_ComplaintTable set uploaded=" + uploaded + "  where dist_code=" + "'" + Mobile + "'");
    }

    public Cursor userinfo(String Mobile) {
        SQLiteDatabase db = getWritableDatabase();
        Cursor mCursor = db.rawQuery("SELECT  FarmerName,Mobile,crop,cropStage,observation,prododuct,sowingDate,area,dosage,purchaseProduct,Remark FROM FarmerVisit where Mobile='" + Mobile + "' ORDER BY visitno DESC LIMIT 1", null);
        return mCursor;
    }
    public Cursor DistAssing(String ret_code) {
        SQLiteDatabase db = getWritableDatabase();
        Cursor mCursor = db.rawQuery("select * from distAssing where ret_code=" + "'" + ret_code + "'", null);
        return mCursor;
    }
    public Cursor ProductPrice(String prd_code) {
        SQLiteDatabase db = getWritableDatabase();
        Cursor mCursor = db.rawQuery("SELECT dist_price,ret_price FROM ProductTable where product_code='" + prd_code + "'", null);
        return mCursor;
    }

    public Cursor Dist_data(String Mobile) {
        SQLiteDatabase db = getWritableDatabase();
        String selectQuery = "SELECT  dist_mobile FROM DistributorTable where dist_mobile like '%" + Mobile + "%'";
        Cursor mCursor = db.rawQuery(selectQuery, null);
        return mCursor;
    }

    public Cursor Ret_data(String Mobile) {
        SQLiteDatabase db = getWritableDatabase();
        String selectQuery = "SELECT  ret_mobile FROM RetailerTable where ret_mobile like '%" + Mobile + "%'";
        Cursor mCursor = db.rawQuery(selectQuery, null);
        return mCursor;
    }

    public ArrayList<listModel> getFarmerlistData() {
        ArrayList<listModel> list;
        list = new ArrayList<listModel>();
        String selectQuery = "select FarmerName,Dist,taluka,village,Mobile,visitno from FarmerMaster";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                //JSONObject jobj = new JSONObject();
                listModel reportModel = new listModel();
                try {

                    reportModel.setFarmername(cursor.getString(0));
                    reportModel.setMobile(cursor.getString(4));
                    reportModel.setDistrict(cursor.getString(1));
                    reportModel.setTaluka(cursor.getString(2));
                    reportModel.setVillage(cursor.getString(3));
                    reportModel.setVisitDetails(cursor.getString(5));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                list.add(reportModel);
            } while (cursor.moveToNext());
        }
        return list;
    }

    public ArrayList<listModel> SearchFarmerlistData(String mobileNo) {
        ArrayList<listModel> list;
        list = new ArrayList<listModel>();
        String selectQuery = "select FarmerName,Dist,taluka,village,Mobile,visitno from FarmerMaster where Mobile like '%" + mobileNo + "%'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                //JSONObject jobj = new JSONObject();
                listModel reportModel = new listModel();
                try {

                    reportModel.setFarmername(cursor.getString(0));
                    reportModel.setDistrict(cursor.getString(1));
                    reportModel.setTaluka(cursor.getString(2));
                    reportModel.setVillage(cursor.getString(3));
                    reportModel.setMobile(cursor.getString(4));
                    reportModel.setVisitDetails(cursor.getString(5));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                list.add(reportModel);
            } while (cursor.moveToNext());
        }
        return list;
    }

    //State List

    public ArrayList<StateModel> getStateListData() {
        ArrayList<StateModel> list;
        list = new ArrayList<StateModel>();
        String selectQuery = "select distinct stat_desc, stat_code from stateMaster order by stat_code";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                //JSONObject jobj = new JSONObject();
                StateModel reportModel = new StateModel();
                //reportModel.setStateCode("0");
               // reportModel.setStateName("Select State");
                try {
                    reportModel.setStateName(cursor.getString(0));
                    reportModel.setStateCode(cursor.getString(1));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                list.add(reportModel);
            } while (cursor.moveToNext());
        }
        return list;
    }
    //District List

    public ArrayList<DistrictModel> getDistrictListData() {
        ArrayList<DistrictModel> list;
        list = new ArrayList<DistrictModel>();
        String selectQuery = "select distinct dist_desc, dist_code from stateMaster order by dist_code";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                //JSONObject jobj = new JSONObject();
                DistrictModel reportModel = new DistrictModel();
                reportModel.setDistrictCode("0");
                reportModel.setDistrictName("Select District");
                try {
                    reportModel.setDistrictName(cursor.getString(0));
                    reportModel.setDistrictCode(cursor.getString(1));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                list.add(reportModel);
            } while (cursor.moveToNext());
        }
        return list;
    }

    //Taluka List with district Code
    public ArrayList<TalukaModel> getTalukaListData(String DistictCode) {
        ArrayList<TalukaModel> list;
        list = new ArrayList<TalukaModel>();
        String selectQuery = "select distinct talq_desc, talq_code from stateMaster where dist_code='" + DistictCode.trim() + "' order by talq_code";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                //JSONObject jobj = new JSONObject();
                TalukaModel reportModel = new TalukaModel();
                reportModel.setTalukaCode("0");
                reportModel.setTalukaName("Select Taluka");
                try {
                    reportModel.setTalukaName(cursor.getString(0));
                    reportModel.setTalukaCode(cursor.getString(1));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                list.add(reportModel);
            } while (cursor.moveToNext());
        }
        return list;
    }

    //Taluka List
    public ArrayList<TalukaModel> getTalukaDetailsData() {
        ArrayList<TalukaModel> list;
        list = new ArrayList<TalukaModel>();
        String selectQuery = "select distinct talq_desc, talq_code from stateMaster order by talq_code";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                //JSONObject jobj = new JSONObject();
                TalukaModel reportModel = new TalukaModel();
                reportModel.setTalukaCode("0");
                reportModel.setTalukaName("Select Taluka");
                try {
                    reportModel.setTalukaName(cursor.getString(0));
                    reportModel.setTalukaCode(cursor.getString(1));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                list.add(reportModel);
            } while (cursor.moveToNext());
        }
        return list;
    }
    //Distributor List
    public ArrayList<DistributorModel> getDistributorListData() {
        ArrayList<DistributorModel> list;
        list = new ArrayList<DistributorModel>();
        String selectQuery = "select distributor, distributorCode from DistributorTable order by distributorCode";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                //JSONObject jobj = new JSONObject();
                DistributorModel reportModel = new DistributorModel();
                reportModel.setDistributorCode("0");
                reportModel.setDistributorName("Select Distributor");
                try {
                    reportModel.setDistributorName(cursor.getString(0));
                    reportModel.setDistributorCode(cursor.getString(1));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                list.add(reportModel);
            } while (cursor.moveToNext());
        }
        return list;
    }

    //Distributor List
    public ArrayList<RetailerModel> getRetailerListData() {
        ArrayList<RetailerModel> list;
        list = new ArrayList<RetailerModel>();//retailer Text,retailerCode text
        String selectQuery = "select retailer, retailerCode from RetailerTable order by retailerCode";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                //JSONObject jobj = new JSONObject();
                RetailerModel reportModel = new RetailerModel();
                reportModel.setRetailerCode("0");
                reportModel.setRetailerName("Select Retailer");
                try {
                    reportModel.setRetailerName(cursor.getString(0));
                    reportModel.setRetailerCode(cursor.getString(1));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                list.add(reportModel);
            } while (cursor.moveToNext());
        }
        return list;
    }
//// Retailer + distributor list with number search

    //Distributor List
    public ArrayList<DistributorModel> getDistributorListDataSearch(String Mobile) {
        ArrayList<DistributorModel> list;
        list = new ArrayList<DistributorModel>();
        String selectQuery = "select distributor, distributorCode from DistributorTable where dist_mobile ='" + Mobile.trim() + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                //JSONObject jobj = new JSONObject();
                DistributorModel reportModel = new DistributorModel();
                reportModel.setDistributorCode("0");
                reportModel.setDistributorName("Select Distributor");
                try {
                    reportModel.setDistributorName(cursor.getString(0));
                    reportModel.setDistributorCode(cursor.getString(1));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                list.add(reportModel);
            } while (cursor.moveToNext());
        }
        return list;
    }

    //Retailer List
    public ArrayList<RetailerModel> getRetailerListDataSearch(String Mobile) {
        ArrayList<RetailerModel> list;
        list = new ArrayList<RetailerModel>();//retailer Text,retailerCode text
        String selectQuery = "select retailer, retailerCode from RetailerTable where ret_mobile ='" + Mobile.trim() + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                //JSONObject jobj = new JSONObject();
                RetailerModel reportModel = new RetailerModel();
                reportModel.setRetailerCode("0");
                reportModel.setRetailerName("Select Retailer");
                try {
                    reportModel.setRetailerName(cursor.getString(0));
                    reportModel.setRetailerCode(cursor.getString(1));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                list.add(reportModel);
            } while (cursor.moveToNext());
        }
        return list;
    }

    ///// END
    //Crop List
    public ArrayList<CropModel> getCropListData() {
        ArrayList<CropModel> list;
        list = new ArrayList<CropModel>();
        String selectQuery = "select cropName from CropMaster";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                //JSONObject jobj = new JSONObject();
                CropModel reportModel = new CropModel();
                try {
                    reportModel.setCrop_name(cursor.getString(0));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                list.add(reportModel);
            } while (cursor.moveToNext());
        }
        return list;
    }

    //Product List
    public ArrayList<ProductModel> getProductListData() {
        ArrayList<ProductModel> list;
        list = new ArrayList<ProductModel>();
        String selectQuery = "select product,product_code from ProductTable order by product";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                //JSONObject jobj = new JSONObject();
                ProductModel reportModel = new ProductModel();
                reportModel.setProduct_name("Select Product");
                reportModel.setProduct_code("0");
                try {
                    reportModel.setProduct_name(cursor.getString(0));
                    reportModel.setProduct_code(cursor.getString(1));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                list.add(reportModel);
            } while (cursor.moveToNext());
        }
        return list;
    }

    public ArrayList<ReportVisitModel> getVisitData(String Mobile) {
        ArrayList<ReportVisitModel> list;
        list = new ArrayList<ReportVisitModel>();
        String selectQuery = "SELECT visitno,visitPurpose,period,cropStage,observation,prododuct,dosage,purchaseProduct,tr_date,Remark FROM FarmerVisit where Mobile='" + Mobile.trim() + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                //JSONObject jobj = new JSONObject();
                ReportVisitModel reportModel = new ReportVisitModel();
                try {
                    reportModel.setTxt_visitnoDetails(cursor.getString(0));
                    reportModel.setTxt_visitDetails(cursor.getString(1));
                    reportModel.setTxt_PeriodDetails(cursor.getString(2));
                    reportModel.setTxt_CropStageDetails(cursor.getString(3));
                    reportModel.setTxt_ObservationDetails(cursor.getString(4));
                    reportModel.setTxt_ProductDetails(cursor.getString(5));
                    reportModel.setTxt_DosageDetails(cursor.getString(6));
                    reportModel.setTxt_PuchaseDetails(cursor.getString(7));
                    reportModel.setTxt_VisitDateDetails(cursor.getString(8));
                    reportModel.setTxt_RemarkDetails(cursor.getString(9));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                list.add(reportModel);
            } while (cursor.moveToNext());
        }
        return list;
    }

    public ArrayList<Cursor> getData(String Query) {
        //get writable database
        SQLiteDatabase sqlDB = getWritableDatabase();
        String[] columns = new String[]{"mesage"};
        //an array list of cursor to save two cursors one has results from the query
        //other cursor stores error message if any errors are triggered
        ArrayList<Cursor> alc = new ArrayList<Cursor>(2);
        MatrixCursor Cursor2 = new MatrixCursor(columns);
        alc.add(null);
        alc.add(null);

        try {
            String maxQuery = Query;
            //execute the query results will be save in Cursor c
            Cursor c = sqlDB.rawQuery(maxQuery, null);


            //add value to cursor2
            Cursor2.addRow(new Object[]{"Success"});

            alc.set(1, Cursor2);
            if (null != c && c.getCount() > 0) {


                alc.set(0, c);
                c.moveToFirst();

                return alc;
            }
            return alc;
        } catch (SQLException sqlEx) {
            Log.d("printing exception", sqlEx.getMessage());
            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[]{"" + sqlEx.getMessage()});
            alc.set(1, Cursor2);
            return alc;
        } catch (Exception ex) {

            Log.d("printing exception", ex.getMessage());

            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[]{"" + ex.getMessage()});
            alc.set(1, Cursor2);
            return alc;
        }
    }
}
