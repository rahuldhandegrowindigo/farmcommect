package com.growindigo.connectplus.databaseutils;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.growindigo.connectplus.Farmer_Demo;
import com.growindigo.connectplus.R;
import com.growindigo.connectplus.farmer_Complaint;
import com.growindigo.connectplus.farmer_meet;
import com.growindigo.connectplus.farmer_visit_2;
import com.growindigo.connectplus.model.listModel;

import java.util.ArrayList;
import java.util.List;

public class listadaptor extends RecyclerView.Adapter<listadaptor.DataObjectHolder> {
    private List<listModel> objects = new ArrayList<listModel>();
    private Context context;
    private LayoutInflater layoutInflater;
    //Messageclass ms;
    int imageResourceId = 0;
    String visitnodetails = "";
    FragmentManager fragmentManager;
    String getSelectedFarmerVisit;

    public listadaptor(String getSelectedFarmerVisit, List<listModel> getlist, Context context, FragmentManager fragmentManager) {
        this.context = context;
        this.objects = getlist;
        this.layoutInflater = LayoutInflater.from(context);
        this.fragmentManager = fragmentManager;
        this.getSelectedFarmerVisit = getSelectedFarmerVisit;
    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listview, parent, false);

        return new listadaptor.DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final DataObjectHolder holder, final int position) {
        holder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.white1));

        holder.txt_fnamedetails.setText(objects.get(position).getFarmername());
        holder.txt_MobileDetails.setText(objects.get(position).getMobile());
        holder.txt_DistrictDetails.setText(objects.get(position).getDistrict());
        holder.txt_TalukaDetails.setText(objects.get(position).getTaluka());
        holder.txt_VillageDetails.setText(objects.get(position).getVillage());
        holder.txt_VisitDetails.setText(objects.get(position).getVisitDetails());

        visitnodetails = holder.txt_VisitDetails.getText().toString();
        holder.rel_top.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                visitnodetails = holder.txt_VisitDetails.getText().toString();

                if (getSelectedFarmerVisit.equalsIgnoreCase("complaint")) {
                    Intent i = new Intent(context, farmer_Complaint.class);
                    i.putExtra("farmerName", objects.get(position).getFarmername());
                     i.putExtra("mobile", objects.get(position).getMobile());
                     i.putExtra("visitcount", objects.get(position).getVisitDetails());
                    context.startActivity(i);
                } else if (getSelectedFarmerVisit.equalsIgnoreCase("Farmer Meet")) {
                    Intent i = new Intent(context, farmer_meet.class);
                    i.putExtra("farmerName", objects.get(position).getFarmername());
                     i.putExtra("mobile", objects.get(position).getMobile());
                     i.putExtra("visitcount", objects.get(position).getVisitDetails());
                    context.startActivity(i);
                } else if (getSelectedFarmerVisit.equalsIgnoreCase("Field Visit")){
                    Intent i = new Intent(context, farmer_visit_2.class);
                    i.putExtra("farmerName", objects.get(position).getFarmername());
                    i.putExtra("mobile", objects.get(position).getMobile());
                    i.putExtra("visitcount", objects.get(position).getVisitDetails());
                    context.startActivity(i);
                }
                else if (getSelectedFarmerVisit.equalsIgnoreCase("Demonstration"))
                {
                    Intent i = new Intent(context, Farmer_Demo.class);
                    i.putExtra("farmerName", objects.get(position).getFarmername());
                    i.putExtra("mobile", objects.get(position).getMobile());
                    i.putExtra("visitcount", objects.get(position).getVisitDetails());
                    i.putExtra("DemoFollow", "Demo");
                    context.startActivity(i);
                }
                else if (getSelectedFarmerVisit.equalsIgnoreCase("Demo Follow up Result Check"))
                {
                    Intent i = new Intent(context, Farmer_Demo.class);
                    i.putExtra("farmerName", objects.get(position).getFarmername());
                    i.putExtra("mobile", objects.get(position).getMobile());
                    i.putExtra("visitcount", objects.get(position).getVisitDetails());
                    i.putExtra("DemoFollow", "Democheck");
                    context.startActivity(i);
                }
                else
                {
                    Toast.makeText(context,"Select Visit Type", Toast.LENGTH_SHORT).show();
                }

//                }
//                else if(visitnodetails.equals("1")){
//                    Intent i = new Intent(context, farmer_visit_2.class);
//                    i.putExtra("farmerName", objects.get(position).getFarmername());
//                    i.putExtra("mobile",objects.get(position).getMobile());
//                    context.startActivity(i);
//                }
//                else if(visitnodetails.equals("2")){
//                    Toast.makeText(context,"Visits Completed", Toast.LENGTH_SHORT).show();
//                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return objects.size();
    }


    public class DataObjectHolder extends RecyclerView.ViewHolder {
        private LinearLayout rel_top;
        private RelativeLayout rel_booked_by;
        private CardView cardView;
        private LinearLayout linOrders;
        private TextView txt_fnamedetails;
        private TextView txt_MobileDetails;
        private TextView txt_DistrictDetails;
        private TextView txt_TalukaDetails;
        private TextView txt_VillageDetails;
        private TextView txt_VisitDetails;
        private Button btnPLD;
        private TextView noorder;
        private RecyclerView recycler;
        ImageView arrow;

        public DataObjectHolder(View view) {
            super(view);
            rel_top = (LinearLayout) view.findViewById(R.id.rel_top);
            cardView = (CardView) view.findViewById(R.id.card_view);
            txt_fnamedetails = (TextView) view.findViewById(R.id.txt_fnamedetails);
            txt_MobileDetails = (TextView) view.findViewById(R.id.txt_MobileDetails);
            txt_DistrictDetails = (TextView) view.findViewById(R.id.txt_DistrictDetails);
            txt_TalukaDetails = (TextView) view.findViewById(R.id.txt_TalukaDetails);
            txt_VillageDetails = (TextView) view.findViewById(R.id.txt_VillageDetails);
            txt_VisitDetails = (TextView) view.findViewById(R.id.txt_VisitDetails);

        }
    }
}
