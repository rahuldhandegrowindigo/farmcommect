package com.growindigo.connectplus;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.growindigo.connectplus.databaseutils.DBCreation;
import com.growindigo.connectplus.databaseutils.Messageclass;
import com.growindigo.connectplus.model.CropModel;
import com.growindigo.connectplus.model.DistributorModel;
import com.growindigo.connectplus.utils.CheckConnection;
import com.growindigo.connectplus.utils.Constants;
import com.growindigo.connectplus.utils.Preferences;
import com.growindigo.connectplus.utils.Utils;
import com.growindigo.connectplus.volley.ApiRequest;
import com.growindigo.connectplus.volley.IApiResponse;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class farmer_meet extends AppCompatActivity implements LocationListener, IApiResponse {
    private EditText txtVillageName, txtNoFarmer, txtRemark;
    SearchableSpinner spn_Distributor;
    Button btn_add;

    private double longitude;
    private double latitude;
    Messageclass msclass;
    String cordinates = "";
    String currentDateandTime = "";
    int imageselect;
    File photoFile = null;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    ImageView ivImage;
    EditText txtpalce;
    private static final String IMAGE_DIRECTORY_NAME = "VISITPHOTO";
    private String Imagepath1;
    DBCreation db;
    String geoAddress = "";
    LocationManager locationManager;
    String up_villageName,
            up_farmercount,
            up_usercode,
            up_tr_date,
            up_cordinate,
            up_geoaddress,
            up_uploaded,
            up_remark;

    ArrayList<DistributorModel> mDistributor_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farmer_meet);
        getSupportActionBar().setTitle("Farmer Meet");
        txtVillageName = (EditText) findViewById(R.id.txtVillageName);
        txtNoFarmer = (EditText) findViewById(R.id.txtNoFarmer);
        txtRemark = (EditText) findViewById(R.id.txtRemark);
        spn_Distributor = (SearchableSpinner) findViewById(R.id.spn_Distributor);
        btn_add = (Button) findViewById(R.id.btn_add);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        currentDateandTime = sdf.format(new Date());
        getLocation();
        db = new DBCreation(this);
        mDistributor_list = db.getDistributorListData();
        //Log.d("DistSize: ", mDistributor_list.size() + "");
        List<String> spnDisributor_list = new ArrayList<String>();
        spnDisributor_list.add("Select Distributor");
        for (int i = 0; i < mDistributor_list.size(); i++) {
            spnDisributor_list.add(mDistributor_list.get(i).getDistributorName());
        }
        spn_Distributor.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, spnDisributor_list));

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validation() == true) {
                    Map<String, String> paramsReq = new HashMap<String, String>();
                    String userCode = Preferences.get(farmer_meet.this, Constants.PREF_USERCODE);
                    paramsReq.put("usercode", userCode);
                    boolean result = db.farmer_meet_insert(txtVillageName.getText().toString(), txtNoFarmer.getText().toString(),
                            userCode,
                            currentDateandTime, cordinates, geoAddress, "0", txtRemark.getText().toString());
                    if (result) {
                        Utils.showAlertDialogOk("SUCCESS", "Data Save Successfully", farmer_meet.this);

                    } else {
                        Utils.showAlertDialogOk("Error", "Something Went Wrong", farmer_meet.this);
                    }
                    if (CheckConnection.isNetworkAvailable(farmer_meet.this)) {
                        String searchQuery = "select  *  from Farmer_MeetTable where  uploaded ='0'";
                        Cursor cursor = db.getReadableDatabase().rawQuery(searchQuery, null);
                        int count = cursor.getCount();
                        if (count > 0) {
                            if (cursor.moveToFirst()) {
                                do {

                                    up_villageName = cursor.getString(cursor.getColumnIndex("villageName"));
                                    up_farmercount = cursor.getString(cursor.getColumnIndex("farmercount"));
                                    up_usercode = cursor.getString(cursor.getColumnIndex("usercode"));
                                    up_tr_date = cursor.getString(cursor.getColumnIndex("tr_date"));
                                    up_cordinate = cursor.getString(cursor.getColumnIndex("cordinate"));
                                    up_geoaddress = cursor.getString(cursor.getColumnIndex("geoaddress"));
                                    up_uploaded = cursor.getString(cursor.getColumnIndex("uploaded"));
                                    up_remark= cursor.getString(cursor.getColumnIndex("remark"));
                                    uploadData(up_villageName, up_farmercount, up_usercode, up_tr_date,
                                            up_cordinate, up_geoaddress, up_uploaded,up_remark);
                                }
                                while (cursor.moveToNext());
                            }
                        } else {
                            Toast.makeText(farmer_meet.this, "Data Will Upload When Internet Available", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });
    }

    protected boolean validation() {
        boolean flag = true;

        if (txtVillageName.getText().length() == 0) {

            msclass.showMessage("Please Enter Village Name ");
            txtVillageName.setError("Required");
            return false;
        }
        if (txtNoFarmer.getText().length() == 0) {

            msclass.showMessage("Please Enter Number of Farmer ");
            txtNoFarmer.setError("Required");
            return false;
        }
        if (spn_Distributor.getSelectedItem().toString().equals("Select Distributor")) {
            msclass.showMessage("Please Select Distributor");
            return false;
        }
        if (txtRemark.getText().length() == 0) {

            msclass.showMessage("Please Enter Message ");
            txtRemark.setError("Required");
            return false;
        }

        return true;
    }

    public void getLocation() {
        try {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
            }
            Criteria criteria = new Criteria();
            criteria.setVerticalAccuracy(Criteria.ACCURACY_HIGH);
            criteria.setPowerRequirement(Criteria.POWER_HIGH);
            criteria.setAltitudeRequired(false);
            criteria.setSpeedRequired(false);
            criteria.setCostAllowed(true);
            criteria.setBearingRequired(false);
            locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) this);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, (LocationListener) this);
            //locationManager.getLastKnownLocation(locationManager.GPS_PROVIDER);
            setUpMapIfNeeded();
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    public void onLocationChanged(Location location) {
        longitude = location.getLongitude();
        latitude = location.getLatitude();
        cordinates = location.getLatitude() + "-" + location.getLongitude();
        getLocation();
    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, "Please Enable GPS", Toast.LENGTH_SHORT).show();
        final Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    private void setUpMapIfNeeded() {

        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        String result = null;
        try {
            List<Address> addressList = geocoder.getFromLocation(
                    latitude, longitude, 1);
            if (addressList != null && addressList.size() > 0) {
                Address address = addressList.get(0);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getAddressLine(i)).append("\n");//adress
                }
                geoAddress = address.getAddressLine(0).toString();
                result = sb.toString();
            }
        } catch (IOException e) {
            // Log.e(TAG, "Unable connect to Geocoder", e);
        }


    }

    public void uploadData(String up_villageName, String up_farmercount,
                           String up_usercode, String up_tr_date,
                           String up_cordinate, String up_geoaddress,
                           String up_uploaded,String up_remark) {

        Map<String, String> paramsReq = new HashMap<String, String>();
        paramsReq.put("villageName", up_villageName);
        paramsReq.put("farmercount", up_farmercount);
        paramsReq.put("remark", up_remark);
        paramsReq.put("usercode", up_usercode);
        paramsReq.put("tr_date", up_tr_date);
        if (up_cordinate != null && up_cordinate.length() > 0)
            paramsReq.put("cordinate", up_cordinate);
        else
            paramsReq.put("cordinate", "0");
        if (up_geoaddress != null && up_geoaddress.length() > 0)
            paramsReq.put("geoaddress", up_geoaddress);
        else
            paramsReq.put("geoaddress", "0");
        paramsReq.put("uploaded", up_uploaded);
        db.updateFarmerMeet("1");
        apiRequest(paramsReq, Constants.UplaodFarmerMeetData, Request.Method.POST);
    }

    private void apiRequest(Map<String, String> paramsReq, String tag, int method) {
        ApiRequest apiRequest = new ApiRequest(this, this);
        apiRequest.postJSONRequest(Constants.BASE_URL + Constants.UplaodFarmerMeetData, tag,
                paramsReq, method);
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        //Log.d("upload:: ", response);
      //  Toast.makeText(farmer_meet.this, "Data Uploaded Successfully", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }
    @Override
    public void onResume() {
        super.onResume();
        String userCode = Preferences.get(farmer_meet.this, Constants.PREF_USERCODE);
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
        Utils.fetchCurrentLocation(userCode, String.valueOf(latitude), String.valueOf(longitude), this.getClass().getSimpleName(), formattedDate + " " + currentTime, this, this);
    }
}
