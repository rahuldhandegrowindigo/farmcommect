package com.growindigo.connectplus;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.growindigo.connectplus.databaseutils.DBCreation;
import com.growindigo.connectplus.databaseutils.listadaptor;
import com.growindigo.connectplus.model.listModel;
import com.growindigo.connectplus.utils.Utils;

import java.util.ArrayList;

public class usercheck extends AppCompatActivity {
    EditText txtMobileNumber;
    TextView txtmsg, txtOR;
    Button btn_next, btn_OR;
    DBCreation db;
    String ReferanceFrom = "";
    String avialbel = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usercheck);
        txtMobileNumber = (EditText) findViewById(R.id.txtMobileNumber);
        txtmsg = (TextView) findViewById(R.id.txtmsg);
        txtOR = (TextView) findViewById(R.id.txtOR);
        btn_next = (Button) findViewById(R.id.btn_next);
        btn_OR = (Button) findViewById(R.id.btn_OR);
        db = new DBCreation(this);
        txtMobileNumber.setText("");
        Intent i = getIntent();
        ReferanceFrom = i.getStringExtra("ref_From");

        if (ReferanceFrom.equals("DistributorTab")) {
            getSupportActionBar().setTitle("Distributor Visit");

        } else if (ReferanceFrom.equals("RetailerTab")) {
            getSupportActionBar().setTitle("Retailer Visit");
        } else if (ReferanceFrom.equals("FaremerTab")) {
            getSupportActionBar().setTitle("Farmer Visit");
            btn_OR.setVisibility(View.INVISIBLE);
            txtOR.setVisibility(View.INVISIBLE);
        }

        txtMobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (txtMobileNumber.getText().length() == 10) {
                    if (ReferanceFrom.equals("FaremerTab")) {
                        ArrayList<listModel> mlist = db.SearchFarmerlistData(txtMobileNumber.getText().toString());
                        if (mlist.size() <= 0) {
                            txtmsg.setText("Farmer not available ! \nClick on go to add new farmer");
                            avialbel = "No";
                            //Utils.showAlertDialog("Warning","Farmer Not Available!\nKindly Add New Farmer!",usercheck.this);
                        } else {
                            txtmsg.setText("Farmer available ! \nClick on go to complete activity");
                            avialbel = "Yes";
                        }
                    } else if (ReferanceFrom.equals("DistributorTab")) {
                        Cursor data = db.Dist_data(txtMobileNumber.getText().toString());
                        if (data.getCount() == 0) {
                            txtmsg.setText("Distributor not available ! \nClick on go to add new distributor");
                            avialbel = "No";
                        } else {
                            txtmsg.setText("Distributor available ! \nClick on go to complete activity");
                            avialbel = "Yes";
                        }
                    } else if (ReferanceFrom.equals("RetailerTab")) {
                        Cursor data = db.Ret_data(txtMobileNumber.getText().toString());
                        if (data.getCount() == 0) {

                            txtmsg.setText("Retailer not available ! \nClick on go to add new retailer");
                            avialbel = "No";
                        } else {
                            txtmsg.setText("Retailer available ! \nClick on go to complete activity");
                            avialbel = "Yes";
                        }
                    }
                }
            }
        });

        btn_OR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ReferanceFrom.equals("DistributorTab")) {
                    Intent i = new Intent(usercheck.this, distributor.class);
                    i.putExtra("ref_From", "DistributorTab");
                    i.putExtra("mobile", "NA");
                    startActivity(i);
                } else if (ReferanceFrom.equals("RetailerTab")) {
                    Intent i = new Intent(usercheck.this, distributor.class);
                    i.putExtra("ref_From", "RetailerTab");
                    i.putExtra("mobile", "NA");
                    // i.putExtra("dist_code",distcode);
                    startActivity(i);
                }
            }
        });

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtMobileNumber.setText("");
                txtmsg.setText("");
                if (avialbel.equals("Yes") && ReferanceFrom.equals("FaremerTab")) {
                    Intent i = new Intent(usercheck.this, farmer.class);
                    i.putExtra("ref_From", "Yes");
                    // i.putExtra("dist_code",distcode);
                    startActivity(i);
                } else if (avialbel.equals("No") && ReferanceFrom.equals("FaremerTab")) {
                    Intent i = new Intent(usercheck.this, farmer.class);
                    i.putExtra("ref_From", "No");
                    // i.putExtra("dist_code",distcode);
                    startActivity(i);
                } else if (avialbel.equals("Yes") && ReferanceFrom.equals("DistributorTab")) {
                    Intent i = new Intent(usercheck.this, distributor.class);
                    i.putExtra("ref_From", "DistributorTab");
                    i.putExtra("mobile", txtMobileNumber.getText().toString());
                    startActivity(i);
                } else if (avialbel.equals("No") && ReferanceFrom.equals("DistributorTab")) {
                    Intent i = new Intent(usercheck.this, dist_appointment.class);
                    i.putExtra("ReferanceFrom", ReferanceFrom);
                    i.putExtra("mobile", txtMobileNumber.getText().toString());
                    startActivity(i);
                } else if (avialbel.equals("Yes") && ReferanceFrom.equals("RetailerTab")) {
                    Intent i = new Intent(usercheck.this, distributor.class);
                    i.putExtra("ref_From", "RetailerTab");
                    i.putExtra("mobile", txtMobileNumber.getText().toString());
                    // i.putExtra("dist_code",distcode);
                    startActivity(i);
                } else if (avialbel.equals("No") && ReferanceFrom.equals("RetailerTab")) {
                    Intent i = new Intent(usercheck.this, dist_appointment.class);
                    i.putExtra("ReferanceFrom", ReferanceFrom);
                    i.putExtra("mobile", txtMobileNumber.getText().toString());
                    startActivity(i);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        txtMobileNumber.setText("");
    }
}