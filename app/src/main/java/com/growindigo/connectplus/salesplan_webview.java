package com.growindigo.connectplus;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.growindigo.connectplus.utils.CheckConnection;
import com.growindigo.connectplus.utils.Constants;
import com.growindigo.connectplus.utils.Preferences;
import com.growindigo.connectplus.utils.Utils;
import com.growindigo.connectplus.volley.IApiResponse;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class salesplan_webview extends AppCompatActivity implements LocationListener, IApiResponse {
    WebView webview;
    private double longitude;
    private double latitude;
    String cordinates = "";
    String geoAddress = "";
    LocationManager locationManager;
    ProgressBar progressBar;
    String TAG = "salesPlan";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salesplan_webview);
        webview = (WebView) findViewById(R.id.webview);
        progressBar = findViewById(R.id.progressBar);
        WebSettings settings = webview.getSettings();
        // settings.setJavaScriptEnabled(true);
        String userCode = Preferences.get(salesplan_webview.this, Constants.PREF_USERCODE);
        //Log.d(TAG, " usercode: " + userCode);

        startWebView("https://connect.growindigo.co.in/SalesPlanning.aspx?usercode=" + userCode);
        // startWebView("https://www.google.co.in");

    }

    private void startWebView(String url) {

        //Create new webview Client to show progress dialog
        //When opening a url or click on link

        webview.setWebViewClient(new WebViewClient() {
            ProgressDialog progressDialog;

            //If you will not use this method url links are opeen in new brower not in webview
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            //Show loader on url load
            public void onLoadResource(WebView view, String url) {
                if (progressDialog == null) {
                    // in standard case YourActivity.this
                    progressDialog = new ProgressDialog(salesplan_webview.this);
                    progressDialog.setMessage("Loading...");
                    progressDialog.show();
                }
            }

            public void onPageFinished(WebView view, String url) {
                try {
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                        // progressDialog = null;
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
        });

        // Javascript inabled on webview
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setLoadWithOverviewMode(true);
        webview.getSettings().setUseWideViewPort(true);
        webview.loadUrl(url);

    }

    public void callURL() {
        String userCode = Preferences.get(salesplan_webview.this, Constants.PREF_USERCODE);
        //Log.d(TAG, " usercode: " + userCode);
        webview.loadUrl("https://connect.growindigo.co.in/SalesPlanning.aspx?usercode=" + userCode);
    }

    public void getLocation() {
        try {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
            }
            Criteria criteria = new Criteria();
            criteria.setVerticalAccuracy(Criteria.ACCURACY_HIGH);
            criteria.setPowerRequirement(Criteria.POWER_HIGH);
            criteria.setAltitudeRequired(false);
            criteria.setSpeedRequired(false);
            criteria.setCostAllowed(true);
            criteria.setBearingRequired(false);
            locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) this);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, (LocationListener) this);
            locationManager.getLastKnownLocation(locationManager.GPS_PROVIDER);
            setUpMapIfNeeded();
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    protected void checkInternet() {
        if (CheckConnection.isNetworkAvailable(this)) {
            //String userCode = Preferences.get(salesplan_webview.this, Constants.PREF_USERCODE);
            // webview.loadUrl("https://connect.growindigo.co.in/SalesPlanning.aspx?usercode="+userCode);
            // callURL();
        } else {
            Utils.showAlertDialogOk("Warning", "Internet Connection Not Available!", this);
        }
    }

    private void setUpMapIfNeeded() {

        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        String result = null;
        try {
            List<Address> addressList = geocoder.getFromLocation(
                    latitude, longitude, 1);
            if (addressList != null && addressList.size() > 0) {
                Address address = addressList.get(0);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getAddressLine(i)).append("\n");//adress
                }
                geoAddress = address.getAddressLine(0).toString();
                result = sb.toString();
            }
        } catch (IOException e) {
            // Log.e(TAG, "Unable connect to Geocoder", e);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        longitude = location.getLongitude();
        latitude = location.getLatitude();
        cordinates = location.getLatitude() + "-" + location.getLongitude();
        //Log.d("coordinates::", " :: " + cordinates);
        getLocation();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, "Please Enable GPS", Toast.LENGTH_SHORT).show();
        final Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        //Log.d("activityTracking::", response);
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResume() {
        super.onResume();
        String userCode = Preferences.get(salesplan_webview.this, Constants.PREF_USERCODE);
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
        getLocation();
        Utils.fetchCurrentLocation(userCode, String.valueOf(latitude), String.valueOf(longitude), this.getClass().getSimpleName(), formattedDate + " " + currentTime, this, this);
    }
}