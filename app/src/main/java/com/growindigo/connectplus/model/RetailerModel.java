package com.growindigo.connectplus.model;

public class RetailerModel {
    public RetailerModel() {
    }


    private String RetailerName;
    private String RetailerCode;

    public String getRetailerName() {
        return RetailerName;
    }

    public void setRetailerName(String RetailerName) {
        this.RetailerName = RetailerName;
    }

    public String getRetailerCode() {
        return RetailerCode;
    }

    public void setRetailerCode(String RetailerCode) {
        this.RetailerCode = RetailerCode;
    }
}