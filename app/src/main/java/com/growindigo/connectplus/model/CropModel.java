package com.growindigo.connectplus.model;

public class CropModel {
    public CropModel() {

    }

    private String crop_name;

    public String getCrop_name() {
        return crop_name;
    }

    public void setCrop_name(String crop_name) {
        this.crop_name = crop_name;
    }
}
