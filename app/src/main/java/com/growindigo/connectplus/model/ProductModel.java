package com.growindigo.connectplus.model;

public class ProductModel {
    public ProductModel() {

    }

    private String product_name;
    private String product_code;
    private String mrp;
    private String dist_price;

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_code() {
        return product_code;
    }

    public void setProduct_code(String product_code) {
        this.product_code = product_code;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getDist_price() {
        return dist_price;
    }

    public void setDist_price(String dist_price) {
        this.dist_price = dist_price;
    }
}
