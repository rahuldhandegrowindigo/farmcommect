package com.growindigo.connectplus.model;

public class DistributorModel {
    public DistributorModel() {

    }

    private String distributorName;
    private String distributorCode;

    public String getDistributorName() {
        return distributorName;
    }

    public void setDistributorName(String distributorName) {
        this.distributorName = distributorName;
    }

    public String getDistributorCode() {
        return distributorCode;
    }

    public void setDistributorCode(String distributorCode) {
        this.distributorCode = distributorCode;
    }
}
