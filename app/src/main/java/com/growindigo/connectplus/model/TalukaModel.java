package com.growindigo.connectplus.model;

public class TalukaModel {
    private String TalukaName;

    public String getTalukaName() {
        return TalukaName;
    }

    public void setTalukaName(String talukaName) {
        TalukaName = talukaName;
    }

    public String getTalukaCode() {
        return TalukaCode;
    }

    public void setTalukaCode(String talukaCode) {
        TalukaCode = talukaCode;
    }

    private String TalukaCode;
}
