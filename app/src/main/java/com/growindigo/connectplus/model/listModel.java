package com.growindigo.connectplus.model;

public class listModel {
    public listModel() {

    }

    private String Farmername;
    private String Mobile;
    private String district;

    public String getVisitDetails() {
        return VisitDetails;
    }

    public void setVisitDetails(String visitDetails) {
        VisitDetails = visitDetails;
    }

    private String VisitDetails;

    public String getFarmername() {
        return Farmername;
    }

    public void setFarmername(String farmername) {
        Farmername = farmername;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getTaluka() {
        return taluka;
    }

    public void setTaluka(String taluka) {
        this.taluka = taluka;
    }

    public String getVillage() {
        return Village;
    }

    public void setVillage(String village) {
        Village = village;
    }

    private String taluka;
    private String Village;

}
