package com.growindigo.connectplus;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.growindigo.connectplus.databaseutils.DBCreation;
import com.growindigo.connectplus.utils.Constants;
import com.growindigo.connectplus.utils.Preferences;
import com.growindigo.connectplus.utils.Utils;
import com.growindigo.connectplus.volley.ApiRequest;
import com.growindigo.connectplus.volley.IApiResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class register extends AppCompatActivity implements IApiResponse {

    EditText username, password;
    Button btnRegister;
    private DBCreation mDatabase;
    String token="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getSupportActionBar().setTitle("Register");
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        btnRegister = (Button) findViewById(R.id.btnRegister);
        mDatabase = new DBCreation(this);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit();
            }
        });
    }

    public void submit() {

        btnRegister.setText("Please Wait...");
        FCMToken();
        String userNameEntered = username.getText().toString().trim();
        String passwordEntered = password.getText().toString().trim();
        if (userNameEntered.equals("")) {
            username.requestFocus();
            username.setError(getString(R.string.required));
        } else if (passwordEntered.equals("")) {
            password.requestFocus();
            password.setError(getString(R.string.required));
        } else {
            Map<String, String> paramsReq = new HashMap<String, String>();
            //  paramsReq.put("Type", Constants.VerifyUser);
            paramsReq.put("username", userNameEntered);
            paramsReq.put("password", passwordEntered);
            paramsReq.put("FCMToken",token);

            apiRequest(paramsReq, Constants.VerifyUser, Request.Method.POST);

        }
    }

    private void apiRequest(Map<String, String> paramsReq, String tag, int method) {
        ApiRequest apiRequest = new ApiRequest(this, this);
        //  apiRequest.postRequest(Constants.BASE_URL, tag, paramsReq, Request.Method.GET);
        apiRequest.postJSONRequest(Constants.BASE_URL + Constants.VerifyUser, tag,
                paramsReq, method);
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        //Log.d("verifyesponse: ", response);
        try {
            if (tag_json_obj.equals(Constants.VerifyUser)) {
                JSONObject jsonObj = new JSONObject(response);
                if (response.contains("Table")) {
                    JSONArray jsonArray = jsonObj.getJSONArray("Table");
                    // Extract the Place descriptions from the results
                    String responseUsername = "", responseUsercode = "", responseUser_pwd = "",
                            responseUser_role = "",resposnseTerritory_Name="",resposnseterritory_Code="";
                    for (int i = 0; i < jsonArray.length(); i++) {
                        responseUsername = jsonArray.getJSONObject(i).getString("username");
                        responseUsercode = jsonArray.getJSONObject(i).getString("usercode");
                        responseUser_pwd = jsonArray.getJSONObject(i).getString("user_pwd");
                        responseUser_role = jsonArray.getJSONObject(i).getString("user_role");
                        resposnseTerritory_Name= jsonArray.getJSONObject(i).getString("Territory_Name");
                        resposnseterritory_Code= jsonArray.getJSONObject(i).getString("territory_Code");
                        Preferences.save(this, Constants.PREF_USERNAME, responseUsername);
                        Preferences.save(this, Constants.PREF_USERCODE, responseUsercode);
                        Preferences.save(this, Constants.PREF_USERPWD, responseUser_pwd);
                        Preferences.save(this, Constants.PREF_USERROLE, responseUser_role);
                        Preferences.save(this, Constants.PREF_Territory_Name, resposnseTerritory_Name);
                        Preferences.save(this, Constants.PREF_Territory_code, resposnseterritory_Code);
                        mDatabase.insertUserRegistrationData(responseUsername, responseUsercode, responseUser_pwd, responseUser_role,resposnseTerritory_Name,resposnseterritory_Code);
                    }
                    Utils.showAlertDialogOk(getString(R.string.success), getString(R.string.success_register), register.this);
                } else {
                    Utils.showAlertDialog(getString(R.string.error), getString(R.string.some_problem_occurred), register.this);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    protected  void FCMToken()
    {
        SharedPreferences prefs = getSharedPreferences("TOKEN_PREF", MODE_PRIVATE);
        token = prefs.getString("token", "");
        Log.e("NEW_INACTIVITY_TOKEN", token);
        if (TextUtils.isEmpty(token)) {
            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(register.this, new OnSuccessListener<InstanceIdResult>() {
                @Override
                public void onSuccess(InstanceIdResult instanceIdResult) {
                    String newToken = instanceIdResult.getToken();
                    Log.e("newToken", newToken);
                    SharedPreferences.Editor editor = getSharedPreferences("TOKEN_PREF", MODE_PRIVATE).edit();
                    if (token != null) {
                        editor.putString("token", newToken);
                        editor.apply();
                    }

                }
            });
        }
    }
}
