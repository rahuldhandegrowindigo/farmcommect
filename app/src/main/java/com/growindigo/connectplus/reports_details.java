package com.growindigo.connectplus;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

public class reports_details extends AppCompatActivity {
CardView card_view1,card_view2,card_view3,card_view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reports_details);
        getSupportActionBar().setTitle("My Report");
        card_view=(CardView) findViewById(R.id.card_view);
        card_view1=(CardView) findViewById(R.id.card_view1);
        card_view2=(CardView) findViewById(R.id.card_view2);
        card_view3=(CardView) findViewById(R.id.card_view3);



        Animation slide_down = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_down);
        card_view.setAnimation(slide_down);
        card_view1.setAnimation(slide_down);
        card_view2.setAnimation(slide_down);
        card_view3.setAnimation(slide_down);

        card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"Under Development", Toast.LENGTH_SHORT).show();
            }
        });


        card_view1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(reports_details.this, WebviewReports.class));
            }
        });

        card_view2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"Under Development", Toast.LENGTH_SHORT).show();
            }
        });

        card_view3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"Under Development", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
