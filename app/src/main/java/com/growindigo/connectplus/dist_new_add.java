package com.growindigo.connectplus;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.growindigo.connectplus.databaseutils.DBCreation;
import com.growindigo.connectplus.utils.CheckConnection;
import com.growindigo.connectplus.utils.Constants;
import com.growindigo.connectplus.utils.Preferences;
import com.growindigo.connectplus.utils.Utils;
import com.growindigo.connectplus.volley.ApiRequest;
import com.growindigo.connectplus.volley.IApiResponse;

import java.util.HashMap;
import java.util.Map;

public class dist_new_add extends AppCompatActivity implements IApiResponse {
    EditText txtnewDistName, txtnewDistMobile, txtnewDistEmail;
    Button btn_newSave;
    String up_ret_name, up_MobileNo, up_TerritoryCode, up_email_id, up_isFrom;
    DBCreation db;
    String ReferanceFrom = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dist_new_add);
        txtnewDistEmail = (EditText) findViewById(R.id.txtnewDistEmail);
        txtnewDistMobile = (EditText) findViewById(R.id.txtnewDistMobile);
        txtnewDistName = (EditText) findViewById(R.id.txtnewDistName);
        btn_newSave = (Button) findViewById(R.id.btn_newSave);
        db = new DBCreation(this);
        Intent i = getIntent();
        ReferanceFrom = i.getStringExtra("ReferanceFrom");

        btn_newSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ReferanceFrom.equals("DistributorTab")) {
                    SaveDist();
                } else if (ReferanceFrom.equals("RetailerTab")) {
                    SaveRet();
                }
            }
        });
    }

    protected void SaveDist() {
        boolean result = db.insertNewDist(txtnewDistName.getText().toString(), txtnewDistMobile.getText().toString(), txtnewDistEmail.getText().toString());
        if (result == true) {
            Utils.showAlertDialogOk("SUCCESS", "Data Save Successfully", this);
        } else {
            Utils.showAlertDialogOk("Error", "Something Went Wrong", this);
        }

        if (CheckConnection.isNetworkAvailable(this)) {
            String searchQuery = "select  *  from newDist where  uploaded ='0'";
            Cursor cursor = db.getReadableDatabase().rawQuery(searchQuery, null);
            int count = cursor.getCount();
            if (count > 0) {
                if (cursor.moveToFirst()) {
                    do {
                        up_ret_name = cursor.getString(cursor.getColumnIndex("up_ret_name"));
                        up_MobileNo = cursor.getString(cursor.getColumnIndex("up_MobileNo"));
                        up_email_id = cursor.getString(cursor.getColumnIndex("up_email_id"));
                        uploadData(up_ret_name, up_MobileNo, up_email_id, "DistributorTab");
                    }
                    while (cursor.moveToNext());
                }
            } else {
                Toast.makeText(this, "Data Will Upload When Internet Available", Toast.LENGTH_SHORT).show();
            }
        }
    }

    protected void SaveRet() {
        boolean result = db.insertNewRet(txtnewDistName.getText().toString(), txtnewDistMobile.getText().toString(), txtnewDistEmail.getText().toString());
        if (result = true) {
            Utils.showAlertDialogOk("SUCCESS", "Data Save Successfully", this);
        } else {
            Utils.showAlertDialogOk("Error", "Something Went Wrong", this);
        }

        if (CheckConnection.isNetworkAvailable(this)) {
            String searchQuery = "select  *  from newRet where  uploaded ='0'";
            Cursor cursor = db.getReadableDatabase().rawQuery(searchQuery, null);
            int count = cursor.getCount();
            if (count > 0) {
                if (cursor.moveToFirst()) {
                    do {

                        up_ret_name = cursor.getString(cursor.getColumnIndex("up_ret_name"));
                        up_MobileNo = cursor.getString(cursor.getColumnIndex("up_MobileNo"));
                        up_email_id = cursor.getString(cursor.getColumnIndex("up_email_id"));
                        uploadData(up_ret_name, up_MobileNo, up_email_id, "RetailerTab");
                    }
                    while (cursor.moveToNext());
                }
            } else {
                Toast.makeText(this, "Data Will Upload When Internet Available", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void uploadData(String up_ret_name, String up_MobileNo, String up_email_id, String up_isFrom) {

        Map<String, String> paramsReq = new HashMap<String, String>();
        String Ter_code = Preferences.get(this, Constants.PREF_Territory_code);
        paramsReq.put("ret_name", up_ret_name);
        paramsReq.put("MobileNo", up_MobileNo);
        paramsReq.put("email_id", up_email_id);
        paramsReq.put("isFrom", up_isFrom);
        paramsReq.put("TerritoryCode", Ter_code);
        if (up_isFrom.equals("DistributorTab")) {
            db.updateNewDist(up_MobileNo, "1");
        } else if (up_isFrom.equals("RetailerTab")) {
            db.updateNewRet(up_MobileNo, "1");
        }
        apiRequest(paramsReq, Constants.Ret_dist_upload, Request.Method.POST);
    }

    private void apiRequest(Map<String, String> paramsReq, String tag, int method) {
        ApiRequest apiRequest = new ApiRequest(this, this);
        apiRequest.postJSONRequest(Constants.BASE_URL + Constants.Ret_dist_upload, tag,
                paramsReq, method);
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {

    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

}
