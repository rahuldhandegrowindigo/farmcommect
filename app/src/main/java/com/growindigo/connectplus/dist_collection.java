package com.growindigo.connectplus;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.growindigo.connectplus.databaseutils.DBCreation;
import com.growindigo.connectplus.databaseutils.Messageclass;
import com.growindigo.connectplus.utils.Constants;
import com.growindigo.connectplus.utils.Preferences;
import com.growindigo.connectplus.utils.Utils;
import com.growindigo.connectplus.volley.ApiRequest;
import com.growindigo.connectplus.volley.IApiResponse;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class dist_collection extends AppCompatActivity implements LocationListener, IApiResponse {
    private SearchableSpinner spn_paymentType,spn_paymentMode;
    private EditText txtDetails,txtAmount;
    private Button btn_add;
    String currentDateandTime="";
    DBCreation db;
    String geoAddress="";
    LocationManager locationManager;
    private double longitude;
    private double latitude;
    Messageclass msclass;
    String cordinates="";
    private TextView DistributorName;
    String DistributorCode="";
    String up_dist_name,up_dist_code,up_paymentType,up_detail,up_payment_mode,up_usercode,up_tr_date,up_cordinate,up_geoaddress,up_Amount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dist_collection);
        getSupportActionBar().setTitle("Distributor Collection Visit");
        Intent i = getIntent();

        Initializecontrol();
        loadspinnerdata();

        DistributorName.setText(i.getStringExtra("dist_name"));
        DistributorCode =i.getStringExtra("dist_code");
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validation() == true) {
                    Map<String, String> paramsReq = new HashMap<String, String>();
                    String userCode = Preferences.get(dist_collection.this, Constants.PREF_USERCODE);
                    paramsReq.put("usercode", userCode);
                    boolean result =db.dist_collection_insert(DistributorName.getText().toString(),
                            DistributorCode,spn_paymentType.getSelectedItem().toString(),
                            spn_paymentMode.getSelectedItem().toString(),
                            txtDetails.getText().toString(),userCode,currentDateandTime,cordinates,geoAddress,
                            txtAmount.getText().toString());
                    if (result) {
                        Utils.showAlertDialogOk("SUCCESS", "Data Save Successfully", dist_collection.this);

                    } else {
                        Utils.showAlertDialogOk("Error", "Something Went Wrong", dist_collection.this);
                    }
                    /* Data Upload */
                    if (Utils.checkPermissionLocation(dist_collection.this)) {
                        String searchQuery = "select  *  from dist_ColleactionTable where  uploaded ='0'";
                        Cursor cursor = db.getReadableDatabase().rawQuery(searchQuery, null);
                        int count = cursor.getCount();
                        if (count > 0) {
                            if (cursor.moveToFirst()) {
                                do {
                                    up_dist_name = cursor.getString(cursor.getColumnIndex("dist_name"));
                                    up_dist_code = cursor.getString(cursor.getColumnIndex("dist_code"));
                                    up_paymentType = cursor.getString(cursor.getColumnIndex("paymentType"));
                                    up_detail = cursor.getString(cursor.getColumnIndex("detail"));
                                    up_payment_mode = cursor.getString(cursor.getColumnIndex("payment_mode"));
                                    up_usercode = cursor.getString(cursor.getColumnIndex("usercode"));
                                    up_tr_date = cursor.getString(cursor.getColumnIndex("tr_date"));
                                    up_cordinate = cursor.getString(cursor.getColumnIndex("cordinate"));
                                    up_geoaddress = cursor.getString(cursor.getColumnIndex("geoaddress"));
                                    up_Amount=cursor.getString(cursor.getColumnIndex("Amount"));
                                    uploadData(up_dist_name,up_dist_code,up_paymentType,up_detail,up_payment_mode,up_usercode,up_tr_date,up_cordinate,up_geoaddress,up_Amount);
                                } while (cursor.moveToNext());
                            } else {
                                Toast.makeText(dist_collection.this, "Data Will Upload When Internet Available", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            }
        });
    }

    protected void Initializecontrol() {
        spn_paymentType = (SearchableSpinner) findViewById(R.id.spn_paymentType);
        spn_paymentMode = (SearchableSpinner) findViewById(R.id.spn_paymentMode);
        DistributorName=(TextView)findViewById(R.id.DistributorName);
        txtDetails = (EditText) findViewById(R.id.txtDetails);
        txtAmount=(EditText)findViewById(R.id.txtAmount);
        btn_add=(Button)findViewById(R.id.btn_add);
        msclass = new Messageclass(this);
        db=new DBCreation(this);
        getLocation();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        currentDateandTime = sdf.format(new Date());

    }
    protected  boolean validation () {
        boolean flag = true;

        if (txtDetails.getText().length() == 0) {
            msclass.showMessage("Please Enter Details ");
            txtDetails.setError("Required");
            return false;
        }
        if(txtAmount.getText().length()==0)
        {
            msclass.showMessage("Enter Amount");
            txtAmount.setError("Required");
            return false;
        }
        if(spn_paymentType.getSelectedItem().toString().equals("Select Payment Type")){
            msclass.showMessage("Please Select Payment Type");
            return false;
        }
        if(spn_paymentMode.getSelectedItem().toString().equals("Select Payment Mode")){
            msclass.showMessage("Please Select Payment Mode");
            return false;
        }
        return true;
    }
    protected void loadspinnerdata()
    {
        List<String> spnDisributorVisitPurpose_list = new ArrayList<String>();
        spnDisributorVisitPurpose_list.add("Select Payment Type");
        spnDisributorVisitPurpose_list.add("Advance");
        spnDisributorVisitPurpose_list.add("Regular");
        spnDisributorVisitPurpose_list.add("Overdue");
        spnDisributorVisitPurpose_list.add("Interest");
        spn_paymentType.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, spnDisributorVisitPurpose_list));


        List<String> spnDisributor_list = new ArrayList<String>();
        spnDisributor_list.add("Select Payment Mode");
        spnDisributor_list.add("RTGS / NEFT / IMPS");
        spnDisributor_list.add("Cheque");
        spnDisributor_list.add("Cash");
        spn_paymentMode.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, spnDisributor_list));
    }

    public void getLocation() {
        try {
            locationManager = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);

            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) this);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, (LocationListener) this);
            //locationManager.getLastKnownLocation(locationManager.GPS_PROVIDER);
            setUpMapIfNeeded();
        }
        catch(SecurityException e) {
            e.printStackTrace();
        }
    }
    public void onLocationChanged(Location location) {
        longitude=location.getLongitude();
        latitude=location.getLatitude();
        cordinates=location.getLatitude() + "-" + location.getLongitude();
        getLocation();
    }
    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, "Please Enable GPS", Toast.LENGTH_SHORT).show();
        final Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
    }
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }
    @Override
    public void onProviderEnabled(String provider) {

    }
    private void setUpMapIfNeeded() {

        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        String result = null;
        try {
            List<Address> addressList = geocoder.getFromLocation(
                    latitude,longitude, 1);
            if (addressList != null && addressList.size() > 0) {
                Address address = addressList.get(0);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getAddressLine(i)).append("\n");//adress
                }
                geoAddress=address.getAddressLine(0).toString();
                result = sb.toString();
            }
        } catch (IOException e) {
            // Log.e(TAG, "Unable connect to Geocoder", e);
        }


    }


    public void uploadData(String up_dist_name,String up_dist_code,String up_paymentType,String up_detail,
                           String up_payment_mode,String up_usercode,String up_tr_date,
                           String up_cordinate,String up_geoaddress,String Amount ) {

        //String converdImage = Utils.encodeImage(up_photo);
        Map<String, String> paramsReq = new HashMap<String, String>();
        paramsReq.put("dist_name", up_dist_name);
        paramsReq.put("dist_code", up_dist_code);
        paramsReq.put("paymentType", up_paymentType);
        paramsReq.put("detail", up_detail);
        paramsReq.put("payment_mode", up_payment_mode);
        paramsReq.put("cordinate", up_cordinate);
        paramsReq.put("geoaddress", up_geoaddress);
        paramsReq.put("usercode", up_usercode);
        paramsReq.put("tr_date", up_tr_date);
        paramsReq.put("Amount",Amount);
        db.updatedistCollection(up_dist_code, "1");
        apiRequest(paramsReq, Constants.dist_collection_upload, Request.Method.POST);
    }

    private void apiRequest(Map<String, String> paramsReq, String tag, int method) {
        ApiRequest apiRequest = new ApiRequest(this, this);
        apiRequest.postJSONRequest(Constants.BASE_URL + Constants.dist_collection_upload, tag,
                paramsReq, method);
    }
    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        //Log.d("upload:: ", response);

       // Toast.makeText(dist_collection.this, "Data Uploaded Successfully", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        ///Log.d("Error::", "error:: " + error);
    }
    @Override
    public void onResume() {
        super.onResume();
        String userCode = Preferences.get(dist_collection.this, Constants.PREF_USERCODE);
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
        Utils.fetchCurrentLocation(userCode, String.valueOf(latitude), String.valueOf(longitude), this.getClass().getSimpleName(), formattedDate + " " + currentTime, this, this);
    }

}
