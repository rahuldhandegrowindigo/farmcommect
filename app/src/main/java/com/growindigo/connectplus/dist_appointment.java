package com.growindigo.connectplus;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.growindigo.connectplus.databaseutils.DBCreation;
import com.growindigo.connectplus.databaseutils.Messageclass;
import com.growindigo.connectplus.model.DistributorModel;
import com.growindigo.connectplus.model.DistrictModel;
import com.growindigo.connectplus.model.StateModel;
import com.growindigo.connectplus.model.TalukaModel;
import com.growindigo.connectplus.model.listModel;
import com.growindigo.connectplus.utils.CheckConnection;
import com.growindigo.connectplus.utils.Constants;
import com.growindigo.connectplus.utils.Preferences;
import com.growindigo.connectplus.utils.Utils;
import com.growindigo.connectplus.volley.ApiRequest;
import com.growindigo.connectplus.volley.IApiResponse;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.growindigo.connectplus.volley.AppController.getContext;

public class dist_appointment extends AppCompatActivity implements LocationListener, IApiResponse {

    EditText txtfirmName, txtPropriterName, txtVillage, txtPincode, txtMobile, txtNearestClient, txtremark;
    Switch switchIntrested, switchmahyco;
    Button btn_Photo, btn_farmerSave;
    ImageView ivImage;
    LocationManager locationManager;
    DBCreation db;
    private double longitude;
    private double latitude;
    String cordinates = "";
    String currentDateandTime = "";
    int imageselect;
    File photoFile = null;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    EditText txtpalce;
    private static final String IMAGE_DIRECTORY_NAME = "VISITPHOTO";
    private String Imagepath1, spnStateCode, spnDistrictCode, spnTalukaCode;
    String geoAddress = "";
    Messageclass msclass;
    String ReferanceFrom = "";
    String usertoAdd = "";
    ProgressDialog pDialog;
    String up_user_type, up_firmName, up_PropriterName, up_Village, up_Taluka, up_District, up_State, up_Pincode, up_Mobile, up_NearestClient,
            up_remark, up_Intrested, up_mahycoClient, up_photo, up_usercode, up_tr_date, up_cordinate, up_geolocation;

    SearchableSpinner spnState, spnDistrict, spnTaluka;
    ArrayList<StateModel> mState_list;
    ArrayList<StateModel> mState_Code;
    ArrayList<DistrictModel> mDistrict_list;
    ArrayList<DistrictModel> mDistrict_Code;
    ArrayList<TalukaModel> mTaluka_list;
    ArrayList<TalukaModel> mTaluka_Code;
    List<String> spnStateCodeList;
    List<String> spnDistrictCodeList;
    List<String> spntalukaCodeList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dist_appointment);

        Initializecontrol();

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
        }
        Criteria criteria = new Criteria();
        criteria.setVerticalAccuracy(Criteria.ACCURACY_HIGH);
        criteria.setPowerRequirement(Criteria.POWER_HIGH);
        criteria.setAltitudeRequired(false);
        criteria.setSpeedRequired(false);
        criteria.setCostAllowed(true);
        criteria.setBearingRequired(false);
        getLocation();

//API level 9 and up
        criteria.setHorizontalAccuracy(Criteria.ACCURACY_HIGH);
        criteria.setVerticalAccuracy(Criteria.ACCURACY_HIGH);

        spnState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //spnStateCode = mState_Code.get(position).getStateCode();
                spnStateCode = spnStateCodeList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spnDistrictCode = spnDistrictCodeList.get(position);
                mTaluka_list = db.getTalukaListData(spnDistrictCode);
                mTaluka_Code = db.getTalukaListData(spnDistrictCode);
                spntalukaCodeList = new ArrayList<String>();
                spntalukaCodeList.add("0");
                if (mTaluka_list != null) {
                    for (int i = 0; i < mTaluka_list.size(); i++) {
                        spntalukaCodeList.add(mTaluka_list.get(i).getTalukaCode());
                    }

                    List<String> spntalukaList = new ArrayList<String>();
                    spntalukaList.add("Select Taluka");
                    for (int i = 0; i < mTaluka_list.size(); i++) {
                        spntalukaList.add(mTaluka_list.get(i).getTalukaName());
                    }
                    spnTaluka.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, spntalukaList));
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnTaluka.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spnTalukaCode = spntalukaCodeList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        /*Bind State District Taluka*/

        spnStateCodeList = new ArrayList<String>();
        spnStateCodeList.add("0");
        for (int i = 0; i < mState_list.size(); i++) {
            spnStateCodeList.add(mState_list.get(i).getStateCode());
        }

        List<String> spnStateList = new ArrayList<String>();
        spnStateList.add("Select State");
        for (int i = 0; i < mState_list.size(); i++) {
            spnStateList.add(mState_list.get(i).getStateName());
        }
        spnState.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, spnStateList));

        /////////////////////////////////////////////////////////////////////////////

        spnDistrictCodeList = new ArrayList<String>();
        spnDistrictCodeList.add("0");
        for (int i = 0; i < mDistrict_list.size(); i++) {
            spnDistrictCodeList.add(mDistrict_list.get(i).getDistrictCode());
        }


        List<String> spnDistrictList = new ArrayList<String>();
        spnDistrictList.add("Select District");
        for (int i = 0; i < mDistrict_list.size(); i++) {
            spnDistrictList.add(mDistrict_list.get(i).getDistrictName());
        }
        spnDistrict.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, spnDistrictList));

        /////////////////////////////////////////////////////////////////////////////

//        spntalukaCodeList = new ArrayList<String>();
//        spntalukaCodeList.add("0");
//        if (mTaluka_list != null) {
//            for (int i = 0; i < mTaluka_list.size(); i++) {
//                spntalukaCodeList.add(mTaluka_list.get(i).getTalukaCode());
//            }
//
//            List<String> spntalukaList = new ArrayList<String>();
//            spntalukaList.add("Select Taluka");
//            for (int i = 0; i < mTaluka_list.size(); i++) {
//                spntalukaList.add(mTaluka_list.get(i).getTalukaName());
//            }
//            spnTaluka.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, spntalukaList));
//        }
        /*Bind State District Taluka End*/
        Intent i = getIntent();
        ReferanceFrom = i.getStringExtra("ReferanceFrom");

        if (ReferanceFrom.equals("DistributorTab")) {
            getSupportActionBar().setTitle("Distributor Appointment");
        } else if (ReferanceFrom.equals("RetailerTab")) {
            getSupportActionBar().setTitle("Retailer Appointment");
        }

        txtMobile.setText(i.getStringExtra("mobile"));
        txtMobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (txtMobile.getText().length() == 10) {

                    if (ReferanceFrom.equals("DistributorTab")) {
                        Cursor data = db.Dist_data(txtMobile.getText().toString());
                        if (data.getCount() == 0) {

                        } else {
                            Utils.showAlertDialog("Warning", "User Already Available!", dist_appointment.this);
                        }
                    } else if (ReferanceFrom.equals("RetailerTab")) {
                        Cursor data = db.Ret_data(txtMobile.getText().toString());
                        if (data.getCount() == 0) {

                        } else {
                            Utils.showAlertDialog("Warning", "User Already Available!", dist_appointment.this);
                        }
                    }

                } else {

                }
            }
        });
        btn_Photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(dist_appointment.this, android.Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_DENIED) {
                }
                try {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        captureImage();
                    } else {
                        captureImage2();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    msclass.showMessage(ex.getMessage());
                }
            }

        });

        btn_farmerSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Interest = "";
                String mahyco = "";
                if (switchIntrested.isChecked()) {
                    Interest = "Yes";
                } else {
                    Interest = "No";
                }
                if (switchmahyco.isChecked()) {
                    mahyco = "Yes";
                } else {
                    mahyco = "No";
                }
                String userCode = Preferences.get(dist_appointment.this, Constants.PREF_USERCODE);
                if (ReferanceFrom.equals("DistributorTab")) {
                    usertoAdd = "Dist";
                } else if (ReferanceFrom.equals("RetailerTab")) {
                    usertoAdd = "ret";
                }
                if (validation() == true) {
                    boolean result = db.InsertDist_appointment(usertoAdd, txtfirmName.getText().toString(), txtPropriterName.getText().toString(),
                            txtVillage.getText().toString(), spnTalukaCode, spnDistrictCode,
                            spnStateCode, txtPincode.getText().toString(), txtMobile.getText().toString(),
                            txtNearestClient.getText().toString(), txtremark.getText().toString(), Interest, mahyco,
                            Imagepath1,
                            userCode, currentDateandTime,
                            cordinates, geoAddress,
                            "0");
                    if (result) {
                        //Utils.showAlertDialogOk("SUCCESS", "New Request generated and sent for approval!\nWill available after 24 hours to download.", dist_appointment.this);
                    } else {
                        Utils.showAlertDialogOk("Error", "Something Went Wrong", dist_appointment.this);
                    }

                    /* Uplaod Data*/
                    if (CheckConnection.isNetworkAvailable(dist_appointment.this)) {
                        String searchQuery = "select  *  from Dist_appoint where  uploaded ='0'";
                        Cursor cursor = db.getReadableDatabase().rawQuery(searchQuery, null);
                        int count = cursor.getCount();
                        if (count > 0) {
                            if (cursor.moveToFirst()) {
                                do {
                                    up_user_type = cursor.getString(cursor.getColumnIndex("user_type"));
                                    up_firmName = cursor.getString(cursor.getColumnIndex("firmName"));
                                    up_PropriterName = cursor.getString(cursor.getColumnIndex("PropriterName"));
                                    up_Village = cursor.getString(cursor.getColumnIndex("Village"));
                                    up_Taluka = cursor.getString(cursor.getColumnIndex("Taluka"));
                                    up_District = cursor.getString(cursor.getColumnIndex("District"));
                                    up_State = cursor.getString(cursor.getColumnIndex("State"));
                                    up_Pincode = cursor.getString(cursor.getColumnIndex("Pincode"));
                                    up_Mobile = cursor.getString(cursor.getColumnIndex("Mobile"));
                                    up_NearestClient = cursor.getString(cursor.getColumnIndex("NearestClient"));
                                    up_remark = cursor.getString(cursor.getColumnIndex("remark"));
                                    up_Intrested = cursor.getString(cursor.getColumnIndex("Intrested"));
                                    up_mahycoClient = cursor.getString(cursor.getColumnIndex("mahycoClient"));
                                    up_photo = cursor.getString(cursor.getColumnIndex("photo"));
                                    up_usercode = cursor.getString(cursor.getColumnIndex("usercode"));
                                    up_tr_date = cursor.getString(cursor.getColumnIndex("tr_date"));
                                    up_cordinate = cursor.getString(cursor.getColumnIndex("cordinate"));
                                    up_geolocation = cursor.getString(cursor.getColumnIndex("geolocation"));

                                    uploadData(up_user_type, up_firmName, up_PropriterName, up_Village, up_Taluka, up_District, up_State, up_Pincode, up_Mobile, up_NearestClient,
                                            up_remark, up_Intrested, up_mahycoClient, up_photo, up_usercode, up_tr_date, up_cordinate, up_geolocation);
                                }
                                while (cursor.moveToNext());
                            }
                        } else {
                            Toast.makeText(dist_appointment.this, "Data Will Upload When Internet Available", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });
    }

    protected boolean validation() {
        boolean flag = true;

        if (txtMobile.getText().length() == 0 || txtMobile.getText().length() < 10) {
            msclass.showMessage("Please Enter Valid Mobile No ");
            txtMobile.setError("Required");
            return false;
        }

        if (txtfirmName.getText().length() == 0) {
            msclass.showMessage("Please Enter Firm name ");
            txtfirmName.setError("Required");
            return false;
        }
        if (txtPropriterName.getText().length() == 0) {
            msclass.showMessage("Please Enter Proprieter Name ");
            txtPropriterName.setError("Required");
            return false;
        }
        if (spnStateCode == null || spnStateCode.isEmpty() || spnStateCode.equalsIgnoreCase("0")) {
            msclass.showMessage("Please select State ");
            return false;
        }
        if (spnDistrictCode == null || spnDistrictCode.isEmpty() || spnDistrictCode.equalsIgnoreCase("0")) {
            msclass.showMessage("Please select District ");
            return false;
        }
        if (spnTalukaCode == null || spnTalukaCode.isEmpty() || spnTalukaCode.equalsIgnoreCase("0")) {
            msclass.showMessage("Please select Taluka ");
            return false;
        }

        if (txtVillage.getText().length() == 0) {
            msclass.showMessage("Please Enter Village ");
            txtVillage.setError("Required");
            return false;
        }
        if (txtPincode.getText().length() == 0) {
            msclass.showMessage("Please Enter Pincode ");
            txtPincode.setError("Required");
            return false;
        }

        if (txtNearestClient.getText().length() == 0) {
            msclass.showMessage("Please Enter nearest client ");
            txtNearestClient.setError("Required");
            return false;
        }

        if (!hasImage(ivImage)) {
            msclass.showMessage("Please Capture Image");
            return false;
        }
        return true;
    }

    private boolean hasImage(@NonNull ImageView view) {
        Drawable drawable = view.getDrawable();
        boolean hasImage = (drawable != null);
        if (hasImage && (drawable instanceof BitmapDrawable)) {
            hasImage = ((BitmapDrawable) drawable).getBitmap() != null;
        }
        return hasImage;
    }

    private void captureImage2() {

        try {
            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

            photoFile = createImageFile4();
            if (photoFile != null) {
                Log.i("Mayank", photoFile.getAbsolutePath());
                Uri photoURI = Uri.fromFile(photoFile);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(cameraIntent, REQUEST_CAMERA);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            msclass.showMessage(ex.getMessage());
        }
    }

    private void captureImage() {

        try {

            if (ContextCompat.checkSelfPermission(this
                    , android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
            } else {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(this.getPackageManager()) != null) {
                    // Create the File where the photo should go
                    try {
                        photoFile = createImageFile();

                        if (photoFile != null) {
                            Uri photoURI = FileProvider.getUriForFile(this,
                                    "com.growindigo.connectplus",
                                    photoFile);
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                            startActivityForResult(takePictureIntent, REQUEST_CAMERA);
                        }
                    } catch (Exception ex) {

                        msclass.showMessage(ex.toString());
                        ex.printStackTrace();
                    }


                } else {
                }
            }
        } catch (Exception ex) {
            msclass.showMessage(ex.getMessage());
            ex.printStackTrace();
            //dialog.dismiss();
        }
    }

    private File createImageFile4() //  throws IOException
    {
        File mediaFile = null;
        try {
            // External sdcard location
            File mediaStorageDir = new File(
                    Environment
                            .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                    IMAGE_DIRECTORY_NAME);
            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    return null;
                }
            }
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                    Locale.getDefault()).format(new Date());
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } catch (Exception ex) {
            ex.printStackTrace();
            msclass.showMessage(ex.getMessage());
        }
        return mediaFile;
    }

    private File createImageFile() {
        // Create an image file name
        File image = null;
        try {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = "JPEG_" + timeStamp + "_";
            File storageDir = this.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );

        } catch (Exception ex) {
            ex.printStackTrace();
            msclass.showMessage(ex.toString());
        }
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == SELECT_FILE)
                    onSelectFromGalleryResult(data);
                else if (requestCode == REQUEST_CAMERA)
                    onCaptureImageResult(data);
            }
        } catch (Exception e) {
            e.printStackTrace();
            msclass.showMessage(e.toString());
        }
    }

    private void onCaptureImageResult(Intent data) {
        try {
            try {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 2;
                Bitmap myBitmap = BitmapFactory.decodeFile(photoFile.getAbsolutePath(), options);
                //Imagepath1 = Utils.getBase64FromPath(photoFile.getAbsolutePath());
                Imagepath1 = photoFile.getAbsolutePath();
                //Log.d("Imagepath1::", Imagepath1);
                ivImage.setImageBitmap(myBitmap);
                ivImage.setVisibility(View.VISIBLE);

            } catch (Exception e) {
                msclass.showMessage(e.toString());
                e.printStackTrace();
            }
            //end
        } catch (Exception e) {
            msclass.showMessage(e.toString());
            e.printStackTrace();
        }
    }

    private void onSelectFromGalleryResult(Intent data) {


        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(this.getApplicationContext().getContentResolver(), data.getData());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        ivImage.setImageBitmap(bm);
        ivImage.setVisibility(View.VISIBLE);


    }

    protected void Initializecontrol() {
        txtfirmName = (EditText) findViewById(R.id.txtfirmName);
        txtPropriterName = (EditText) findViewById(R.id.txtPropriterName);
        txtVillage = (EditText) findViewById(R.id.txtVillage);

        spnState = (SearchableSpinner) findViewById(R.id.spnState);
        spnDistrict = (SearchableSpinner) findViewById(R.id.spnDistrict);
        spnTaluka = (SearchableSpinner) findViewById(R.id.spnTaluka);

        txtPincode = (EditText) findViewById(R.id.txtPincode);
        txtMobile = (EditText) findViewById(R.id.txtMobile);
        txtNearestClient = (EditText) findViewById(R.id.txtNearestClient);
        txtremark = (EditText) findViewById(R.id.txtremark);
        switchIntrested = (Switch) findViewById(R.id.switchIntrested);
        switchmahyco = (Switch) findViewById(R.id.switchmahyco);
        btn_Photo = (Button) findViewById(R.id.btn_Photo);
        btn_farmerSave = (Button) findViewById(R.id.btn_farmerSave);
        ivImage = (ImageView) findViewById(R.id.ivImage);
        msclass = new Messageclass(this);
        db = new DBCreation(this);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        currentDateandTime = sdf.format(new Date());
        getLocation();
        mState_list = db.getStateListData();
        mState_Code = db.getStateListData();
        mDistrict_list = db.getDistrictListData();
        mDistrict_Code = db.getDistrictListData();
        // mTaluka_list = db.getTalukaDetailsData();
        // mTaluka_Code = db.getTalukaDetailsData();
        pDialog = new ProgressDialog(this);
        checkInternet();
    }

    public void getLocation() {
        try {
            locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) this);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, (LocationListener) this);
            //locationManager.getLastKnownLocation(locationManager.GPS_PROVIDER);
            setUpMapIfNeeded();
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    public void onLocationChanged(Location location) {
        longitude = location.getLongitude();
        latitude = location.getLatitude();
        cordinates = location.getLatitude() + "-" + location.getLongitude();
        getLocation();
    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, "Please Enable GPS", Toast.LENGTH_SHORT).show();
        final Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    private void setUpMapIfNeeded() {

        Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
        String result = null;
        try {
            List<Address> addressList = geocoder.getFromLocation(
                    latitude, longitude, 1);
            if (addressList != null && addressList.size() > 0) {
                Address address = addressList.get(0);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getAddressLine(i)).append("\n");//adress
                }
//                sb.append(address.getLocality()).append("\n");//village
//
//                sb.append(address.getPostalCode()).append("\n");
//                sb.append(address.getCountryName());
//                sb.append(address.getAdminArea()).append("\n"); //state
//
//                sb.append(address.getSubAdminArea()).append("\n");//district


                //txtState.setText(address.getAdminArea());
                //txtDistrict.setText(address.getSubAdminArea());
                txtVillage.setText(address.getLocality());
                // txtTaluka.setText(address.getSubAdminArea());
                txtPincode.setText(address.getPostalCode());
                geoAddress = (address.getAddressLine(0).toString());
                //txtFarmerAddress.setEnabled(false);
                result = sb.toString();
            }
        } catch (IOException e) {
            // Log.e(TAG, "Unable connect to Geocoder", e);
        }


    }


    public void uploadData(String up_user_type, String up_firmName, String up_PropriterName, String up_Village, String up_Taluka,
                           String up_District, String up_State, String up_Pincode, String up_Mobile, String up_NearestClient,
                           String up_remark, String up_Intrested, String up_mahycoClient, String up_photo,
                           String up_usercode, String up_tr_date, String up_cordinate, String up_geolocation
    ) {
        String converdImage = "";
        if (up_photo != null) {
            if (up_photo == "") {
                converdImage = "";
            } else {
                converdImage = Utils.getImageDatadetail(up_photo);
            }
        }
        Map<String, String> paramsReq = new HashMap<String, String>();
        paramsReq.put("user_type", up_user_type);
        paramsReq.put("firmName", up_firmName);
        paramsReq.put("PropriterName", up_PropriterName);
        paramsReq.put("Village", up_Village);
        paramsReq.put("Taluka", up_Taluka);
        paramsReq.put("District", up_District);
        paramsReq.put("State", up_State);
        paramsReq.put("Pincode", up_Pincode);
        paramsReq.put("Mobile", up_Mobile);
        paramsReq.put("NearestClient", up_NearestClient);
        paramsReq.put("remark", up_remark);
        paramsReq.put("Intrested", up_Intrested);
        paramsReq.put("mahycoClient", up_mahycoClient);
        paramsReq.put("photo", converdImage);
        paramsReq.put("usercode", up_usercode);
        paramsReq.put("tr_date", up_tr_date);
        paramsReq.put("cordinate", up_cordinate);
        paramsReq.put("geolocation", up_geolocation);
        if (up_Mobile != null && !up_Mobile.isEmpty())
            db.updateDist_appoint(up_Mobile, "1");
        apiRequest(paramsReq, Constants.dist_Appointment_upload, Request.Method.POST);
    }

    private void apiRequest(Map<String, String> paramsReq, String tag, int method) {
        ApiRequest apiRequest = new ApiRequest(this, this);
        pDialog.setMessage("Generating User");
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setCancelable(false);
        pDialog.show();
        apiRequest.postJSONRequest(Constants.BASE_URL + Constants.dist_Appointment_upload, tag,
                paramsReq, method);
    }


    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        //Log.d("upload:: ", response);
        try {
            if (tag_json_obj.equals(Constants.dist_Appointment_upload)) {
                JSONObject jsonObj = new JSONObject(response);
                if (response.contains("Table")) {
                    JSONArray jsonArray = jsonObj.getJSONArray("Table");
                    // Extract the Place descriptions from the results
                    String responseUsername = "", responseUsercode = "", responseUserMobile = "";
                    for (int i = 0; i < jsonArray.length(); i++) {
                        responseUsername = jsonArray.getJSONObject(i).getString("dist_name");
                        responseUsercode = jsonArray.getJSONObject(i).getString("Code");
                        responseUserMobile = jsonArray.getJSONObject(i).getString("mobile");
                        if (ReferanceFrom.equals("DistributorTab")) {
                            db.insertDistributorData(responseUsername, responseUsercode, responseUserMobile);
                        } else if (ReferanceFrom.equals("RetailerTab")) {
                            db.insertRetailerData(responseUsername, responseUsercode, responseUserMobile);
                        }

                    }
                    Utils.showAlertDialogOk("SUCCESS", "User Register Successfully", dist_appointment.this);
                } else {
                    Utils.showAlertDialog(getString(R.string.error), getString(R.string.some_problem_occurred), dist_appointment.this);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        //Toast.makeText(dist_appointment.this, "Data Uploaded Successfully", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        //Log.d("Error::", "error:: " + error);
    }

    @Override
    public void onResume() {
        super.onResume();
        String userCode = Preferences.get(dist_appointment.this, Constants.PREF_USERCODE);
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
        Utils.fetchCurrentLocation(userCode, String.valueOf(latitude), String.valueOf(longitude), this.getClass().getSimpleName(), formattedDate + " " + currentTime, this, this);
    }

    protected void checkInternet() {
        if (CheckConnection.isNetworkAvailable(this)) {

        } else {
            Utils.showAlertDialogOk("Warning", "Internet Connection Not Available!", this);
        }
    }
}
