package com.growindigo.connectplus;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class ReportVisitAdaptor extends RecyclerView.Adapter<ReportVisitAdaptor.DataObjectHolder> {
    private List<ReportVisitModel> objects = new ArrayList<ReportVisitModel>();
    private Context context;
    private LayoutInflater layoutInflater;
    //Messageclass ms;
    int imageResourceId = 0;
    String visitnodetails="";
    FragmentManager fragmentManager;
    public ReportVisitAdaptor(List<ReportVisitModel> getlist, Context context , FragmentManager fragmentManager) {
        this.context = context;
        this.objects = getlist;
        this.layoutInflater = LayoutInflater.from(context);
        this.fragmentManager = fragmentManager;
    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.report_visitdetails, parent, false);

        return new ReportVisitAdaptor.DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, int position) {
        holder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.white1));

        holder.txt_visitnoDetails.setText(objects.get(position).getTxt_visitnoDetails());
        holder.txt_visitDetails.setText(objects.get(position).getTxt_visitDetails());
        holder.txt_PeriodDetails.setText(objects.get(position).getTxt_PeriodDetails());
        holder.txt_CropStageDetails.setText(objects.get(position).getTxt_CropStageDetails());
        holder.txt_ObservationDetails.setText(objects.get(position).getTxt_ObservationDetails());
        holder.txt_ProductDetails.setText(objects.get(position).getTxt_ProductDetails());
        holder.txt_DosageDetails.setText(objects.get(position).getTxt_DosageDetails());
        holder.txt_PuchaseDetails.setText(objects.get(position).getTxt_PuchaseDetails());
        holder.txt_VisitDateDetails.setText(objects.get(position).getTxt_VisitDateDetails());
        holder.txt_RemarkDetails.setText(objects.get(position).getTxt_RemarkDetails());

    }


    @Override
    public int getItemCount() {
        return objects.size();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        private LinearLayout rel_top;
        private RelativeLayout rel_booked_by;
        private CardView cardView;
        private LinearLayout linOrders;
//        private TextView txt_fnamedetails;
//        private TextView txt_MobileDetails;
//        private TextView txt_CropDetails;
//        private TextView txt_AreaDetails;
//        private TextView txt_SowingDateDetails;
        private TextView txt_visitnoDetails;
        private TextView txt_visitDetails;
        private TextView txt_PeriodDetails;
        private TextView txt_CropStageDetails;
        private TextView txt_ObservationDetails;
        private TextView txt_ProductDetails;
        private TextView txt_DosageDetails;
        private TextView txt_PuchaseDetails;
        private TextView txt_RemarkDetails,txt_VisitDateDetails;
        private RecyclerView recycler;
        ImageView arrow;

        public DataObjectHolder(View view) {
            super(view);
            rel_top = (LinearLayout) view.findViewById(R.id.rel_top);
            cardView = (CardView) view.findViewById(R.id.card_view);

//            txt_fnamedetails= (TextView) view.findViewById(R.id.txt_fnamedetails);
//            txt_MobileDetails= (TextView) view.findViewById(R.id.txt_MobileDetails);
//            txt_CropDetails= (TextView) view.findViewById(R.id.txt_CropDetails);
//            txt_AreaDetails= (TextView) view.findViewById(R.id.txt_AreaDetails);
//            txt_SowingDateDetails= (TextView) view.findViewById(R.id.txt_SowingDateDetails);
            txt_visitnoDetails= (TextView) view.findViewById(R.id.txt_visitnoDetails);
            txt_visitDetails= (TextView) view.findViewById(R.id.txt_visitDetails);
            txt_PeriodDetails= (TextView) view.findViewById(R.id.txt_PeriodDetails);
            txt_CropStageDetails= (TextView) view.findViewById(R.id.txt_CropStageDetails);
            txt_ObservationDetails= (TextView) view.findViewById(R.id.txt_ObservationDetails);
            txt_ProductDetails= (TextView) view.findViewById(R.id.txt_ProductDetails);
            txt_DosageDetails= (TextView) view.findViewById(R.id.txt_DosageDetails);
            txt_PuchaseDetails= (TextView) view.findViewById(R.id.txt_PuchaseDetails);
            txt_RemarkDetails= (TextView) view.findViewById(R.id.txt_RemarkDetails);
            txt_VisitDateDetails= (TextView) view.findViewById(R.id.txt_VisitDateDetails);

        }
    }
}
