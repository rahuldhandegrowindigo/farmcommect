package com.growindigo.connectplus;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.growindigo.connectplus.databaseutils.DBCreation;
import com.growindigo.connectplus.utils.Constants;
import com.growindigo.connectplus.utils.Preferences;

public class splash_activity extends AppCompatActivity {
    DBCreation CreateDB;
    boolean IS_LOGGED_IN = false;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);
        CreateDB = new DBCreation(this);
        new Thread(new Runnable() {
            @Override
            public void run() {
                dowork();
                startapp();
                finish();
            }
        }).start();
    }

    private void dowork() {

        for (int progress = 0; progress < 100; progress += 10) {
            try {
                Thread.sleep(100);//300
                //mprogresss.setProgress(progress);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void startapp() {

        IS_LOGGED_IN = Preferences.getBool(this, Constants.IS_LOGGED_IN);
        if (IS_LOGGED_IN) {
            intent = new Intent(this, MainActivity.class);
        } else {
            intent = new Intent(this, login.class);
        }
        startActivity(intent);
    }

}
