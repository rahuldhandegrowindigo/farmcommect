package com.growindigo.connectplus;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.growindigo.connectplus.databaseutils.DBCreation;
import com.growindigo.connectplus.model.listModel;
import com.growindigo.connectplus.databaseutils.listadaptor;
import com.growindigo.connectplus.utils.Constants;
import com.growindigo.connectplus.utils.Preferences;
import com.growindigo.connectplus.utils.Utils;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 */
public class existing_farmer extends Fragment {
    public ListView list;
    public RecyclerView recyclerList;
    listadaptor listadaptor;
    DBCreation db;
    private EditText txtMobile;
    SearchableSpinner spnFarmerVisitPurpose;
    String getSelectedFarmerVisit = "";
    ArrayList<listModel> mlist;

    public existing_farmer() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_existing_farmer, container, false);

        txtMobile = (EditText) rootView.findViewById(R.id.txtMobile);
        recyclerList = (RecyclerView) rootView.findViewById(R.id.recycler1);
        spnFarmerVisitPurpose = (SearchableSpinner) rootView.findViewById(R.id.spnFarmerVisitPurpose);
        db = new DBCreation(this.getContext());
        LinearLayoutManager lllm = new LinearLayoutManager(this.getContext());
        lllm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerList.setLayoutManager(lllm);
        mlist = db.getFarmerlistData();

        loadspinnerdata();
        spnFarmerVisitPurpose.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getSelectedFarmerVisit = spnFarmerVisitPurpose.getSelectedItem().toString();
                //Log.d("selectedFarmer::", getSelectedFarmerVisit);
                listadaptor = new listadaptor(getSelectedFarmerVisit, mlist, getActivity(), getFragmentManager());
                recyclerList.setAdapter(listadaptor);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        txtMobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                ArrayList<listModel> mlist = db.SearchFarmerlistData(txtMobile.getText().toString());
                if (mlist != null && mlist.size() <= 0) {
                    Utils.showAlertDialog("Warning", "Farmer Not Available!\nKindly Add New Farmer!", getContext());
                } else {
                    listadaptor = new listadaptor(getSelectedFarmerVisit, mlist, getActivity(), getFragmentManager());
                    recyclerList.setAdapter(listadaptor);
                }
            }
        });
        return rootView;
    }


    protected void loadspinnerdata() {
        List<String> spnFarmerVisitPurpose_list = new ArrayList<String>();
        spnFarmerVisitPurpose_list.add("Select Visit Purpose");
        spnFarmerVisitPurpose_list.add("Field Visit");
        spnFarmerVisitPurpose_list.add("Demonstration");
        spnFarmerVisitPurpose_list.add("Demo Follow up Result Check");
        spnFarmerVisitPurpose_list.add("Complaint");
        spnFarmerVisitPurpose_list.add("Farmer Meet");
        spnFarmerVisitPurpose.setAdapter(new ArrayAdapter<String>(this.getContext(), android.R.layout.simple_spinner_dropdown_item, spnFarmerVisitPurpose_list));
    }
}
