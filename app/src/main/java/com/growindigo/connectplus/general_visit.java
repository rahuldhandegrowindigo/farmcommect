package com.growindigo.connectplus;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.growindigo.connectplus.utils.CheckConnection;
import com.growindigo.connectplus.utils.Constants;
import com.growindigo.connectplus.utils.Preferences;
import com.growindigo.connectplus.utils.Utils;
import com.growindigo.connectplus.volley.ApiRequest;
import com.growindigo.connectplus.volley.IApiResponse;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class general_visit extends AppCompatActivity implements IApiResponse, LocationListener {

    EditText txtRemark;
    Button btn_next;
    private double longitude;
    private double latitude;
    String cordinates = "";
    String geoAddress = "";
    LocationManager locationManager;
    String ReferanceFrom = "";
    String FromScreen = "";
    String DistributorCode;
    TextView DistributorName, lbldistName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_general_visit);

        txtRemark=(EditText)findViewById(R.id.txtRemark);
        btn_next=(Button)findViewById(R.id.btn_next);
        DistributorName = (TextView) findViewById(R.id.DistributorName);
        lbldistName = (TextView) findViewById(R.id.lbldistName);
        checkInternet();

        Intent i = getIntent();
        ReferanceFrom = i.getStringExtra("ReferanceFrom");
        FromScreen = i.getStringExtra("ref_From");
        DistributorName.setText(i.getStringExtra("dist_name"));
        DistributorCode = i.getStringExtra("dist_code");

        if (ReferanceFrom.equals("DistributorTab")) {
            getSupportActionBar().setTitle("Distributor Visit");
            lbldistName.setText("Distributor Name");
        } else if (ReferanceFrom.equals("RetailerTab")) {
            getSupportActionBar().setTitle("Retailer Visit");
            lbldistName.setText("Retailer Name");
        }

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userCode = Preferences.get(general_visit.this, Constants.PREF_USERCODE);

                uploadData(DistributorName.getText().toString(), DistributorCode, userCode,
                        cordinates, geoAddress,txtRemark.getText().toString());
            }
        });
    }
    public void getLocation() {
        try {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
            }
            Criteria criteria = new Criteria();
            criteria.setVerticalAccuracy(Criteria.ACCURACY_HIGH);
            criteria.setPowerRequirement(Criteria.POWER_HIGH);
            criteria.setAltitudeRequired(false);
            criteria.setSpeedRequired(false);
            criteria.setCostAllowed(true);
            criteria.setBearingRequired(false);
            locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) this);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, (LocationListener) this);
            locationManager.getLastKnownLocation(locationManager.GPS_PROVIDER);
            setUpMapIfNeeded();
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }
    protected void checkInternet() {
        if (CheckConnection.isNetworkAvailable(this)) {

        } else {
            Utils.showAlertDialogOk("Warning", "Internet Connection Not Available!", this);
        }
    }
    private void setUpMapIfNeeded() {

        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        String result = null;
        try {
            List<Address> addressList = geocoder.getFromLocation(
                    latitude, longitude, 1);
            if (addressList != null && addressList.size() > 0) {
                Address address = addressList.get(0);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getAddressLine(i)).append("\n");//adress
                }
                geoAddress = address.getAddressLine(0).toString();
                result = sb.toString();
            }
        } catch (IOException e) {
            // Log.e(TAG, "Unable connect to Geocoder", e);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        longitude = location.getLongitude();
        latitude = location.getLatitude();
        cordinates = location.getLatitude() + "-" + location.getLongitude();
        //Log.d("coordinates::", " :: " + cordinates);
        getLocation();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, "Please Enable GPS", Toast.LENGTH_SHORT).show();
        final Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
    }
    public void uploadData(String name, String code, String usercode, String cordinate, String geoaddress, String remark) {

        //encodedData
        Map<String, String> paramsReq = new HashMap<String, String>();
        paramsReq.put("dist_name", name);
        paramsReq.put("dist_Code", code);
        paramsReq.put("usercode", usercode);
        if (remark != null && remark.length() > 0)
            paramsReq.put("remark", remark);
        else
            paramsReq.put("remark", "NA");
        apiRequest(paramsReq, Constants.UploadGenralVisit, Request.Method.POST);
    }
    private void apiRequest(Map<String, String> paramsReq, String tag, int method) {
        ApiRequest apiRequest = new ApiRequest(this, this);
        apiRequest.postJSONRequest(Constants.BASE_URL + Constants.UploadGenralVisit, tag,
                paramsReq, method);
    }
    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        //Log.d("upload:: ", response);
      //  Toast.makeText(general_visit.this, "Data Uploaded Successfully", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        //Log.d("Error::", error.getMessage());
    }

    @Override
    public void onResume() {
        super.onResume();
        String userCode = Preferences.get(general_visit.this, Constants.PREF_USERCODE);
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
        getLocation();
        Utils.fetchCurrentLocation(userCode, String.valueOf(latitude), String.valueOf(longitude), this.getClass().getSimpleName(), formattedDate + " " + currentTime, this, this);
    }
}