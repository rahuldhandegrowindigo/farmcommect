package com.growindigo.connectplus;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.growindigo.connectplus.databaseutils.DBCreation;
import com.growindigo.connectplus.utils.Messageclass;

import java.util.ArrayList;
import java.util.List;

public class login extends AppCompatActivity {
    private Button btnLogin, btnRegister;
    private TextView Forget_pass;
    ProgressDialog dialog;
    public Messageclass msclass;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    private Handler handler = null;
    private DBCreation mDatabase;
    EditText username, password;

    private static final int MY_PERMISSIONS_REQUEST_ACCOUNTS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().setTitle("Login");
        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        editor = pref.edit();
        handler = new Handler();
        mDatabase = new DBCreation(this);
        msclass = new Messageclass(this);
        dialog = new ProgressDialog(this);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        btnLogin = (Button) findViewById(R.id.btn_login);
        btnRegister = (Button) findViewById(R.id.btnRegister);

        Forget_pass = (TextView) findViewById(R.id.txt_forget_pass);
        checkAndRequestPermissions();
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(login.this, register.class));
                // finish();

            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginRequest();
                //  startActivity(new Intent(login.this, MainActivity.class));
                // finish();
            }
        });
        Forget_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(login.this, password_reset.class));
                //finish();
            }
        });
    }

    private boolean checkAndRequestPermissions() {
        int permissionCAMERA = ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.CAMERA);

        int storagePermission = ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.READ_EXTERNAL_STORAGE);

        int READ_PHONE_STATE = ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.READ_PHONE_STATE);
        int WRITE_EXTERNAL_STORAGE = ContextCompat.checkSelfPermission(this,

                android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int ACCESS_FINE_LOCATION = ContextCompat.checkSelfPermission(this,

                android.Manifest.permission.ACCESS_FINE_LOCATION);
        int ACCESS_COARSE_LOCATION = ContextCompat.checkSelfPermission(this,

                android.Manifest.permission.ACCESS_COARSE_LOCATION);
        int USE_FINGERPRINT = ContextCompat.checkSelfPermission(this,

                android.Manifest.permission.USE_FINGERPRINT);
        int INTERNET = ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.INTERNET);

        int permissionSendMessage = ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.SEND_SMS);

        int receiveSMS = ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.RECEIVE_SMS);

        int readSMS = ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.READ_SMS);

        List<String> listPermissionsNeeded = new ArrayList<>();
        if (INTERNET != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.INTERNET);
        }
        if (READ_PHONE_STATE != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.READ_PHONE_STATE);
        }
        if (WRITE_EXTERNAL_STORAGE != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (ACCESS_FINE_LOCATION != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (ACCESS_COARSE_LOCATION != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (USE_FINGERPRINT != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.USE_FINGERPRINT);
        }

        if (storagePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (permissionCAMERA != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.CAMERA);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.SEND_SMS);
        }
        if (receiveSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.RECEIVE_SMS);
        }
        if (readSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.READ_SMS);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this,
                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MY_PERMISSIONS_REQUEST_ACCOUNTS);
            return false;
        }
        return true;
    }

    public void LoginRequest() {
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        String usernameEntered = username.getText().toString().trim();
        String passwordEntered = password.getText().toString().trim();
        // String lang = spnlanguage.getSelectedItem().toString().trim();
        dialog.setMessage("Loading. Please wait...");
        dialog.show();
        logincheck(usernameEntered.trim(), passwordEntered.trim());
        //  new LoginReq().execute(SERVER,username,pass);

    }

    private void logincheck(String username, String pass) {
        try {
            boolean isExist = mDatabase.checkUser(username, pass);
            dialog.dismiss();
            if (isExist) {
                Intent intent = new Intent(login.this, MainActivity.class);
                // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            } else {
                msclass.showMessage("User name and password not correct ,please try again Or Register Once again.");
            }
        } catch (Exception ex) {
            msclass.showMessage(ex.getMessage());
            dialog.dismiss();
        }
    }
}
